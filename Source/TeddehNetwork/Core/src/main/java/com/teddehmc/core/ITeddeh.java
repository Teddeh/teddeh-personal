package com.teddehmc.core;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface ITeddeh
{
	void load();
	void disable();
}
