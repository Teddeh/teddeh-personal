package com.teddehmc.core;

/**
 * Created: 05/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum ModuleState
{
	DEVELOPMENT,
	PRE_ALPHA,
	ALPHA,
	BETA,
	RELEASE
}
