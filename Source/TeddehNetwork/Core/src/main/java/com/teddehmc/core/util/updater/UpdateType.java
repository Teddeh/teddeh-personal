package com.teddehmc.core.util.updater;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum UpdateType
{
	SEC_10(10000),
	SEC_5(5000),
	SEC_4(4000),
	SEC_3(3000),
	SEC_2(2000),
	SEC_1(1000),
	TICK_15(750),
	TICK_10(500),
	TICK_5(250),
	TICK_4(200),
	TICK_3(150),
	TICK_2(100),
	TICK_1(50);

	private long time, left;

	UpdateType(long time)
	{
		this.time = time;
		this.left = System.currentTimeMillis();
	}

	public long getTimeInMilli()
	{
		return time;
	}

	public long getTimeLeft()
	{
		return left;
	}

	public void setTimeLeft(long time)
	{
		this.left = time;
	}
}
