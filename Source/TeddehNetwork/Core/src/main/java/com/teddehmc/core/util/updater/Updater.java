package com.teddehmc.core.util.updater;

import com.teddehmc.core.util.TimeUtil;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Updater implements Runnable
{
	private JavaPlugin plugin;

	public Updater(JavaPlugin plugin)
	{
		this.plugin = plugin;
		plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, this, 0L, 1L);
	}

	@Override
	public void run()
	{
		for (UpdateType update : UpdateType.values())
		{
			if (!TimeUtil.elapsed(update.getTimeLeft(), update.getTimeInMilli())) continue;

			update.setTimeLeft(System.currentTimeMillis());
			Bukkit.getServer().getPluginManager().callEvent(new ScheduleEvent(update));
		}
	}
}
