package com.teddehmc.core.util;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.json.simple.JSONObject;

/**
 * Created: 09/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PlayerUtil
{
	public static void message(Player player, String prefix, String... messages)
	{
		for(String msg : messages)
		{
			sendPacket(player, new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + JSONObject.escape(prefix + ": " + msg) + "\"}")));
		}
	}

	public static void sendPacket(Player player, Packet packet)
	{
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}
}
