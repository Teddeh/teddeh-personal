package com.teddehmc.core.achievement.achievements;

import com.teddehmc.core.achievement.*;
import com.teddehmc.core.reward.RewardType;
import com.teddehmc.core.reward.TReward;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created: 07/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class AchievementWelcome extends Achievement
{

	public AchievementWelcome(JavaPlugin plugin)
	{
		super(
				plugin,
				1,
				"Newcomer",
				10,
				AchievementCategory.RECRUIT,
				new String[]
					{
						"This achievement is obtained by",
						"joining TeddehMC for the first time."
					}
		);
	}

	@Override
	public TReward[] getRewards()
	{
		return new TReward[]
			{
				new TReward<Integer>(RewardType.GLOBAL_EXP, 10)
			};
	}
}
