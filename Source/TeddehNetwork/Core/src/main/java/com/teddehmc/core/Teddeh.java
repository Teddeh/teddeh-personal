package com.teddehmc.core;

import com.teddehmc.core.packet.PacketInterceptor;
import com.teddehmc.core.packet.PacketManager;
import com.teddehmc.core.util.updater.Updater;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class Teddeh extends JavaPlugin implements ITeddeh
{
	@Override
	public void onEnable()
	{
		PacketInterceptor packetInterceptor = new PacketInterceptor();
		new PacketManager(this, packetInterceptor).load();

		new Updater(this).run();

		load();
	}

	@Override
	public void onDisable()
	{
		disable();
	}
}