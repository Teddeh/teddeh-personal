package com.teddehmc.core.util;

import org.bukkit.Bukkit;

import java.util.logging.Level;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class ServerUtil
{
	public static void log(String message)
	{
		log(Level.INFO, message);
	}

	public static void log(Level level, String message)
	{
		Bukkit.getLogger().log(level, message);
	}
}
