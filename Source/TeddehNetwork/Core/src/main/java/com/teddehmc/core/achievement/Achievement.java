package com.teddehmc.core.achievement;

import com.teddehmc.core.component.Component;
import com.teddehmc.core.reward.TReward;
import com.teddehmc.core.util.ServerUtil;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created: 07/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class Achievement extends Component implements IAchievement, AchievementReward
{
	protected int id, rewardPoints;
	protected String name;
	protected String[] description;
	protected AchievementCategory achievementCategory;

	public Achievement(JavaPlugin plugin, int id, String name, int rewardPoints, AchievementCategory achievementCategory, String[] description)
	{
		super(plugin);

		this.id = id;
		this.name = name;

		this.rewardPoints = rewardPoints;
		this.achievementCategory = achievementCategory;
		this.description = description;

		ServerUtil.log("Achievement registered(" + id + "): " + name);
		load();
	}

	@Override
	public void load() {}

	@Override
	public void disable() {}

	@Override
	public int getId()
	{
		return id;
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public void setComplete()
	{
		//TODO
	}

	@Override
	public void removeCompletion()
	{
		//TODO
	}

	@Override
	public int getAchievementPoints()
	{
		return rewardPoints;
	}

	@Override
	public TReward[] getRewards()
	{
		return null;
	}

	@Override
	public AchievementCategory getAchievementCategory()
	{
		return achievementCategory;
	}

	public String[] getDescription()
	{
		return description;
	}
}
