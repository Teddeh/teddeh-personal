package com.teddehmc.core;

import java.lang.annotation.*;

/**
 * Created: 05/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE_USE)
@Inherited
public @interface IModule
{
	String name();
	String version() default "1.0-SNAPSHOT";
	ModuleState moduleState();
}
