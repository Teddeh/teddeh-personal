package com.teddehmc.core.util;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class TimeUtil
{
	public static boolean elapsed(long from, long to)
	{
		return System.currentTimeMillis() - from > to;
	}
}
