package com.teddehmc.core.achievement;

/**
 * Created: 07/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface TimedAchievement
{
	long getTimeStartedInMilli();
	long getExpireInMilli();
}
