package com.teddehmc.core.achievement;

import com.teddehmc.core.reward.TReward;

/**
 * Created: 07/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface AchievementReward
{
	int getAchievementPoints();

	//TODO: Custom reward(s)
	TReward[] getRewards();
}
