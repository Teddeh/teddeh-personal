package com.teddehmc.core.util.updater;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created: 27/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class ScheduleEvent extends Event
{
	private static HandlerList handlers = new HandlerList();

	private UpdateType updateType;

	public ScheduleEvent(UpdateType updateType)
	{
		this.updateType = updateType;
	}

	public UpdateType getType()
	{
		return updateType;
	}

	@Override
	public HandlerList getHandlers()
	{
		return handlers;
	}

	public static HandlerList getHandlerList()
	{
		return handlers;
	}
}
