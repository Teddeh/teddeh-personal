package com.teddehmc.core.exception;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class TeddehException extends Exception
{
	public TeddehException() {}

	public TeddehException(String message) {
		super(message);
	}

	public TeddehException(Throwable cause) {
		super(cause);
	}

	public TeddehException(String message, Throwable cause) {
		super(message, cause);
	}
}
