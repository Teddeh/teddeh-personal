package com.teddehmc.core.component;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class Component implements IComponent
{
	public Component(JavaPlugin plugin)
	{
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}

	@Override
	public void load() {}

	@Override
	public void disable() {}
}
