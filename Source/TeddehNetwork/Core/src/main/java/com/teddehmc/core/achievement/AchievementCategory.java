package com.teddehmc.core.achievement;

import org.bukkit.ChatColor;

/**
 * Created: 07/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum AchievementCategory
{
	RECRUIT(ChatColor.YELLOW),
	REGULAR(ChatColor.GREEN),
	HARDENED(ChatColor.BLUE),
	VETERAN(ChatColor.RED);

	private ChatColor color;

	AchievementCategory(ChatColor color)
	{
		this.color = color;
	}

	public String getColor()
	{
		return color.toString();
	}
}
