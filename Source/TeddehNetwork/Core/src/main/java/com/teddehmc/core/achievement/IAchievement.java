package com.teddehmc.core.achievement;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

/**
 * Created: 07/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface IAchievement
{
	int getId();
	String getName();
	AchievementCategory getAchievementCategory();

	void setComplete();
	void removeCompletion();
}
