package com.teddehmc.ark.creature;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface ICreature
{
	float getExp();

	int getLevel();
	int getMaxLevel();

	double getHealth();
	double getMaxHealth();

	double getFood();
	double getMaxFood();

	double getStamina();
	double getMaxStamina();

	double getWeight();
	double getMaxWeight();

	double getOxygen();
	double getMaxOxygen();

	double getMeleeDamage();
}
