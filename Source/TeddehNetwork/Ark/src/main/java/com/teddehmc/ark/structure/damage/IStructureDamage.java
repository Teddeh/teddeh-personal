package com.teddehmc.ark.structure.damage;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface IStructureDamage
{
	double getDamage();
}
