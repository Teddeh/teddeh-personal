package com.teddehmc.ark.creature.food;

import com.teddehmc.ark.user.ArkUser;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface PlayerConsumable
{
	void onPlayerConsume(ArkUser user);
}
