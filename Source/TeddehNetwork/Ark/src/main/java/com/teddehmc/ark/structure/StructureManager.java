package com.teddehmc.ark.structure;

import com.teddehmc.ark.ArkManager;
import com.teddehmc.ark.material.ArkMaterial;
import com.teddehmc.ark.structure.data.Building;
import com.teddehmc.ark.structure.data.StructureBlock;
import com.teddehmc.ark.user.ArkUser;
import com.teddehmc.ark.util.StructureUtil;
import com.teddehmc.core.component.Component;
import com.teddehmc.core.util.Color;
import com.teddehmc.core.util.PlayerUtil;
import com.teddehmc.core.util.ServerUtil;
import com.teddehmc.core.util.updater.ScheduleEvent;
import com.teddehmc.core.util.updater.UpdateType;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */

//TODO: Clean up spaghetti testing code
public class StructureManager extends Component
{
	private ArkManager arkManager;

	private HashMap<ArkUser, Building> building;
	private Set<Material> blacklist;

	public StructureManager(JavaPlugin plugin, ArkManager arkManager)
	{
		super(plugin);

		this.arkManager = arkManager;

		building = new HashMap<>();
		blacklist = new HashSet<>();
	}

	@Override
	public void load()
	{
		for(Material material : Material.values())
		{
			if(material.isSolid())
			{
				if(material == Material.GLASS || material == Material.STAINED_GLASS)
				{
					blacklist.add(material);
					ServerUtil.log(material.toString());
				}
				continue;
			}

			blacklist.add(material);
			ServerUtil.log(material.toString());
		}
	}

	@EventHandler
	public void test(ScheduleEvent event)
	{
		if(event.getType() != UpdateType.TICK_5) return;

		List<ArkUser> usersToRemove = new ArrayList<>();

		for(ArkUser user : building.keySet())
		{
			Building build = building.get(user);
			build.setObstructed(false);
			if(build.isCancel())
			{
				build.getStructureBlocks().forEach(strBlock -> {
					strBlock.getLocation().getBlock().setType(strBlock.getOriginalMaterial());
					strBlock.getLocation().getBlock().setData(strBlock.getOriginalData());
				});
				build.clearStructureBlocks(true);
				usersToRemove.add(user);
				continue;
			}

			List<StructureBlock> structureBlocks = build.getStructureBlocks();
			if(!structureBlocks.isEmpty())
			{
				for (StructureBlock strBlock : structureBlocks)
				{
					strBlock.getLocation().getBlock().setType(strBlock.getOriginalMaterial());
					strBlock.getLocation().getBlock().setData(strBlock.getOriginalData());
				}
			}

			Location target = user.getPlayer().getTargetBlock(blacklist, 10).getLocation();
			if(target == null || target.getBlock() == null || target.getBlock().getType() == Material.AIR) continue;

			List<StructureBlock> newStrBlocks = new ArrayList<>();
			int[][][] dimensions = StructureUtil.adaptToDirection(((Structure)build.getStructureMaterial().getArkItem()).getDimension(), user);
			for(int y = 0; y < dimensions.length; y++)
			{
				for(int x = 0; x < dimensions[y].length; x++)
				{
					for(int z = 0; z < dimensions[y][x].length; z++)
					{
						if(dimensions[y][z][x] != 1) continue;
						Location loc = new Location(target.getWorld(), target.getX() + (x-1), target.getY() + (y+1), target.getZ() + (z-1));
						newStrBlocks.add(new StructureBlock(loc, loc.getBlock().getType(), loc.getBlock().getData()));
					}
				}
			}

			build.refill(newStrBlocks);
			for(StructureBlock strBlock : newStrBlocks)
			{
				if(!build.isObstructed())
				{
					if (strBlock.getOriginalMaterial() == Material.AIR) continue;
					build.setObstructed(true);
					break;
				}
			}

			PlayerUtil.sendPacket(user.getPlayer(), new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + (build.isObstructed() ? Color.RedB + "! OBSTRUCTED !" : Color.Green + "Right click the shovel to place") + "\"}"), (byte)2));
			for(StructureBlock strBlock : newStrBlocks)
			{
				if(strBlock.getOriginalMaterial() != Material.AIR) continue;
				strBlock.getLocation().getBlock().setType(Material.STAINED_GLASS);
				strBlock.getLocation().getBlock().setData((byte) (build.isObstructed() ? 14 : 5));
			}
		}

		usersToRemove.forEach(user -> building.remove(user));
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event)
	{
		if(!event.getMessage().contains(" ")) return;
		if(!event.getMessage().split(Pattern.quote(" "))[0].equalsIgnoreCase("/test")) return;
		event.setCancelled(true);

		int arg = Integer.parseInt(event.getMessage().split(Pattern.quote(" "))[1]);

		Player player = event.getPlayer();
		if(building.containsKey(arkManager.getArkUser(player)))
		{
			PlayerUtil.message(player, "BUILD", Color.Red + "Cancel your structure before placing another.");
			return;
		}

		if(arg == 1) building.put(arkManager.getArkUser(player), new Building(ArkMaterial.STRUCTURE_FOUNDATION_THATCH));
		else if (arg == 2) building.put(arkManager.getArkUser(player), new Building(ArkMaterial.STRUCTURE_WALL_THATCH));
		else if (arg == 3) building.put(arkManager.getArkUser(player), new Building(ArkMaterial.STRUCTURE_ROOF_THATCH));

		player.getInventory().setItem(0, new ItemStack(Material.WOOD_SPADE));
		player.getInventory().setItem(8, new ItemStack(Material.BARRIER));
		player.updateInventory();
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event)
	{
		if(event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) return;

		Player player = event.getPlayer();
		ArkUser user = arkManager.getArkUser(player);
		if(!building.containsKey(user)) return;

		event.setCancelled(true);
		Building build = building.get(user);

		if(event.getItem().getType() == Material.BARRIER)
		{
			build.setCancel(true);

			//TODO: Restore inventory
			player.getInventory().clear();
			player.updateInventory();
			return;
		}

		if(event.getItem().getType() == Material.WOOD_SPADE)
		{
			if(build.isObstructed())
			{
				PlayerUtil.message(player, "BUILD", ChatColor.RED + "Cannot place an obstructed structure");
				return;
			}

			build.getStructureBlocks().forEach(strBlock -> {
				strBlock.getLocation().getBlock().setType(build.getStructureMaterial().getMaterial());
			});

			build.clearStructureBlocks(false);
			building.remove(user);

			//TODO: Restore inventory
			player.getInventory().clear();
			player.updateInventory();
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event)
	{
		if(!building.containsKey(arkManager.getArkUser(event.getPlayer()))) return;
		event.setCancelled(true);
	}

	class ObstructionBlock {
		private Location location;
		private Material material;
		private byte data;

		public ObstructionBlock(Location location, Material material, byte data)
		{
			this.location = location;
			this.material = material;
			this.data = data;
		}
	}
}
