package com.teddehmc.ark.weapon;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum WeaponType
{
	FIST,
	STONE_PICKAXE,
	STONE_AXE,
	//TODO: More weapons/ Tools
}
