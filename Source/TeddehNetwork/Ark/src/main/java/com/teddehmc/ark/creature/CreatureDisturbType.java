package com.teddehmc.ark.creature;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum CreatureDisturbType
{
	CONTACT,
	IN_RANGE;
}
