package com.teddehmc.ark.util;

import com.teddehmc.ark.creature.CreatureStats;
import com.teddehmc.ark.creature.CreatureType;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CreatureUtil
{
	public static CreatureStats calculateStats(int level, CreatureType creatureType)
	{
		//TODO: Create stats based on level
		return new CreatureStats(0F, level, creatureType.getDefaultStats());
	}
}
