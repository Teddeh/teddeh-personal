package com.teddehmc.ark.creature;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CreatureStats
{
	protected float exp;
	protected int level, maxLevel = 120;
	protected double health, maxHealth;
	protected double food, maxFood;
	protected double stamina, maxStamina;
	protected double weight, maxWeight;
	protected double oxygen, maxOxygen;
	protected double melee;

	public CreatureStats(float exp, int level, double[] stats)
	{
		this.exp = exp;
		this.level = level;

		this.health = stats[0];
		this.maxHealth = stats[1];

		this.food = stats[2];
		this.maxFood = stats[3];

		this.stamina = stats[4];
		this.maxStamina = stats[5];

		this.weight = stats[6];
		this.maxWeight = stats[7];

		this.oxygen = stats[8];
		this.maxOxygen = stats[9];

		this.melee = stats[10];
	}
}
