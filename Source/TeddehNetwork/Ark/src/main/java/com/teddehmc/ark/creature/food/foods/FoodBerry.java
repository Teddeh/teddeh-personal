package com.teddehmc.ark.creature.food.foods;

import com.teddehmc.ark.creature.Creature;
import com.teddehmc.ark.creature.food.CreatureConsumable;
import com.teddehmc.ark.creature.food.Food;
import com.teddehmc.ark.creature.food.PlayerConsumable;
import com.teddehmc.ark.material.ArkMaterial;
import com.teddehmc.ark.user.ArkUser;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class FoodBerry extends Food implements PlayerConsumable, CreatureConsumable
{
	public FoodBerry(int id, String name)
	{
		super(
				id,             //ID
				name,           //Name
				0.1,            //Weight
				0.1,            //Food Regen
				0.05,           //Health Regen
				0.05            //Saturation
		);
	}

	@Override
	public void onPlayerConsume(ArkUser user) {}

	@Override
	public void onCreatureConsume(Creature creature) {}
}
