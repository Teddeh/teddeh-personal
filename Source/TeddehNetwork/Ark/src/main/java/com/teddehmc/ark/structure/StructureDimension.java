package com.teddehmc.ark.structure;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface StructureDimension
{
	int[][][][] getDimension();
}
