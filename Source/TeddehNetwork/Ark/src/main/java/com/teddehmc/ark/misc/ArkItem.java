package com.teddehmc.ark.misc;

import com.teddehmc.ark.attribute.UniqueAttribute;
import com.teddehmc.ark.attribute.WeightAttribute;
import org.bukkit.inventory.ItemStack;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class ArkItem extends ItemStack implements UniqueAttribute, WeightAttribute
{
	private int id;
	private String name;

	private double weight;

	public ArkItem(int id, String name, double weight)
	{
		this.id = id;
		this.name = name;
		this.weight = weight;
	}

	@Override
	public int getId()
	{
		return id;
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public double getWeight()
	{
		return weight;
	}
}
