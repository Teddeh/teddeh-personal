package com.teddehmc.ark.creature.food.foods;

import com.teddehmc.ark.creature.food.CreatureConsumable;
import com.teddehmc.ark.creature.food.Food;
import com.teddehmc.ark.creature.food.ForceConsumable;
import com.teddehmc.ark.creature.food.PlayerConsumable;
import com.teddehmc.ark.material.ArkMaterial;
import com.teddehmc.ark.misc.Controller;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Feces extends Food implements ForceConsumable
{
	public Feces(int id, String name, double weight, double foodRegen, double healthRegen, double saturation)
	{
		super(id, name, weight, foodRegen, healthRegen, saturation);
	}

	@Override
	public void onConsume(Controller controller) {}
}
