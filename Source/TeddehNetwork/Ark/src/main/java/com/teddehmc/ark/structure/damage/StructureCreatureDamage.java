package com.teddehmc.ark.structure.damage;

import com.teddehmc.ark.creature.Creature;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class StructureCreatureDamage extends StructureDamage
{
	private Creature creature;

	public StructureCreatureDamage(Creature creature, double damage)
	{
		super(damage);

		this.creature = creature;
	}

	public Creature getCreature()
	{
		return creature;
	}
}
