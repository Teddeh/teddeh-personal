package com.teddehmc.ark.structure.structures;

import com.teddehmc.ark.crafting.CraftingRecipe;
import com.teddehmc.ark.structure.ArkStructure;
import com.teddehmc.ark.structure.StructureMaterial;
import com.teddehmc.ark.structure.damage.StructureDamage;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class StructureWall extends ArkStructure
{
	public StructureWall(int id, String name, int durability, double weight, StructureMaterial structureMaterial, StructureDamage[] structureDamage, CraftingRecipe craftingRecipe)
	{
		super(id, name, durability, weight, structureMaterial,
				new int[][][][] {
						{{{1,1,1},{0,0,0},{0,0,0}},{{1,1,1},{0,0,0},{0,0,0}},{{1,1,1},{0,0,0},{0,0,0}}},
						{{{0,0,1},{0,0,1},{0,0,1}},{{0,0,1},{0,0,1},{0,0,1}},{{0,0,1},{0,0,1},{0,0,1}}}
				},
				structureDamage, craftingRecipe);
	}
}
