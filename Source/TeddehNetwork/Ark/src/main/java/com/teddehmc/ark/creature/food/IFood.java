package com.teddehmc.ark.creature.food;

import com.teddehmc.ark.material.ArkMaterial;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface IFood
{
	double getHealthRegeneration();
	double getFoodRegeneration();
	double getSaturation();
}
