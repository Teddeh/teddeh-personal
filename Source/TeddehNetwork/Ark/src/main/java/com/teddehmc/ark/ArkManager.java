package com.teddehmc.ark;

import com.teddehmc.ark.structure.StructureManager;
import com.teddehmc.ark.user.ArkUser;
import com.teddehmc.core.component.Component;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class ArkManager extends Component
{
	private HashMap<UUID, ArkUser> arkUsers;

	public ArkManager(JavaPlugin plugin)
	{
		super(plugin);

		arkUsers = new HashMap<>();

		new StructureManager(plugin, this).load();
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		createArkUser(event.getPlayer());
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		removeArkUser(event.getPlayer());
	}

	protected void createArkUser(Player player)
	{
		arkUsers.put(player.getUniqueId(), new ArkUser(player));
	}

	protected void removeArkUser(Player player)
	{
		arkUsers.remove(player.getUniqueId());
	}

	public ArkUser getArkUser(Player player)
	{
		return arkUsers.get(player.getUniqueId());
	}
}
