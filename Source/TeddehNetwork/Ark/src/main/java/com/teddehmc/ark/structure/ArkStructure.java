package com.teddehmc.ark.structure;

import com.teddehmc.ark.misc.ArkItem;
import com.teddehmc.ark.crafting.CraftingRecipe;
import com.teddehmc.ark.structure.damage.StructureDamage;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class ArkStructure extends ArkItem implements Structure
{
	private int durability, maxDurability;
	private double weight;
	private StructureMaterial structureMaterial;
	private int[][][][] dimensions;
	private StructureDamage[] structureDamage;
	private CraftingRecipe craftingRecipe;

	public ArkStructure(int id, String name, int durability, double weight, StructureMaterial structureMaterial, int[][][][] dimensions, StructureDamage[] structureDamage, CraftingRecipe craftingRecipe)
	{
		super(id, name, weight);

		this.durability = durability;
		this.maxDurability = durability;
		this.weight = weight;
		this.structureMaterial = structureMaterial;
		this.dimensions = dimensions;
		this.structureDamage = structureDamage;
		this.craftingRecipe = craftingRecipe;
	}

	@Override
	public double getWeight()
	{
		return weight;
	}

	@Override
	public int getItemDurability()
	{
		return durability;
	}

	@Override
	public CraftingRecipe getCraftingRecipe()
	{
		return craftingRecipe;
	}

	@Override
	public int getMaxDurability()
	{
		return maxDurability;
	}

	@Override
	public int[][][][] getDimension()
	{
		return dimensions;
	}

	public StructureMaterial getStructureMaterial()
	{
		return structureMaterial;
	}

	public StructureDamage[] getStructureDamage()
	{
		return structureDamage;
	}
}
