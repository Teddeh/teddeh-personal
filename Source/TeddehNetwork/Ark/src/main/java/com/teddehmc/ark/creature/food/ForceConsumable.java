package com.teddehmc.ark.creature.food;

import com.teddehmc.ark.misc.Controller;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface ForceConsumable
{
	void onConsume(Controller controller);
}
