package com.teddehmc.ark.attribute;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface UniqueAttribute
{
	int getId();
	String getName();
}
