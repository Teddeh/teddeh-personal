package com.teddehmc.ark.creature.food.event;

import com.teddehmc.ark.creature.food.IFood;
import com.teddehmc.ark.misc.Controller;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class ArkFoodConsumeEvent extends Event implements Cancellable
{
	private Controller controller;
	private IFood food;

	public ArkFoodConsumeEvent(Controller controller, IFood food)
	{
		this.controller = controller;
		this.food = food;
	}

	public Controller getController()
	{
		return controller;
	}

	public IFood getFood()
	{
		return food;
	}

	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers()
	{
		return handlers;
	}

	public static HandlerList getHandlerList()
	{
		return handlers;
	}

	@Override
	public boolean isCancelled()
	{
		return false;
	}

	@Override
	public void setCancelled(boolean b)
	{

	}
}
