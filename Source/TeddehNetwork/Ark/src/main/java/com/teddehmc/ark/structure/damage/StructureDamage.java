package com.teddehmc.ark.structure.damage;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class StructureDamage implements IStructureDamage
{
	private double damage;

	public StructureDamage(double damage)
	{
		this.damage = damage;
	}

	@Override
	public double getDamage()
	{
		return damage;
	}
}
