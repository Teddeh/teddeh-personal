package com.teddehmc.ark.creature;

import com.teddehmc.ark.misc.Controller;
import com.teddehmc.ark.util.CreatureUtil;

import java.util.UUID;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class Creature extends Controller implements ICreature
{
	protected CreatureStats creatureStats;

	public Creature(int level, CreatureType creatureType)
	{
		this.creatureStats = CreatureUtil.calculateStats(level, creatureType);
	}

	public CreatureStats getCreatureStats()
	{
		return creatureStats;
	}

	@Override
	public float getExp()
	{
		return creatureStats.exp;
	}

	@Override
	public int getLevel()
	{
		return creatureStats.level;
	}

	@Override
	public int getMaxLevel()
	{
		return creatureStats.maxLevel;
	}

	@Override
	public double getHealth()
	{
		return creatureStats.health;
	}

	@Override
	public double getMaxHealth()
	{
		return creatureStats.maxHealth;
	}

	@Override
	public double getFood()
	{
		return creatureStats.food;
	}

	@Override
	public double getMaxFood()
	{
		return creatureStats.maxFood;
	}

	@Override
	public double getStamina()
	{
		return creatureStats.stamina;
	}

	@Override
	public double getMaxStamina()
	{
		return creatureStats.maxStamina;
	}

	@Override
	public double getWeight()
	{
		return creatureStats.weight;
	}

	@Override
	public double getMaxWeight()
	{
		return creatureStats.maxWeight;
	}

	@Override
	public double getOxygen()
	{
		return creatureStats.oxygen;
	}

	@Override
	public double getMaxOxygen()
	{
		return creatureStats.maxOxygen;
	}

	@Override
	public double getMeleeDamage()
	{
		return creatureStats.melee;
	}
}
