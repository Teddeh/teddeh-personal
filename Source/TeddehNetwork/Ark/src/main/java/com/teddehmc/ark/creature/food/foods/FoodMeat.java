package com.teddehmc.ark.creature.food.foods;

import com.teddehmc.ark.creature.Creature;
import com.teddehmc.ark.creature.food.CreatureConsumable;
import com.teddehmc.ark.creature.food.Food;
import com.teddehmc.ark.creature.food.PlayerConsumable;
import com.teddehmc.ark.material.ArkMaterial;
import com.teddehmc.ark.user.ArkUser;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class FoodMeat extends Food implements PlayerConsumable, CreatureConsumable
{
	public FoodMeat(int id, String name, double weight, double foodRegen, double healthRegen, double saturation)
	{
		super(id, name, weight, foodRegen, healthRegen, saturation);
	}

	@Override
	public void onCreatureConsume(Creature creature) {}

	@Override
	public void onPlayerConsume(ArkUser user) {}
}
