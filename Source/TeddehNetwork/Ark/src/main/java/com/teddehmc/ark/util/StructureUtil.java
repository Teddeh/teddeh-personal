package com.teddehmc.ark.util;

import com.teddehmc.ark.user.ArkUser;
import com.teddehmc.ark.util.location.LocationUtil;
import com.teddehmc.core.util.ServerUtil;
import org.bukkit.block.BlockFace;

import java.util.Arrays;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class StructureUtil
{
	public static int[][][] adaptToDirection(int[][][][] current, ArkUser user)
	{
		BlockFace face = LocationUtil.yawToDirection(user.getPlayer());
		int index = (face == BlockFace.NORTH || face == BlockFace.SOUTH) ? 0 : 1;
		if (face == BlockFace.SOUTH || face == BlockFace.WEST) return current[index];

		int[][][] clone = cloneArray(current[index]);
		for (int x = 0; x < clone.length; x++)
		{
			if (index == 0)
			{
				int[] temp = clone[x][0];
				clone[x][0] = clone[x][2];
				clone[x][2] = temp;
			}
			for (int z = 0; z < clone[x].length; z++)
			{
				for (int i = 0; i < clone[x][z].length / 2; i++)
				{
					int temp = clone[x][z][i];
					clone[x][z][i] = clone[x][z][clone[x][z].length - i - 1];
					clone[x][z][clone[x][z].length - i - 1] = temp;
				}
			}
		}
		return clone;
	}

	private static int[][][] cloneArray(int[][][] current)
	{
		int[][][] clone = new int[3][3][3];
		for(int x = 0; x < current.length; x++)
		{
			for(int y = 0; y < current[x].length; y++)
			{
				for(int z = 0; z < current[x][y].length; z++)
				{
//					clone[x][y] = Arrays.copyOf(current[x][y], 3);
					clone[x][y][z] = current[x][y][z];
				}
			}
		}

		return clone;
	}

	//TODO: Debug
	private static void print(int[][][] i)
	{
		for(int y = 0; y < i.length; y++)
		{
			String str = "{";
			for(int x = 0; x < i[y].length; x++)
			{
				for(int z = 0; z < i[y][x].length; z++)
				{
					if(z == 0) str += " {";
					str += i[y][x][z] + (z == (i[y][x].length-1) ? "" : ",");
					if(z == (i[y][x].length-1)) str += "}";
				}
			}
			ServerUtil.log(str + " }");
		}
	}
}
