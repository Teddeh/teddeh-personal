package com.teddehmc.ark.weapon;

import com.teddehmc.ark.crafting.CraftingRecipe;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class Tool extends Weapon
{
	public Tool(int id, String name, int durability, double weight, int damage, CraftingRecipe craftingRecipe)
	{
		super(id, name, durability, weight, damage, craftingRecipe);
	}
}
