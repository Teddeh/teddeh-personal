package com.teddehmc.ark.creature.food;

import com.teddehmc.ark.ArkManager;
import com.teddehmc.ark.creature.food.event.ArkFoodConsumeEvent;
import com.teddehmc.core.component.Component;
import org.bukkit.Bukkit;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class FoodManager extends Component
{
	private ArkManager arkManager;

	public FoodManager(JavaPlugin plugin, ArkManager arkManager)
	{
		super(plugin);

		this.arkManager = arkManager;
	}

	public void onPlayerBukkitConsume(PlayerItemConsumeEvent event)
	{
		if(!(event.getItem() instanceof IFood)) return;

		IFood food = (IFood) event.getItem();

		if(!event.isCancelled()) event.setCancelled(true);

		ArkFoodConsumeEvent arkFoodConsumeEvent = new ArkFoodConsumeEvent(arkManager.getArkUser(event.getPlayer()), food);
		Bukkit.getPluginManager().callEvent(arkFoodConsumeEvent);
	}
}
