package com.teddehmc.ark;

import com.teddehmc.core.IModule;
import com.teddehmc.core.ModuleState;
import com.teddehmc.core.Teddeh;
import com.teddehmc.core.util.ServerUtil;
import org.bukkit.Bukkit;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
@IModule(name = "Ark", moduleState = ModuleState.DEVELOPMENT)
public class Ark extends Teddeh
{
	private ArkManager arkManager;

	@Override
	public void load()
	{
		//Ark Manager
		arkManager = new ArkManager(this);
		Bukkit.getOnlinePlayers().forEach(player -> arkManager.createArkUser(player));//this is for /reload support (when testing)



		//TODO: This is just for testing purposes, remove once properly implemented.
		ServerUtil.log(getClass().getAnnotation(IModule.class).name());
		ServerUtil.log(getClass().getAnnotation(IModule.class).moduleState().toString());
	}

	@Override
	public void disable()
	{
		//This is for /reload support (when testing)
		Bukkit.getOnlinePlayers().forEach(player -> arkManager.removeArkUser(player));
	}
}
