package com.teddehmc.ark.material;

import com.teddehmc.ark.misc.ArkItem;
import com.teddehmc.ark.creature.Creature;
import com.teddehmc.ark.creature.food.foods.FoodBerry;
import com.teddehmc.ark.creature.food.foods.FoodVegetable;
import com.teddehmc.ark.structure.StructureMaterial;
import com.teddehmc.ark.structure.structures.StructureFoundation;
import com.teddehmc.ark.structure.structures.StructureWall;
import com.teddehmc.ark.user.ArkUser;
import org.bukkit.Material;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum ArkMaterial
{
	//TODO: BERRY (Change Material types to the according models)
	FOOD_AMARBERRY("food/berry/AMARBERRY", Material.APPLE, new FoodBerry(0, "Amarberry")),
	FOOD_AZULBERRY("food/berry/AZULBERRY", Material.APPLE, new FoodBerry(0, "Azulberry")),
	FOOD_MEJOBERRY("food/berry/MEJOBERRY", Material.APPLE, new FoodBerry(0, "Mejoberry")),
	FOOD_TINTOBERRY("food/berry/TINTOBERRY", Material.APPLE, new FoodBerry(0, "Tintoberry")),
	FOOD_STIMBERRY("food/berry/STIMBERRY", Material.APPLE, new FoodBerry(0, "Stimberry") {
		@Override
		public void onPlayerConsume(ArkUser user)
		{
			//Bring down torpor if > 0
		}

		@Override
		public void onCreatureConsume(Creature creature)
		{
			//Bring down torpor if > 0
		}
	}),
	FOOD_NARCOBERRY("food/berry/NARCOBERRY", Material.APPLE, new FoodBerry(0, "Narcoberry") {
		@Override
		public void onPlayerConsume(ArkUser user)
		{
			//Give x torpor if < max
		}

		@Override
		public void onCreatureConsume(Creature creature)
		{
			//Give x torpor if < max
		}
	}),

	//TODO: MEAT (Change Material types to the according models)
	FOOD_RAW_MEAT("food/meat/RAW_MEAT", Material.APPLE),
	FOOD_COOKED_MEAT("food/meat/COOKED_MEAT", Material.APPLE),
	FOOD_COOKED_MEAT_JERKY("food/meat/COOKED_MEAT_JERKY", Material.APPLE),
	FOOD_RAW_PRIME_MEAT("food/meat/RAW_PRIME_MEAT", Material.APPLE),
	FOOD_COOKED_PRIME_MEAT("food/meat/COOKED_PRIME_MEAT", Material.APPLE),
	FOOD_PRIME_MEAT_JERKY("food/meat/PRIME_MEAT_JERKY", Material.APPLE),
	FOOD_RAW_FISH_MEAT("food/meat/RAW_FISH_MEAT", Material.APPLE),
	FOOD_COOKED_FISH_MEAT("food/meat/COOKED_FISH_MEAT", Material.APPLE),
	FOOD_RAW_PRIME_FISH_MEAT("food/meat/RAW_PRIME_FISH_MEAT", Material.APPLE),
	FOOD_COOKED_PRIME_FISH_MEAT("food/meat/COOKED_PRIME_FISH_MEAT", Material.APPLE),
	FOOD_SPOILED_MEAT("food/meat/SPOILED_MEAT", Material.APPLE),

	//TODO: VEGETABLE (Change Material types to the according models)
	FOOD_CITRONAL("food/vegetable/CITRONAL", Material.APPLE, new FoodVegetable(0, "Citronal", 4, 2.5, 6)),
	FOOD_CORN("food/vegetable/CORN", Material.APPLE, new FoodVegetable(0, "Corn", 4, 3, 6)),
	FOOD_CARROT("food/vegetable/CARROT", Material.APPLE, new FoodVegetable(0, "Carrot", 3.5, 3, 5)),
	FOOD_POTATO("food/vegetable/POTATO", Material.APPLE, new FoodVegetable(0, "Potato", 5, 2.5, 6.5)),

	//TODO: EGG (Change Material types to the according models)
	FOOD_ANKYLO_EGG("food/egg/ANKYLO_EGG", Material.APPLE),
	FOOD_ARANEO_EGG("food/egg/ARANEO_EGG", Material.APPLE),
	FOOD_ARGENTAVIS_EGG("food/egg/ARGENTAVIS_EGG", Material.APPLE),
	FOOD_BRONTO_EGG("food/egg/BRONTO_EGG", Material.APPLE),
	FOOD_TURTLE_EGG("food/egg/TURTLE_EGG", Material.APPLE),
	FOOD_CARNO_EGG("food/egg/CARNO_EGG", Material.APPLE),
	FOOD_COMPY_EGG("food/egg/COMPY_EGG", Material.APPLE),
	FOOD_DILO_EGG("food/egg/DILO_EGG", Material.APPLE),
	FOOD_DIMETRODON_EGG("food/egg/DIMETRODON_EGG", Material.APPLE),
	FOOD_DIMORPH_EGG("food/egg/DIMORPH_EGG", Material.APPLE),
	FOOD_DODO_EGG("food/egg/DODO_EGG", Material.APPLE),
	FOOD_GALLIMIMUS_EGG("food/egg/GALLIMIMUS_EGG", Material.APPLE),
	FOOD_GIGANOTOSAURUS_EGG("food/egg/GIGANOTOSAURUS_EGG", Material.APPLE),
	FOOD_KAIRUKU_EGG("food/egg/KAIRUKU_EGG", Material.APPLE),
	FOOD_LYSTRO_EGG("food/egg/LYSTRO_EGG", Material.APPLE),
	FOOD_OVIRAPTOR_EGG("food/egg/OVIRAPTOR_EGG", Material.APPLE),
	FOOD_PACHY_EGG("food/egg/PACHY_EGG", Material.APPLE),
	FOOD_PARASAUR_EGG("food/egg/PARASAUR_EGG", Material.APPLE),
	FOOD_PTERANODON_EGG("food/egg/PTERANODON_EGG", Material.APPLE),
	FOOD_PULMONOSCORPIUS_EGG("food/egg/PULMONOSCORPIUS_EGG", Material.APPLE),
	FOOD_QUETZAL_EGG("food/egg/QUETZAL_EGG", Material.APPLE),
	FOOD_RAPTOR_EGG("food/egg/RAPTOR_EGG", Material.APPLE),
	FOOD_TREX_EGG("food/egg/TREX_EGG", Material.APPLE),
	FOOD_SARCO_EGG("food/egg/SARCO_EGG", Material.APPLE),
	FOOD_SPINO_EGG("food/egg/SPINO_EGG", Material.APPLE),
	FOOD_STEGO_EGG("food/egg/STEGO_EGG", Material.APPLE),
	FOOD_TERRORBIRD_EGG("food/egg/TERRORBIRD_EGG", Material.APPLE),
	FOOD_TITANOBOA_EGG("food/egg/TITANOBOA_EGG", Material.APPLE),
	FOOD_TRIKE_EGG("food/egg/TRIKE_EGG", Material.APPLE),

	//TODO: KIBBLE (Change Material types to the according models)
	FOOD_ANKYLO_KIBBLE("food/kibble/ANKYLO_KIBBLE", Material.APPLE),
	FOOD_ARANEO_KIBBLE("food/kibble/ARANEO_KIBBLE", Material.APPLE),
	FOOD_ARGENTAVIS_KIBBLE("food/kibble/ARGENTAVIS_KIBBLE", Material.APPLE),
	FOOD_BRONTO_KIBBLE("food/kibble/BRONTO_KIBBLE", Material.APPLE),
	FOOD_TURTLE_KIBBLE("food/kibble/TURTLE_KIBBLE", Material.APPLE),
	FOOD_CARNO_KIBBLE("food/kibble/CARNO_KIBBLE", Material.APPLE),
//	FOOD_COMPY_KIBBLE("food/kibble/COMPY_KIBBLE", null),
	FOOD_DILO_KIBBLE("food/kibble/DILO_KIBBLE", Material.APPLE),
	FOOD_DIMETRODON_KIBBLE("food/kibble/DIMETRODON_KIBBLE", Material.APPLE),
	FOOD_DIMORPH_KIBBLE("food/kibble/DIMORPH_KIBBLE", Material.APPLE),
	FOOD_DODO_KIBBLE("food/kibble/DODO_KIBBLE", Material.APPLE),
	FOOD_GALLIMIMUS_KIBBLE("food/kibble/GALLIMIMUS_KIBBLE", Material.APPLE),
//	FOOD_GIGANOTOSAURUS_KIBBLE("food/kibble/GIGANOTOSAURUS_KIBBLE", null),
	FOOD_KAIRUKU_KIBBLE("food/kibble/KAIRUKU_KIBBLE", Material.APPLE),
//	FOOD_LYSTRO_KIBBLE("food/kibble/LYSTRO_KIBBLE", null),
//	FOOD_OVIRAPTOR_KIBBLE("food/kibble/OVIRAPTOR_KIBBLE", null),
	FOOD_PACHY_KIBBLE("food/kibble/PACHY_KIBBLE", Material.APPLE),
	FOOD_PARASAUR_KIBBLE("food/kibble/PARASAUR_KIBBLE", Material.APPLE),
	FOOD_PTERANODON_KIBBLE("food/kibble/PTERANODON_KIBBLE", Material.APPLE),
	FOOD_PULMONOSCORPIUS_KIBBLE("food/kibble/PULMONOSCORPIUS_KIBBLE", Material.APPLE),
	FOOD_QUETZAL_KIBBLE("food/kibble/QUETZAL_KIBBLE", Material.APPLE),
	FOOD_RAPTOR_KIBBLE("food/kibble/RAPTOR_KIBBLE", Material.APPLE),
	FOOD_TREX_KIBBLE("food/kibble/TREX_KIBBLE", Material.APPLE),
	FOOD_SARCO_KIBBLE("food/kibble/SARCO_KIBBLE", Material.APPLE),
	FOOD_SPINO_KIBBLE("food/kibble/SPINO_KIBBLE", Material.APPLE),
	FOOD_STEGO_KIBBLE("food/kibble/STEGO_KIBBLE", Material.APPLE),
	FOOD_TERRORBIRD_KIBBLE("food/kibble/TERRORBIRD_KIBBLE", Material.APPLE),
	FOOD_TITANOBOA_KIBBLE("food/kibble/TITANOBOA_KIBBLE", Material.APPLE),
	FOOD_TRIKE_KIBBLE("food/kibble/TRIKE_KIBBLE", Material.APPLE),

	//TODO: FECES (Change Material types to the according models)
	FOOD_HUMAN_FECES("food/feces/HUMAN_FECES", Material.APPLE),
	FOOD_SMALL_FECES("food/feces/SMALL_FECES", Material.APPLE),
	FOOD_MEDIUM_FECES("food/feces/MEDIUM_FECES", Material.APPLE),
	FOOD_LARGE_FECES("food/feces/LARGE_FECES", Material.APPLE),

	STRUCTURE_FOUNDATION_THATCH("structure/thatch/FOUNDATION_THATCH", Material.STONE, new StructureFoundation(0, "Thatch Foundation", 0, 0, StructureMaterial.THATCH, null, null)),
	STRUCTURE_WALL_THATCH("structure/thatch/FOUNDATION_THATCH", Material.WOOD, new StructureWall(0, "Thatch Wall", 0, 0, StructureMaterial.THATCH, null, null)),
	STRUCTURE_ROOF_THATCH("structure/thatch/ROOF_THATCH", Material.WOOD, new StructureWall(0, "Thatch Wall", 0, 0, StructureMaterial.THATCH, null, null)),

	WEAPON_FIST("weapon/FIST", Material.APPLE);

	private String pathId;
	private Material material;
	private ArkItem arkItem;

	ArkMaterial(String pathId, Material material, ArkItem arkItem)
	{
		this.pathId = pathId;
		this.material = material;
		this.arkItem = arkItem;
	}

	@Deprecated //TODO: Remove once all materials are complete.
	ArkMaterial(String pathId, Material material)
	{
		this.pathId = pathId;
		this.material = material;
	}

	public String getPathId()
	{
		return pathId;
	}

	public Material getMaterial()
	{
		return material;
	}

	public ArkItem getArkItem()
	{
		return arkItem;
	}
}
