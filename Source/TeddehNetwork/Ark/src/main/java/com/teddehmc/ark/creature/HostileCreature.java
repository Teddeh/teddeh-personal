package com.teddehmc.ark.creature;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface HostileCreature
{
	CreatureDisturbType getDisturbType();
}
