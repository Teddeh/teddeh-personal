package com.teddehmc.ark.util.location;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * Created: 05/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class ArkLocation<X extends Number, Y extends Number, Z extends Number>
{
	private X xCoord;
	private Y yCoord;
	private Z zCoord;

	public ArkLocation(X xCoord, Y yCoord, Z zCoord)
	{
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.zCoord = zCoord;
	}

	public Number getX()
	{
		return xCoord;
	}

	public Number getY()
	{
		return yCoord;
	}

	public Number getZ()
	{
		return zCoord;
	}

	public String toString()
	{
		return getX() + ";" + getY() + ";" + getZ();
	}

	public String toString(String splitSymbol)
	{
		return getX() + splitSymbol + getY() + splitSymbol + getZ();
	}

	public Location toBukkitLocation()
	{
		return toBukkitLocation("world");
	}

	public Location toBukkitLocation(String world)
	{
		return toBukkitLocation(Bukkit.getWorld(world));
	}

	public Location toBukkitLocation(World world)
	{
		return new Location(world, (double) getX(), (double) getY(), (double) getZ());
	}
}
