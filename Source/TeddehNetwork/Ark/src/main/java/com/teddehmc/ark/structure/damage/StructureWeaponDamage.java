package com.teddehmc.ark.structure.damage;

import com.teddehmc.ark.weapon.Weapon;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class StructureWeaponDamage extends StructureDamage
{
	private Weapon weapon;

	public StructureWeaponDamage(Weapon weapon, double damage)
	{
		super(damage);

		this.weapon = weapon;
	}

	public Weapon getWeapon()
	{
		return weapon;
	}
}
