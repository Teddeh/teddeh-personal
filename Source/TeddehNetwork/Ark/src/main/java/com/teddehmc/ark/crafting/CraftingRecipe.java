package com.teddehmc.ark.crafting;

import com.teddehmc.ark.material.ArkMaterial;

/**
 * Created: 06/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CraftingRecipe
{
	private int requiredLevel = 1;
	private double expReward;
	private ArkMaterial[] arkMaterials;

	public CraftingRecipe(int requiredLevel, double expReward, ArkMaterial[] arkMaterials)
	{
		this.requiredLevel = requiredLevel;
		this.expReward = expReward;
		this.arkMaterials = arkMaterials;
	}

	public int getRequiredLevel()
	{
		return requiredLevel;
	}

	public double getExpReward()
	{
		return expReward;
	}

	public ArkMaterial[] getArkMaterials()
	{
		return arkMaterials;
	}
}
