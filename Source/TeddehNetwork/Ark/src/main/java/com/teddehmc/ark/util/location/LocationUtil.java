package com.teddehmc.ark.util.location;

import com.teddehmc.core.util.ServerUtil;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class LocationUtil
{
	private static final BlockFace[] axis = { BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST };

	public static BlockFace yawToDirection(Player player)
	{
		float yaw = player.getLocation().getYaw();
//		BlockFace face = BlockFace.NORTH;
//
//		if (yaw > 135 && yaw <= -135) face = BlockFace.NORTH;
//		else if (yaw > -135 && yaw <= -45) face = BlockFace.EAST;
//		else if (yaw > -45 && yaw <= 45) face = BlockFace.SOUTH;
//		else if (yaw > 45 && yaw <= 135) face = BlockFace.WEST;
//
//		return face;

		return axis[Math.round(yaw / 90f) & 0x3];
	}
}
