package com.teddehmc.ark.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created: 05/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CommonExpressions
{
	/**
	 * Checks if a string contains a valid ip address.
	 *
	 * @param string
	 * @param ranged Checks if the IP is ranged, xxx.xxx.xxx.*
	 * @return
	 */
	public boolean containsIP(String string, boolean ranged)
	{
		Pattern pattern = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])" + (ranged ? "|(:?*)" : "") + ")$");
		Matcher matcher = pattern.matcher(string);
		return matcher.find();
	}

	public List<String> containsEmail(String text)
	{
		List<String> found = new ArrayList<>();
		Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(text);
		while(matcher.find())
		{
			found.add(matcher.group(1));
		}

		return found;
	}
}
