package com.teddehmc.ark.creature;

import com.teddehmc.ark.creature.creatures.CreatureTREX;

/**
 * Created: 04/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum CreatureType
{
	TREX(1, "T-Rex", true, CreatureTREX.class, new double[]
			{
					1000D,      //Health
					1200D,      //Food
					500D,       //Stamina
					500D,       //Weight
					100D,       //Oxygen
					100D        //Melee Damage (Percentage)
			});

	private int id;
	private String name;
	private boolean alphaCompatible;

	private double[] defaultStats;

	private Class<? extends Creature> parent;

	CreatureType(int id, String name, boolean alphaCompatible, Class<? extends Creature> parent, double[] defaultStats)
	{
		this.id = id;
		this.name = name;
		this.alphaCompatible = alphaCompatible;
		this.parent = parent;
		this.defaultStats = defaultStats;
	}

	public double[] getDefaultStats()
	{
		return defaultStats;
	}

	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public boolean isAlphaCompatible()
	{
		return alphaCompatible;
	}

	public Class<? extends Creature> getParentClass()
	{
		return parent;
	}
}
