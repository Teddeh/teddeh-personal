package com.teddehmc.ark.structure;

import com.teddehmc.ark.attribute.*;

/**
 * Created: 08/06/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface Structure extends UniqueAttribute, WeightAttribute, DurabilityAttribute, RepairAttribute, CraftableAttribute, StructureDimension
{
}
