package org.mccentral.game;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.mccentral.core.component.player.NameTag;
import org.mccentral.core.database.MCCPlayer;
import org.mccentral.core.locale.LocaleMessage;
import org.mccentral.core.util.*;
import org.mccentral.game.command.GameCommand;
import org.mccentral.game.component.game.CentralGame;
import org.mccentral.game.component.game.countdown.Countdown;
import org.mccentral.game.component.game.countdown.CountdownDisplay;
import org.mccentral.game.component.game.map.MapDataModule;
import org.mccentral.game.component.game.map.MapModule;
import org.mccentral.game.component.game.player.GamePlayer;
import org.mccentral.game.component.game.player.state.PlayerState;
import org.mccentral.game.component.game.player.state.PlayerStateChangeEvent;
import org.mccentral.game.component.game.scoreboard.CentralScoreboard;
import org.mccentral.game.component.game.state.GameState;
import org.mccentral.game.component.game.state.GameStateChangeEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class GameManager implements Listener
{
	private List<GamePlayer> gamePlayers;
	private final Game game;
	private GameState gameState;
	private CentralGame currentGame;
	private MapDataModule mapDataModule;

	private Countdown countdown = null;

	public GameManager(Game game)
	{
		this.game = game;
		Bukkit.getPluginManager().registerEvents(this, game);

		this.gamePlayers = new ArrayList<>();
		new GameCommand(game, this);
	}

	public void initialise(CentralGame game)
	{
		currentGame = game;
		ServerUtil.log(Level.INFO, game.getId() + " - " + game.getName());

		MapModule mapModule = new MapModule(currentGame.getGameType());
		String map = mapModule.loadRandomMap();
		mapDataModule = new MapDataModule(map);

		setGameState(GameState.WAITING);
		game.initialise();
		game.scoreboard();
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		Player player = event.getPlayer();
		player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
		player.teleport(new Location(Bukkit.getWorld("world"), 0, 51, 0));
		addGamePlayer(player);

		MCCPlayer mccPlayer = game.getPlayer(player.getUniqueId());

		//TODO: Testing Locale
		Messenger.message(mccPlayer, LocaleMessage.GAME_JOINED);

		Rank rank = mccPlayer.getRank();
		ServerUtil.log(Level.WARNING, rank.toString());
		if(rank == null) rank = Rank.MEMBER;
		new NameTag(
				player,
				rank.getName(false) == null ? "" : Colour.translate(rank.getRankColor(true)) + rank.getName(true) + Colour.Reset + " ",
				" " + Colour.Gray + "[suffix]",
				rank,
				Rank.values()
		);

		switch (gameState)
		{
			case WAITING:
				getGamePlayer(player).setPlayerState(PlayerState.LOBBY);
				if (Bukkit.getOnlinePlayers().size() >= 1)//TODO: Change
				{
					if (countdown != null) return;

					setGameState(GameState.COUNTDOWN);
					countdown = new Countdown(this)
					{
						@Override
						public void finish()
						{
							setGameState(GameState.LOADING);
						}
					}.setStartingTime(20);

					countdown.setDisplay(new CountdownDisplay(20, "Game starting in %s seconds."));
					countdown.setDisplay(new CountdownDisplay(10, "Game starting in %s seconds.")
					{
						@Override
						public void display(Player player, String message)
						{

						}
					});

					for (Integer i : new int[]{1, 2, 3, 4, 5})
					{
						String str = Colour.Red;
						if (i == 3 || i == 2) str = Colour.Yellow;
						else if (i == 1) str = Colour.Green;
						final String color = str;
						countdown.setDisplay(new CountdownDisplay(i, "%s")
						{
							@Override
							public void display(Player player, String message)
							{
								PlayerUtil.sendTitle(player, color + message);
								PlayerUtil.sendSubtitle(player, Colour.Gray + "Game starting in..");
							}
						});
					}

					countdown.setDisplay(new CountdownDisplay(0, "Game has started!")
					{
						@Override
						public void display(Player player, String message)
						{
							PlayerUtil.sendTitle(player, Colour.Green + message);
						}
					});

					countdown.start();
				}
				break;
		}
	}

	@EventHandler
	public void onStateChange(GameStateChangeEvent event)
	{
		if (event.getTo() == null)
		{
			ServerUtil.log(Level.WARNING, "GameState -TO- is null.");
			return;
		}

		CentralScoreboard board = currentGame.getScoreboard(event.getTo());
		if (board != null)
		{
			Bukkit.getOnlinePlayers().forEach(player -> player.setScoreboard(board.getScoreboard()));
			ServerUtil.log(Level.INFO, event.getTo().toString() + " BOARD: " + board.getScoreboard().getEntries().size());
		} else
		{
			ServerUtil.log(Level.INFO, event.getTo().toString() + " BOARD: null");
		}

		switch (event.getTo())
		{
			case LOADING:
				int i = 0;
				Location loc;
				for (Player all : Bukkit.getOnlinePlayers())
				{
					if (i >= mapDataModule.getSpawnLocations("BLACK").size()) i = 0;
					loc = mapDataModule.getSpawnLocations("BLACK").get(i++);
					Location dir = LocationUtil.lookAt(loc.add(0.5, 0, 0.5), new Location(loc.getWorld(), 0.5, 50, 0.5), true, false);
					all.teleport(dir);
				}

				countdown = new Countdown(this)
				{
					@Override
					public void finish()
					{
						setGameState(GameState.INGAME);
					}
				}.setStartingTime(5);
				for (Integer x : new int[]{1, 2, 3, 4, 5})
				{
					String str = Colour.Red;
					if (x == 3 || x == 2) str = Colour.Yellow;
					else if (x == 1) str = Colour.Green;
					final String color = str;
					countdown.setDisplay(new CountdownDisplay(x, "%s")
					{
						@Override
						public void display(Player player, String message)
						{
							PlayerUtil.sendTitle(player, color + message);
							PlayerUtil.sendSubtitle(player, Colour.Gray + "Game starting in..");
						}
					});
				}

				countdown.setDisplay(new CountdownDisplay(0, "Game has started!")
				{
					@Override
					public void display(Player player, String message)
					{
						PlayerUtil.sendTitle(player, " ");
						PlayerUtil.sendSubtitle(player, " ");
					}
				});

				countdown.start();
				break;

			case INGAME:
				gamePlayers.forEach(gp -> gp.setPlayerState(PlayerState.PLAYING));
				ServerUtil.log(Level.INFO, "Gamestate = RUNNING.");
				break;

			case ENDING:
				gamePlayers.forEach(gp -> gp.setPlayerState(PlayerState.LOBBY));
				Bukkit.getOnlinePlayers().forEach(p -> p.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard()));
				WorldUtil.unloadWorld(mapDataModule.getMap(), false);

				new BukkitRunnable()
				{
					@Override
					public void run()
					{
						FileUtil.deleteFile(new File(Bukkit.getWorldContainer().getAbsolutePath() + "/" + mapDataModule.getMap()));
					}
				}.runTaskLater(game, 20);
				break;
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		NameTag.removePlayerTag(event.getPlayer());
	}

	public void stop()
	{
		Location location = new Location(Bukkit.getWorld("world"), 0, 51, 0);
		Bukkit.getOnlinePlayers().forEach(player -> player.teleport(location));
		setGameState(GameState.ENDING);
		countdown.stop();
		countdown = null;
	}

	public void setGameState(GameState gameState)
	{
		GameStateChangeEvent event = new GameStateChangeEvent((this.gameState == null ? GameState.ENDING : this.gameState), gameState);
		this.gameState = gameState;
		Bukkit.getPluginManager().callEvent(event);
	}

	public void setDefaultScoreboard(GameState gameState)
	{
		CentralScoreboard board = new CentralScoreboard("SomeTitle");
		switch (gameState)
		{
			case WAITING:
				break;

			case COUNTDOWN:
				break;

			case INGAME:
				break;
		}
	}

	public void addGamePlayer(Player player)
	{
		if(getGamePlayer(player) != null) return;
		gamePlayers.add(new GamePlayer(player.getName(), player.getUniqueId()));
	}

	public void removeGamePlayer(Player player)
	{
		GamePlayer gamePlayer = getGamePlayer(player);
		if(gamePlayer == null) return;
		gamePlayers.remove(gamePlayer);
	}

	public GamePlayer getGamePlayer(Player player)
	{
		for(GamePlayer gamePlayer : gamePlayers)
		{
			if(!gamePlayer.getUuid().equals(player.getUniqueId())) continue;
			return gamePlayer;
		}

		return null;
	}

	public void setPlayerState(Player player, PlayerState playerState)
	{
		GamePlayer gamePlayer = getGamePlayer(player);
		gamePlayer.setPlayerState(playerState);
		PlayerStateChangeEvent event = new PlayerStateChangeEvent(player, playerState);
		Bukkit.getPluginManager().callEvent(event);
	}

	public List<GamePlayer> getGamePlayers()
	{
		return gamePlayers;
	}

	public List<GamePlayer> getAlivePlayer()
	{
		List<GamePlayer> temp = new ArrayList<>();
		for(GamePlayer gamePlayer : gamePlayers)
		{
			if(gamePlayer.getPlayerState() != PlayerState.PLAYING) continue;
			temp.add(gamePlayer);
		}

		return temp;
	}

	public GameState getGameState()
	{
		return gameState;
	}

	public MapDataModule getMapDataModule()
	{
		return mapDataModule;
	}

	public Game getGame()
	{
		return game;
	}
}
