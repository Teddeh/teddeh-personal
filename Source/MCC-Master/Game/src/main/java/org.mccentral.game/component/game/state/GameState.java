package org.mccentral.game.component.game.state;

/**
 * Created by Teddeh on 12/04/2016.
 */
public enum GameState
{
	WAITING,
	COUNTDOWN,
	LOADING,
	INGAME,
	ENDING;
}
