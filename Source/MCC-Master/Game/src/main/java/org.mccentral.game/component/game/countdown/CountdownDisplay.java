package org.mccentral.game.component.game.countdown;

import org.bukkit.entity.Player;
import org.mccentral.core.util.PlayerUtil;

/**
 * Created by Teddeh on 15/04/2016.
 */
public class CountdownDisplay
{
	private int tick;
	private String display;

	public CountdownDisplay(int tick, String display)
	{
		this.tick = tick;
		this.display = display.replace("%s", ""+tick);
	}

	public void display(Player player, String message)
	{
		PlayerUtil.message(player, message);
	}

	public int getTick()
	{
		return tick;
	}

	public String getDisplay()
	{
		return display;
	}
}
