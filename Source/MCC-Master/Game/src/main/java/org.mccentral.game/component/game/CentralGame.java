package org.mccentral.game.component.game;

import org.mccentral.game.Game;
import org.mccentral.game.component.game.kit.CentralKit;
import org.mccentral.game.component.game.scoreboard.CentralScoreboard;
import org.mccentral.game.component.game.state.GameState;

import java.util.HashMap;

/**
 * Created by Teddeh on 12/04/2016.
 */
public abstract class CentralGame
{
	protected Game game;
	private int id;
	private String name;
	private GameType gameType;
	private String[] description;
	private CentralKit[] centralKits;
	private HashMap<GameState, CentralScoreboard> scoreboards;

	public CentralGame(Game game, int id, String name, GameType gameType, String[] description, CentralKit[] centralKits)
	{
		this.game = game;
		this.id = id;
		this.name = name;
		this.gameType = gameType;
		this.description = description;
		this.centralKits = centralKits;
		this.scoreboards = new HashMap<>();
	}

	public void initialise() {}
	public void scoreboard() {}

	public void addScoreboard(GameState gameState, CentralScoreboard scoreboard)
	{
		scoreboards.put(gameState, scoreboard);
	}

	public CentralScoreboard getScoreboard(GameState gameState)
	{
		if(gameState == null) return null;
		if(!scoreboards.containsKey(gameState)) return null;
		return scoreboards.get(gameState);
	}

	public Game getPlugin()
	{
		return game;
	}

	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public GameType getGameType()
	{
		return gameType;
	}

	public String[] getDescription()
	{
		return description;
	}

	public CentralKit[] getCentralKits()
	{
		return centralKits;
	}
}
