package org.mccentral.game.component.game.stats;

/**
 * Created: 13/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface ICentralStat
{
	int getId();
	String getName();
}
