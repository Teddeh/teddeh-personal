package org.mccentral.game.component.game.kit;

import org.bukkit.inventory.ItemStack;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class KitItem
{
	private ItemStack itemStack;
	private int index;

	public KitItem(ItemStack itemStack, int index)
	{
		this.itemStack = itemStack;
		this.index = index;
	}

	public ItemStack getItemStack()
	{
		return itemStack;
	}

	public int getIndex()
	{
		return index;
	}
}
