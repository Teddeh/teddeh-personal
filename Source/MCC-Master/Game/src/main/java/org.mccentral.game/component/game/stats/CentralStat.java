package org.mccentral.game.component.game.stats;

import org.mccentral.core.component.Component;
import org.mccentral.game.component.game.CentralGame;

/**
 * Created: 30/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class CentralStat<T> extends Component implements ICentralStat
{
	private T value;

	public CentralStat(CentralGame game)
	{
		super(game.getPlugin());
	}

	public T getValue()
	{
		return value;
	}

	public void setValue(T value)
	{
		this.value = value;
	}

	/**
	 *
	 * COMBAT:
	 *   Kills
	 *   Deaths
	 *   Assists
	 *
	 * TRAVEL:
	 *   Walk Distance
	 *
	 * CONSTRUCT:
	 *   Blocks Broken
	 *   Blocks Placed
	 *
	 * HACK REPORTS:
	 *   Kill Aura
	 *   Flight
	 *   Anti Knockback
	 *   More TODO
	 *
	 *
	 */
}
