package org.mccentral.game.component.game.stats.combat;

import org.mccentral.game.component.game.CentralGame;
import org.mccentral.game.component.game.stats.CentralStat;
import org.mccentral.game.component.game.stats.type.StatCombat;

/**
 * Created: 30/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
@StatCombat
public class StatKills extends CentralStat<Integer>
{

	public StatKills(CentralGame game)
	{
		super(game);
	}

	@Override
	public int getId()
	{
		return 1;
	}

	@Override
	public String getName()
	{
		return "Kills";
	}

	@Override
	protected void onEnable()
	{

	}

	@Override
	protected void onDisable()
	{

	}
}
