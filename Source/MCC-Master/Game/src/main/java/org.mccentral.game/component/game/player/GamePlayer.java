package org.mccentral.game.component.game.player;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mccentral.game.component.game.player.state.PlayerState;

import java.util.UUID;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class GamePlayer
{
	private final String name;
	private final UUID uuid;
	private ChatColor team = ChatColor.BLACK;

	private PlayerState playerState;

	public GamePlayer(String name, UUID uuid)
	{
		this.name = name;
		this.uuid = uuid;
	}

	public GamePlayer(Player player)
	{
		this.name = player.getName();
		this.uuid = player.getUniqueId();
	}

	public UUID getUuid()
	{
		return uuid;
	}

	public String getName()
	{
		return name;
	}

	public Player getPlayer()
	{
		return Bukkit.getPlayer(uuid);
	}

	public ChatColor getTeam()
	{
		return team;
	}

	public void setTeam(ChatColor team)
	{
		this.team = team;
	}

	/**
	 * Get the current player state for the game.
	 *
	 * @return
	 */
	public PlayerState getPlayerState()
	{
		return playerState;
	}

	/**
	 * Set the specified player state for the current game.
	 *
	 * @param playerState
	 */
	public void setPlayerState(PlayerState playerState)
	{
		this.playerState = playerState;
	}
}
