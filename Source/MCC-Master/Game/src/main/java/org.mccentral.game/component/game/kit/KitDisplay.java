package org.mccentral.game.component.game.kit;

import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class KitDisplay
{
	private ItemStack itemStack;
	private EntityType entityType;

	public KitDisplay(ItemStack itemStack, EntityType entityType)
	{
		this.itemStack = itemStack;
		this.entityType = entityType;
	}

	public ItemStack getItemStack()
	{
		return itemStack;
	}

	public EntityType getEntityType()
	{
		return entityType;
	}
}
