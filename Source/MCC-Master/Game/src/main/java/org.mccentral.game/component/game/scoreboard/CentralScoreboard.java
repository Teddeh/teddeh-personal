package org.mccentral.game.component.game.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.*;
import org.mccentral.core.util.SequenceUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Teddeh on 16/04/2016.
 */
public class CentralScoreboard
{
	private ScoreboardManager manager;
	private Scoreboard scoreboard;
	private Objective objective;

	public CentralScoreboard(String title)
	{
		manager = Bukkit.getScoreboardManager();
		scoreboard = manager.getNewScoreboard();

		Objective objective = scoreboard.registerNewObjective(SequenceUtil.indexToColumn(new Random().nextInt(99999)), "dummy");
		if(objective == null)
			objective = scoreboard.registerNewObjective("Board", "dummy");

		objective.setDisplayName(title);
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		this.objective = objective;
	}

	public void setScore(String message, int row)
	{
		if (message == null)
		{
			int spaces = Integer.parseInt((row + "").replace("-", ""));
			message = ChatColor.WHITE + "";
			for (int x = 0; x < spaces; x++)
			{
				message += " ";
			}
		}

		Score score = objective.getScore(message);
		score.setScore(row);
	}

	public void setTitle(String title)
	{
		if(objective == null) return;
		objective.setDisplayName(title);
	}

	public void moveTeams(Scoreboard scoreboard)
	{
		List<Team> temp = new ArrayList<>();
		scoreboard.getTeams().forEach(team -> temp.add(team));
		this.scoreboard.getTeams().addAll(temp);
	}

	public Scoreboard getScoreboard()
	{
		return scoreboard;
	}

	public ScoreboardManager getManager()
	{
		return manager;
	}

	public Objective getObjective()
	{
		return objective;
	}
}
