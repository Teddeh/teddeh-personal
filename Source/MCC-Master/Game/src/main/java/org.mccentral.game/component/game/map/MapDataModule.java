package org.mccentral.game.component.game.map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.mccentral.core.util.LocationUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Teddeh on 14/04/2016.
 */
public class MapDataModule
{
	private File file = null;
	private FileConfiguration fileConfiguration = null;
	private String map;

	public MapDataModule(String mapName)
	{
		this.map = mapName;
		file = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/" + mapName + "/worlddata.yml");
		fileConfiguration = YamlConfiguration.loadConfiguration(file);
	}

	public String getMap()
	{
		return map;
	}

	public List<Location> getDataLocations(String color)
	{
		if(fileConfiguration == null)
			return null;

		if(!fileConfiguration.contains("Data." + color))
			return null;

		List<Location> temp = new ArrayList<Location>();
		fileConfiguration.getStringList("Data." + color).forEach(str -> temp.add(LocationUtil.stringTobLocation(str)));

		return temp;
	}

	public List<Location> getSpawnLocations(String color)
	{
		if(fileConfiguration == null)
			return null;

		if(!fileConfiguration.contains("Spawns." + color))
			return null;

		List<Location> temp = new ArrayList<Location>();
		fileConfiguration.getStringList("Spawns." + color).forEach(str -> temp.add(LocationUtil.stringTobLocation(str)));

		return temp;
	}

	public List<ChatColor> getTeams()
	{
		List<ChatColor> temp = new ArrayList<ChatColor>();
		if(fileConfiguration.contains("Spawns.ORANGE"))
			temp.add(ChatColor.GOLD);
		if (fileConfiguration.contains("Spawns.PURPLE"))
			temp.add(ChatColor.LIGHT_PURPLE);
		if (fileConfiguration.contains("Spawns.AQUA"))
			temp.add(ChatColor.AQUA);
		if (fileConfiguration.contains("Spawns.YELLOW"))
			temp.add(ChatColor.YELLOW);
		if (fileConfiguration.contains("Spawns.LIME"))
			temp.add(ChatColor.GREEN);
		if (fileConfiguration.contains("Spawns.DARK_GRAY"))
			temp.add(ChatColor.DARK_GRAY);
		if (fileConfiguration.contains("Spawns.GRAY"))
			temp.add(ChatColor.GRAY);
		if (fileConfiguration.contains("Spawns.CYAN"))
			temp.add(ChatColor.BLUE);
		if (fileConfiguration.contains("Spawns.DARK_PURPLE"))
			temp.add(ChatColor.DARK_PURPLE);
		if (fileConfiguration.contains("Spawns.DARK_BLUE"))
			temp.add(ChatColor.DARK_BLUE);
		if (fileConfiguration.contains("Spawns.DARK_GREEN"))
			temp.add(ChatColor.DARK_GREEN);
		if (fileConfiguration.contains("Spawns.RED"))
			temp.add(ChatColor.RED);
		if (fileConfiguration.contains("Spawns.BLACK"))
			temp.add(ChatColor.BLACK);
		if (temp.isEmpty())
			return null;

		return temp;
	}

	public Location[] getBorder()
	{
		if(fileConfiguration == null)
			return null;

		if (!fileConfiguration.contains("Border"))
			return null;

		List<String> border = fileConfiguration.getStringList("Border");
		return new Location[]{LocationUtil.stringTobLocation(border.get(0)), LocationUtil.stringTobLocation(border.get(1))};
	}

	public void register(Location location, boolean hasPlate, boolean hasSign)
	{
		if(hasPlate) location.getBlock().getRelative(BlockFace.UP).setType(Material.AIR);
		if(hasSign) location.getBlock().getRelative(getSignFace(location)).setType(Material.AIR);
		location.getBlock().setType(Material.AIR);
	}

	public BlockFace getSignFace(Location location)
	{
		for(BlockFace blockFace : new BlockFace[]{BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST})
		{
			Block block = location.getBlock().getRelative(blockFace);
			if(block == null || block.getType() == Material.AIR) continue;
			if(block.getType() != Material.WALL_SIGN) continue;

			return blockFace;
		}

		return null;
	}
}
