package org.mccentral.game.component.game.player.state;

/**
 * Created by Teddeh on 12/04/2016.
 */
public enum PlayerState
{
	LOBBY,
	PLAYING,
	SPECTATING;
}
