package org.mccentral.game.component.game.player.state;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class PlayerStateChangeEvent extends Event
{
	private static HandlerList handlers = new HandlerList();

	private Player player;
	private PlayerState playerState;

	public PlayerStateChangeEvent(Player player, PlayerState playerState)
	{
		this.player = player;
		this.playerState = playerState;
	}

	public Player getPlayer()
	{
		return player;
	}

	public PlayerState getPlayerState()
	{
		return playerState;
	}

	@Override
	public HandlerList getHandlers()
	{
		return handlers;
	}

	public static HandlerList getHandlerList()
	{
		return handlers;
	}
}
