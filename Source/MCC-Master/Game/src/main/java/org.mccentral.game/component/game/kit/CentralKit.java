package org.mccentral.game.component.game.kit;

/**
 * Created by Teddeh on 12/04/2016.
 */
public abstract class CentralKit
{
	private int id;
	private String name;
	private String[] description;
	private KitDisplay kitDisplay;
	private KitItem[] kitItems;

	public CentralKit(int id, String name, String[] description, KitDisplay kitDisplay, KitItem[] kitItems)
	{
		this.id = id;
		this.name = name;
		this.description = description;
		this.kitDisplay = kitDisplay;
		this.kitItems = kitItems;
	}

	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public String[] getDescription()
	{
		return description;
	}

	public KitDisplay getKitDisplay()
	{
		return kitDisplay;
	}

	public KitItem[] getKitItems()
	{
		return kitItems;
	}
}
