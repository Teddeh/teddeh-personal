package org.mccentral.game.component.game.countdown;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.mccentral.core.util.ServerUtil;
import org.mccentral.game.GameManager;
import org.mccentral.game.component.game.state.GameState;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Teddeh on 15/04/2016.
 */
public class Countdown
{
	private List<CountdownDisplay> display;
	private GameManager gameManager;
	private BukkitTask task;
	public boolean running;
	private int startingTime = 10, tick = 10;

	public Countdown(GameManager gameManager)
	{
		this.gameManager = gameManager;
		this.running = false;
		this.task = null;
		this.display = new ArrayList<>();
	}

	public Countdown setStartingTime(int startingTime)
	{
		this.startingTime = startingTime;
		this.tick = startingTime;
		return this;
	}

	public Countdown setDisplay(CountdownDisplay countdownDisplay)
	{
		this.display.add(countdownDisplay);
		return this;
	}

	public void start()
	{
		if (running)
		{
			ServerUtil.log(Level.INFO, "Tried starting countdown whist already running.");
			return;
		}

		task = new BukkitRunnable()
		{
			@Override
			public void run()
			{
				if (containsDisplay(tick))
				{
					CountdownDisplay cd = getCountdownDisplay(tick);
					Bukkit.getOnlinePlayers().forEach(player -> cd.display(player, cd.getDisplay()));
				}

				if (tick <= 0)
				{
					stop();
					finish();
					return;
				}

				tick -= 1;
			}
		}.runTaskTimer(gameManager.getGame(), 0L, 20L);
	}

	public void stop()
	{
		if (task != null) task.cancel();
		reset();
	}

	public void reset()
	{
		this.tick = this.startingTime;
		this.running = false;
	}

	private boolean containsDisplay(int tick)
	{
		for (CountdownDisplay countdownDisplay : display)
		{
			if (countdownDisplay.getTick() != tick) continue;
			return true;
		}

		return false;
	}

	private CountdownDisplay getCountdownDisplay(int tick)
	{
		for (CountdownDisplay countdownDisplay : display)
		{
			if (countdownDisplay.getTick() != tick) continue;
			return countdownDisplay;
		}

		return null;
	}

	public void finish() {}
}
