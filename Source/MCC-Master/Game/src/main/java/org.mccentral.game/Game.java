package org.mccentral.game;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import org.mccentral.core.Core;
import org.mccentral.core.component.world.WorldTimeComponent;
import org.mccentral.core.util.FileUtil;
import org.mccentral.core.util.ServerUtil;
import org.mccentral.core.util.WorldUtil;
import org.mccentral.game.component.game.map.MapDataModule;

import java.io.File;
import java.util.logging.Level;

/**
 * Created by Teddeh on 12/04/2016.
 */
public abstract class Game extends Core
{
	private GameManager gameManager;
	protected boolean standalone;

	@Override
	public void enable()
	{
		ServerUtil.log(Level.INFO, "Game plugin enabled.");
		gameManager = new GameManager(this);

		standalone = false;

		new WorldTimeComponent(this, true, 0, Bukkit.getWorld("world"));
	}

	@Override
	public void disable()
	{
		MapDataModule dataModule = gameManager.getMapDataModule();
		WorldUtil.unloadWorld(dataModule.getMap(), false);

		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				FileUtil.deleteFile(new File(Bukkit.getWorldContainer().getAbsolutePath() + "/" + dataModule.getMap()));
			}
		}.runTaskLater(this, 20);
	}

	public GameManager getGameManager()
	{
		return gameManager;
	}

	public boolean isStandalone()
	{
		return standalone;
	}
}
