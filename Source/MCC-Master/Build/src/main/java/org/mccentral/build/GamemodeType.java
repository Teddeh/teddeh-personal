package org.mccentral.build;

/**
 * Created by Teddeh on 12/04/2016.
 */
public enum GamemodeType
{
	HUB("Hub"),
	SURVIVAL_GAMES("Survival Games");

	private final String name;

	GamemodeType(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}
}
