package org.mccentral.build;

import org.mccentral.build.command.CreateCommand;
import org.mccentral.build.command.MapsCommand;
import org.mccentral.build.command.SaveCommand;
import org.mccentral.build.component.BuildManager;
import org.mccentral.core.Core;
import org.mccentral.core.server.ServerType;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class Build extends Core
{
	@Override
	public void enable()
	{
		new CreateCommand(this);
		new MapsCommand(this);
		new SaveCommand(this);

		enableComponents(new BuildManager(this));
	}

	@Override
	public void disable()
	{

	}

	@Override
	public ServerType getServerType()
	{
		return ServerType.BUILD;
	}
}
