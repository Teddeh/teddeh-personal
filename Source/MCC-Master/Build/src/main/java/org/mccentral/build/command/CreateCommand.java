package org.mccentral.build.command;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.mccentral.build.GamemodeType;
import org.mccentral.core.Core;
import org.mccentral.core.chat.ChatType;
import org.mccentral.core.command.CentralCommand;
import org.mccentral.core.util.*;

import java.io.File;
import java.io.IOException;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class CreateCommand extends CentralCommand
{

	public CreateCommand(Core core)
	{
		super(
				core,
				"create",
				new String[]{"make"},
				Rank.BUILDER,
				"/create <gamemode> <mapName>",
				"test",
				"test",
				Sender.PLAYER
		);
	}

	@Override
	public void execute(CommandSender commandSender, String[] args)
	{
		Player sender = (Player) commandSender;
		if(args.length < 2)
		{
			PlayerUtil.message(sender, ChatType.ERROR, "Incorrect arguments, /create <gamemode> <mapName>");
			return;
		}

		GamemodeType gamemode = null;
		for(GamemodeType gamemodeType : GamemodeType.values())
		{
			if(!args[0].equalsIgnoreCase(gamemodeType.toString()))
				continue;
			gamemode = gamemodeType;
			break;
		}

		if(gamemode == null)
		{
			PlayerUtil.message(sender, ChatType.ERROR, "The directory '" + args[0] + "' could not be recognised.");
			PlayerUtil.message(sender, " ");
			PlayerUtil.message(sender, "Listing all directories:");

			for(GamemodeType gamemodeType : GamemodeType.values())
				PlayerUtil.message(sender, gamemodeType.toString());
			PlayerUtil.message(sender, " ");
			return;
		}

		String mapName = args[1];
		File file = new File(Bukkit.getWorldContainer().getPath() + "/../maps/" + gamemode.getName());
		if(!file.exists()) file.mkdir();
		for(File f : file.listFiles())
		{
			if(!f.getName().equalsIgnoreCase(mapName))
				continue;
			PlayerUtil.message(sender, ChatType.ERROR, "Map conflict, use a different map name.");
			return;
		}

		File templateWorld = new File(Bukkit.getWorldContainer().getPath() + "/../maps/templateWorld");
		File destination = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/" + mapName);

		FileUtil.copyFiles(templateWorld, destination);
		World object = WorldUtil.generateWorld(mapName, World.Environment.NORMAL, true);
		sender.teleport(new Location(object, 0, 52, 0, 0, 0));

		File data = new File(Bukkit.getWorldContainer().getPath() + "/" + mapName, "worlddata.yml");
		try
		{
			if(!data.exists()) data.createNewFile();
			YamlConfiguration configuration = YamlConfiguration.loadConfiguration(data);
			configuration.set("WorldType", "NORMAL");
			configuration.set("Gamemode", gamemode.toString());
			configuration.save(data);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
