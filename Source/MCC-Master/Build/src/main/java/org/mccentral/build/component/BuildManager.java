package org.mccentral.build.component;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPreLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.mccentral.build.GamemodeType;
import org.mccentral.core.Core;
import org.mccentral.core.component.Component;
import org.mccentral.core.database.MCCPlayer;
import org.mccentral.core.util.FileUtil;
import org.mccentral.core.util.Rank;
import org.mccentral.core.util.ServerUtil;
import org.mccentral.core.util.WorldUtil;

import java.io.File;
import java.util.logging.Level;

/**
 * Created by Teddeh on 13/04/2016.
 */
public class BuildManager extends Component
{
	private Core core;

	public BuildManager(Core plugin)
	{
		super(plugin);
		this.core = plugin;
	}

	@Override
	protected void onEnable()
	{
	}

	@Override
	protected void onDisable()
	{
	}

	@EventHandler
	public void playerJoin(PlayerJoinEvent event)
	{
		MCCPlayer mccPlayer = core.getPlayer(event.getPlayer().getUniqueId());
		if(!mccPlayer.getRank().hasRank(Rank.BUILDER))
		{
			event.getPlayer().kickPlayer("ONLY BUILDERS+ CAN JOIN, SILLY..");
		}
	}

	@EventHandler
	public void onLeaveWorld(PlayerChangedWorldEvent event)
	{
		if(event.getFrom().getName().equalsIgnoreCase("world"))
			return;
		System.out.println("Attempting save...");

		World from = event.getFrom();
		String worldName = from.getName();
		File file = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/" + worldName);

		File data = new File(file.getAbsolutePath() + "/worlddata.yml");
		if (!data.exists()) return;



		YamlConfiguration configuration = YamlConfiguration.loadConfiguration(data);
		GamemodeType gamemode = GamemodeType.valueOf(configuration.getString("Gamemode"));
		File to = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/../maps/" + gamemode.getName() + "/" + worldName);
		FileUtil.deleteFile(to);
		if (!to.exists()) to.mkdir();

		WorldUtil.unloadWorld(from, true);
		FileUtil.copyFiles(file, to);
		ServerUtil.log(Level.INFO, "World, " + worldName + " has been saved to " + gamemode.getName());

		FileUtil.deleteFile(file);
		ServerUtil.log(Level.INFO, "World, " + worldName + " saved & deleted from server file.");

	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		World from = event.getPlayer().getWorld();
		if(from.getName().equalsIgnoreCase("world"))
			return;

		event.getPlayer().teleport(new Location(Bukkit.getWorld("world"), 0, 52, 0, 0, 0));
		String worldName = from.getName();
		File file = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/" + worldName);
		File data = new File(file.getAbsolutePath(), "worlddata.yml");

		if (!data.exists()) return;
		YamlConfiguration configuration = YamlConfiguration.loadConfiguration(data);
		GamemodeType gamemode = GamemodeType.valueOf(configuration.getString("Gamemode"));
		File to = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/../maps/" + gamemode.getName() + "/" + worldName);
		FileUtil.deleteFile(to);
		if (!to.exists()) to.mkdir();

		WorldUtil.unloadWorld(from, true);
		FileUtil.copyFiles(file, to);

		FileUtil.deleteFile(file);


	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		event.getPlayer().teleport(new Location(Bukkit.getWorld("world"), 0, 52, 0, 0, 0)); //5 blocks up is damaging
	}
}
