package org.mccentral.build.command;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.mccentral.build.GamemodeType;
import org.mccentral.core.Core;
import org.mccentral.core.chat.ChatType;
import org.mccentral.core.command.CentralCommand;
import org.mccentral.core.util.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Teddeh on 14/04/2016.
 */
public class SaveCommand extends CentralCommand
{

	public SaveCommand(Core core)
	{
		super(
				core,
				"save",
				new String[]{"parse"},
				Rank.BUILDER,
				"/save <integer> <integer>",
				"",
				"",
				Sender.PLAYER
		);

	}

	@Override
	public void execute(CommandSender commandSender, String[] args)
	{
		Player sender = (Player) commandSender;
		if(args.length < 1)
		{
			PlayerUtil.message(sender, ChatType.ERROR, "Incorrect arguments, /save <integer>");
			return;
		}

		if(sender.getWorld().getName().equalsIgnoreCase("world"))
		{
			PlayerUtil.message(sender, ChatType.ERROR, "You cannot save 'world'");
			return;
		}

		World world = sender.getWorld();
		int range;
		try
		{
			range = Integer.parseInt(args[0]);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			PlayerUtil.message(sender, ChatType.ERROR, "Argument needs to be a number.");
			return;
		}

		final int x1 = range, x2 = -range;

		File file = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/" + world.getName() + "/worlddata.yml");
		if(file == null || !file.exists())
		{
			PlayerUtil.message(sender, ChatType.ERROR, "This world does not contain worlddata.yml");
			PlayerUtil.message(sender, ChatType.ERROR, "Contact an admin to fix this.");
		}
		FileConfiguration fileConfiguration = YamlConfiguration.loadConfiguration(file);

		if(fileConfiguration.contains("Data")) fileConfiguration.set("Data", null);
		if(fileConfiguration.contains("Boarder")) fileConfiguration.set("Boarder", null);
		if(fileConfiguration.contains("Spawns")) fileConfiguration.set("Spawns", null);

		for(int x = x2; x < x1; x++)
		{
			for(int z = x2; z < x1; z++)
			{
				for(int y = 1; y < 250; y++)
				{
					Location loc = world.getBlockAt(x, y, z).getLocation();
					if(loc.getBlock() == null)
						continue;

					if(loc.getBlock().getType() != Material.WOOL)
						continue;

					Block block = loc.getBlock();
					String l = loc.getWorld().getName() + ";" + x + ";" + y + ";" + z;
					switch (block.getData())
					{
						case 0:
							addLocation(fileConfiguration, l, "WHITE", block);
							break;

						case 1:
							addLocation(fileConfiguration, l, "ORANGE", block);
							break;

						case 2:
							addLocation(fileConfiguration, l, "PURPLE", block);
							break;

						case 3:
							addLocation(fileConfiguration, l, "AQUA", block);
							break;

						case 4:
							addLocation(fileConfiguration, l, "YELLOW", block);
							break;

						case 5:
							addLocation(fileConfiguration, l, "LIME", block);
							break;

						case 6:
							addLocation(fileConfiguration, l, "PINK", block);
							break;

						case 7:
							addLocation(fileConfiguration, l, "DARK_GRAY", block);
							break;

						case 8:
							addLocation(fileConfiguration, l, "GRAY", block);
							break;

						case 9:
							addLocation(fileConfiguration, l, "CYAN", block);
							break;

						case 10:
							addLocation(fileConfiguration, l, "DARK_PURPLE", block);
							break;

						case 11:
							addLocation(fileConfiguration, l, "DARK_BLUE", block);
							break;

						case 12:
							addLocation(fileConfiguration, l, "BROWN", block);
							break;

						case 13:
							addLocation(fileConfiguration, l, "DARK_GREEN", block);
							break;

						case 14:
							addLocation(fileConfiguration, l, "RED", block);
							break;

						case 15:
							addLocation(fileConfiguration, l, "BLACK", block);
							break;
					}
				}
			}
		}

		try
		{
			fileConfiguration.save(file);
			File f = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/" + world.getName());
			File data = new File(file.getAbsolutePath() + "/worlddata.yml");
			if (!data.exists()) return;
			GamemodeType gamemode = GamemodeType.valueOf(fileConfiguration.getString("Gamemode"));
			File to = new File(Bukkit.getWorldContainer().getAbsolutePath() + "/../maps/" + gamemode.getName() + "/" + world.getName());
			FileUtil.deleteFile(to);
			if(!to.exists()) to.mkdir();

			ServerUtil.log(Level.INFO, "World, " + world.getName() + " has been saved to " + gamemode.getName());
			PlayerUtil.message(sender, "World saving complete.");
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addLocation(FileConfiguration config, String location, String color, Block block)
	{
		if(block.getRelative(BlockFace.UP).getType() == Material.GOLD_PLATE)
		{
			List<String> spawns = new ArrayList<String>();
			if(config.contains("Spawns." + color))
			{
				spawns = config.getStringList("Spawns." + color);
			}

			spawns.add(location);
			config.set("Spawns." + color, spawns);
		}

		if(block.getRelative(BlockFace.UP).getType() == Material.IRON_PLATE)
		{
			List<String> locs = new ArrayList<String>();
			if(color.equalsIgnoreCase("WHITE"))
			{
				if(config.contains("Boarder"))
				{
					locs = config.getStringList("Boarder");
				}

				locs.add(location);
				config.set("Boarder", locs);
				return;
			}

			if(config.contains("Data." + color))
			{
				locs = config.getStringList("Data." + color);
			}

			locs.add(location);
			config.set("Data." + color, locs);
		}

//		block.getRelative(BlockFace.UP).setType(Material.AIR);
//		block.setType(Material.AIR);
	}
}
