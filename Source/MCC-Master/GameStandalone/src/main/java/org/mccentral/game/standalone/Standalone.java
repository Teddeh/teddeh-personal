package org.mccentral.game.standalone;

import org.mccentral.core.server.ServerType;
import org.mccentral.game.Game;
import org.mccentral.game.GameManager;
import org.mccentral.game.survivalgames.SurvivalGames;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class Standalone extends Game
{
	@Override
	public void enable()
	{
		super.enable();
		standalone = true;
		getGameManager().initialise(new SurvivalGames(this));
	}

	@Override
	public void disable()
	{
		super.disable();
	}

	@Override
	public ServerType getServerType()
	{
		return ServerType.GAME_STANDALONE;
	}
}
