package org.mccentral.core.database;

import org.mccentral.core.util.Callback;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Teddeh on 9/04/2016.
 */
public class SafeStatement
{
    private PreparedStatement handle;
    private Connection connection;

    public SafeStatement(PreparedStatement preparedStatement, Connection connection, Object... arguments)
    {
        handle = preparedStatement;
        this.connection = connection;
        for (int i = 0; i < arguments.length; i++)
        {
            set(i + 1, arguments[i]);
        }
    }

    public void executeUpdate()
    {
        try
        {
            handle.executeUpdate();
        } catch (Exception e)
        {
            e.printStackTrace();
            closeConnection();
        } finally
        {
            closeConnection();
        }
    }

    public SafeSet executeQuery()
    {
        try
        {
            SafeSet set = new SafeSet(handle.executeQuery());
            return set;
        } catch (Exception e)
        {
            e.printStackTrace();
            closeConnection();
        } finally
        {
            closeConnection();
        }
        return null;
    }

    public void executeQuery(Callback<SafeSet> callback)
    {
        try
        {
            SafeSet set = new SafeSet(handle.executeQuery());
            callback.call(set);
        } catch (Exception e)
        {
            e.printStackTrace();
            closeConnection();
        } finally
        {
            closeConnection();
        }
    }

    public void executeUpdate(Callback<Integer> callback)
    {
        try
        {
            int i = handle.executeUpdate();
            callback.call(i);
        } catch (Exception e)
        {
            e.printStackTrace();
            closeConnection();
        } finally
        {
            closeConnection();
        }
    }

    public SafeStatement set(int i, Object obj)
    {
        try
        {
            handle.setObject(i, obj);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return this;
    }


    private void closeConnection()
    {
        try
        {
            if (handle != null) handle.close();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        try
        {
            if (connection != null) connection.close();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

}
