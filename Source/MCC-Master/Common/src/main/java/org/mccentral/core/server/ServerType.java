package org.mccentral.core.server;

/**
 * Created by Teddeh on 9/04/2016.
 */
public enum ServerType
{
	LOBBY,
	BUILD_LOBBY,
	BUILD,
	BUILD_TRIAL,
	GAME_STANDALONE,
	GAME_ARCADE
}
