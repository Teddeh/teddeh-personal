package org.mccentral.core.locale;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum LocaleMessage
{
	TEST("test"),
	WELCOME("welcome"),

	GAME_JOINED("game.joined"),
	GAME_COUNTDOWN("game.countdown"),
	GAME_STARTED("game.started");

	private String key;

	LocaleMessage(String key)
	{
		this.key = key;
	}

	public String getKey()
	{
		return key;
	}
}
