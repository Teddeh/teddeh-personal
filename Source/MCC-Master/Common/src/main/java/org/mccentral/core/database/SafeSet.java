package org.mccentral.core.database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Teddeh on 15/04/2016.
 */
public class SafeSet
{
    private List<HashMap<String, Object>> objects = new ArrayList<>();
    private int x = -1;

    public SafeSet(ResultSet set)
    {
        try
        {
            ResultSetMetaData rsmd = set.getMetaData();
            int noColumns = rsmd.getColumnCount();


            while (set.next())
            {
                HashMap<String, Object> temp = new HashMap<>();
                for (int i = 1; i < noColumns; i++)
                {
                    try
                    {
                        temp.put(rsmd.getColumnName(i), set.getObject(i));
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                        System.out.println(i + " unabled to loaded?? engrish where is u");
                    }

                }
                objects.add(temp);
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public boolean next()
    {
        if (++x > objects.size() - 1) return false;
        return true;
    }

    public <T> T get(String columnName, Class<T> clazz)
    {
        return clazz.cast(objects.get(x).get(columnName));
    }
}
