package org.mccentral.core.message;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;

/**
 * Created by Teddeh on 9/04/2016.
 */
public class MessageWriter
{
    private String serverName;
    private Gson gson = new Gson();
    private JedisPool pool;

    public MessageWriter(String serverName, JedisPool pool)
    {
        this.serverName = serverName;
        this.pool = pool;
    }

    public void publishPacket(Object message, String recipient)
    {
        JsonObject label = new JsonObject();
        label.addProperty("name", message.getClass().getSimpleName());
        label.addProperty("sender", serverName);
        label.addProperty("recipient", recipient);
        label.add("content", gson.toJsonTree(message));

        Jedis jedis = null;
        try
        {
            jedis = pool.getResource();
            jedis.publish("mcc", label.toString());
            pool.returnResource(jedis);
        } catch (Exception e)
        {
            e.printStackTrace();
            if(jedis != null) pool.returnBrokenResource(jedis);
        }
    }
}
