package org.mccentral.core.message;

import com.google.gson.Gson;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

/**
 * Created by Teddeh on 9/04/2016.
 */
public class JedisServer
{
    private final Gson gson = new Gson();
    private JedisPool jedisPool;
    private MessageReader reader;
    private MessageWriter writer;

    public JedisServer(String serverName)
    {
        this.jedisPool = new JedisPool(new JedisPoolConfig(), "localhost", 6379, 0, null); //TODO PRODUCTION REDIS
        Jedis jedis = null;
        try
        {
            jedis = jedisPool.getResource();
            jedis.ping();
            jedisPool.returnResource(jedis);
        } catch (JedisConnectionException e)
        {
            if (jedis != null)
            {
                jedisPool.returnBrokenResource(jedis);
            }
        }

        reader = new MessageReader(serverName);
        writer = new MessageWriter(serverName, jedisPool);

        new Thread(() -> getJedisPool().getResource().subscribe(reader, "mcc")).start();

        System.out.println(serverName + " jedis enabled!!!");

    }

    public void sendMessage(Object message, String recipient)
    {
        writer.publishPacket(message, recipient);
    }

    public void disable()
    {
        if(reader.isSubscribed()) reader.unsubscribe();
        jedisPool.destroy();
    }

    public JedisPool getJedisPool()
    {
        return jedisPool;
    }

}
