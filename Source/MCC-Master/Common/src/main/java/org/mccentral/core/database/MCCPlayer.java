package org.mccentral.core.database;

import org.mccentral.core.locale.LocaleType;
import org.mccentral.core.server.ServerType;
import org.mccentral.core.util.Rank;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by Teddeh on 13/04/2016.
 */
public class MCCPlayer
{
    private int id = -1; //-1 means not in database
    private Rank rank = Rank.MEMBER;
    private String lastName = "";
    private UUID uuid;
    private List<String> previousNames = new ArrayList<String>();
    private ServerType lastServer;
    private long money = 0;
    private InetAddress address;
    private LocaleType locale = LocaleType.en_US; //Default language

    //STATS NOT HERE

    public MCCPlayer(UUID uuid)
    {
        this.uuid = uuid;
    }

    public MCCPlayer(int id)
    {
        this.id = id;
    }

    /**
     *
     * DATABASE STRUCTURE
     *
     * ID - UUID - LASTNAME - LASTSERVER - RANK - MONEY - PREVIOUSNAMES
     */

    public void load(Database database)
    {

        SafeSet set = database.prepareStatement("SELECT * FROM users WHERE UUID = ?;").set(1, uuid == null ? id : uuid.toString()).
                    executeQuery();
        if(!set.next()) return;

        id = set.get("ID", Integer.class);
        lastName = set.get("LASTNAME", String.class);
        lastServer = ServerType.valueOf(set.get("LASTSERVER", String.class));
        rank = Rank.valueOf(set.get("RANK", String.class));
        money = set.get("MONEY", Long.class);
        String prevName = set.get("PREVIOUSNAMES", String.class);
        if(prevName == null) return;
        for (String name : prevName.split(Pattern.quote(";")))
        {
            previousNames.add(name);
        }
    }
    public void save(Database database) {
        StringBuilder previous = new StringBuilder();
        for(String string : previousNames) previous.append(string + ";");
        previous.setLength(previous.length() > 0 ? previous.length() - 1 : previous.length());

        if (id == -1)
        {
            database.prepareStatement("INSERT INTO users (UUID, LASTNAME, LASTSERVER, RANK, MONEY, PREVIOUSNAMES) VALUES (?, ?, ?, ?, ?, ?);").
                    set(1, uuid.toString()).set(2, lastName).set(3, lastServer.name()).set(4, rank.name()).set(5, money).set(6, previous.toString()).
            executeUpdate();
        } else {
            database.prepareStatement("UPDATE users SET LASTNAME = ?, LASTSERVER = ?, RANK = ?, MONEY = ?, PREVIOUSNAMES = ? WHERE ID = ?").
            set(1, lastName).set(2, lastServer.name()).set(3, rank.name()).set(4, money).set(5, previous.toString()).set(6, id).executeUpdate();
        }

    }

    public long getMoney()
    {
        return money;
    }

    public Rank getRank()
    {
        return rank;
    }

    public int getId()
    {
        return id;
    }

    public String getLastName()
    {
        return lastName;
    }

    public UUID getUUID()
    {
        return uuid;
    }

    public List<String> getPreviousNames()
    {
        return previousNames;
    }

    public ServerType getLastServer()
    {
        return lastServer;
    }

    public void setAddress(InetAddress address) { this.address = address;}

    public InetAddress getAddress() { return address; }

    public LocaleType getLocale()
    {
        return locale;
    }

    public void setServerType(ServerType serverType)
    {
        lastServer = serverType;
    }

    public void setName(String name) { lastName = name; }

    public void setLocale(LocaleType locale)
    {
        this.locale = locale;
    }
}
