package org.mccentral.core.util;

/**
 * Created by Teddeh on 9/04/2016.
 */
public abstract class Callback<T>
{
    public abstract void call(T object) throws Exception;
}
