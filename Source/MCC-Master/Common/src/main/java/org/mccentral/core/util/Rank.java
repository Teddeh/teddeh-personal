package org.mccentral.core.util;

/**
 * Created by Teddeh on 09/04/2016.
 */
public enum Rank
{
	OWNER("Owner", "&4"),
	ADMIN("Admin", "&c"),
	DEVELOPER("Dev", "&c"),
	MODERATOR("Moderator", "&c"),
	HELPER("Helper", "&c"),
	BUILDER("Builder", "&c"),

	COAL("Coal", "&c"),
	IRON("Iron", "&c"),
	REDSTONE("Redstone", "&c"),
	GOLD("Gold", "&c"),
	LAPIS("Lapis", "&c"),
	EMERALD("Emerald", "&c"),
	DIAMOND("Diamond", "&c"),
	BEDROCK("Bedrock", "&c"),
	LEGEND("Legend", "&c"),

	MEMBER(null, "&7");

	private String name, rankColor;

	Rank(String name, String rankColor)
	{
		this.name = name;
		this.rankColor = rankColor;
	}

	public boolean hasRank(Rank rank)
	{
		return this.compareTo(rank) <= 0;
	}

	public String getName(boolean uppercase)
	{
		return uppercase ? name.toUpperCase() : name;
	}

	public String getRankColor(boolean bold)
	{
		return bold ? rankColor + "&l" : rankColor;
	}
}
