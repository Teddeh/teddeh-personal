package org.mccentral.core.util.packets;

import org.mccentral.core.reflection.SafeField;

import java.util.List;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class Packet {
    private final Object handle;
    private final PacketType type;

    public Packet(PacketType type, List<Class> types, Object... params) {
        this.type = type;
        this.handle = type.newInstance(types, params);
    }

    public Packet(Object object) {
        if (object instanceof PacketType) {
            this.type = (PacketType) object;
            this.handle = type.newInstance();
            return;
        }
        this.handle = object;
        this.type = PacketType.getByName(object.getClass().getSimpleName());
    }

    public PacketType getType() {
        return type;
    }

    public Packet set(String index, Object value) {
        SafeField field = type.getTemplate().getField(index);
        field.set(handle, value);
        return this;
    }

    public <T> T get(String index, Class<T> type) {
        SafeField field = this.type.getTemplate().getField(index);
        return field.get(handle, type);
    }

    public Object getHandle() {
        return handle;
    }
}
