package org.mccentral.core.cosmetic.addon;

import org.mccentral.core.util.Rank;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface CosmeticRequiresRank
{
	Rank getRequiredRank();
}
