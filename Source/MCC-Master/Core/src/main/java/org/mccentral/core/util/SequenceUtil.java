package org.mccentral.core.util;

/**
 * Created: 24/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class SequenceUtil
{
	private static final char[] ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

	//Iterative
	public static String indexToColumn(int index) {
		if (index <= 0) throw new IndexOutOfBoundsException("index must be a positive number");
		if (index <= ALPHABET.length) return Character.toString(ALPHABET[index - 1]);
		StringBuffer sb = new StringBuffer();
		while (index > 0) {
			sb.insert(0, ALPHABET[--index % ALPHABET.length]);
			index /= ALPHABET.length;
		}
		return sb.toString();
	}

	//Recursive
	public static String indexToColumnRec(int index) {
		if (index <= 0) throw new IndexOutOfBoundsException("index must be a positive number");
		if (index <= ALPHABET.length) return Character.toString(ALPHABET[index - 1]);
		return indexToColumnRec(--index / ALPHABET.length) + ALPHABET[index % ALPHABET.length];
	}
}
