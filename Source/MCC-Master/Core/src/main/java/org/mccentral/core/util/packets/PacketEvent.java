package org.mccentral.core.util.packets;

import org.bukkit.entity.Player;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class PacketEvent {
    private Packet packet;
    private final Player player;
    private boolean cancelled = false;

    public PacketEvent(Packet packet, Player player) {
        this.packet = packet;
        this.player = player;
    }

    public Packet getPacket() {
        return packet;
    }

    public PacketType getType() {
        return packet.getType();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPacket(Packet packet) {
        this.packet = packet;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
