package org.mccentral.core.util.packets;

/**
 * Created by Teddeh on 12/04/2016.
 *
 *
 * This PacketManager is a really useful resource.
 * Please do not edit anything to do with netty on the bottom of this class
 *
 * The class hooks into the connection between the server and the player and
 * adds a pipeline called "mcc" right before the minecraft "packet_listener" pipeline.
 * This allows us to hook in the packets being received and sent out by the minecraft
 * server and cancel them or edit them if need be!
 */

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.mccentral.core.Core;
import org.mccentral.core.component.Component;
import org.mccentral.core.reflection.MCVersion;
import org.mccentral.core.reflection.SafeField;
import org.mccentral.core.reflection.SafeMethod;
import org.spigotmc.CustomTimingsHandler;

import java.util.List;

public class PacketManager extends Component {
    private SafeMethod _getHandle;
    private SafeField _playerConnection;
    private SafeField _networkManager;
    private SafeField CHANNEL;
    private SafeMethod _sendPacket;
    private CustomTimingsHandler _handler;

    public PacketManager(Core plugin) {
        super(plugin);
    }

    @Override
    protected void onEnable() {
        _handler = new CustomTimingsHandler("MCC Packet Handling");
        _getHandle = MCVersion.getCBMethod("entity.CraftEntity", "getHandle");
        _playerConnection = MCVersion.getNMSField("EntityPlayer", "playerConnection");
        _networkManager = MCVersion.getNMSField("PlayerConnection", "networkManager");
        CHANNEL = MCVersion.getNMSField("NetworkManager", "channel");
        _sendPacket = MCVersion.getNMSMethod("PlayerConnection", "sendPacket", MCVersion.getNMSClass("Packet").getHandle());

        Bukkit.getOnlinePlayers().forEach(this::hook);
    }
    @Override
    protected void onDisable() {
        Bukkit.getOnlinePlayers().forEach(this::unhook);
    }

    public Packet createPacket(PacketType type, List<Class> paramTypes, Object... params) {
        return new Packet(type, paramTypes, params);
    }

    public Packet createPacket(PacketType type) {
        return new Packet(type);
    }

    public void broadcastPacket(Packet packet) {
        for(Player player : Bukkit.getOnlinePlayers()) {
            sendPacket(player, packet);
        }
    }

    public void sendPacket(Player player, Packet packet) {
        Object handle = _getHandle.invoke(player);
        Object playerConnection = _playerConnection.get(handle);
        _sendPacket.invoke(playerConnection, packet.getHandle());
    }

    public void registerListener(PacketListener listener, PacketType... types) {
        for(PacketType type : (types.length == 0 ? PacketType.values() : types)) {
            type.register(listener);
        }
    }

    public void unregisterListener(PacketListener listener) {
        for(PacketType type : PacketType.values()) {
            type.unregister(listener);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        hook(player);
        System.out.println("HOOKING " + event.getPlayer().getName() + " TO PACKET LISTENER");
    }

    private void hook(Player player) {
        _handler.startTiming();
        Object handle = _getHandle.invoke(player);
        Object playerConnection = _playerConnection.get(handle);
        Object networkManager = _networkManager.get(playerConnection);
        Channel channel = CHANNEL.get(networkManager, Channel.class);
        channel.pipeline().addBefore("packet_handler", "mcc", new PacketInterceptor(player));
        _handler.stopTiming();
    }

    private void unhook(Player player) {
        Object handle = _getHandle.invoke(player);
        Object playerConnection = _playerConnection.get(handle);
        Object networkManager = _networkManager.get(playerConnection);
        final Channel channel = CHANNEL.get(networkManager, Channel.class);
        channel.eventLoop().submit(() -> {
            channel.pipeline().remove("mcc");
        });
    }

    private static class PacketInterceptor extends ChannelDuplexHandler {
        private final Player player;

        private PacketInterceptor(Player player) {
            this.player = player;
        }

        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            Packet packet = new Packet(msg);
            PacketEvent event = new PacketEvent(packet, player);
            packet.getType().onSend(event);
            if(!event.isCancelled()) {
                super.write(ctx, event.getPacket().getHandle(), promise);
            }
        }

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            Packet packet = new Packet(msg);
            PacketEvent event = new PacketEvent(packet, player);
            packet.getType().onReceive(event);
            if(!event.isCancelled()) {
                super.channelRead(ctx, event.getPacket().getHandle());
            }
        }
    }
}