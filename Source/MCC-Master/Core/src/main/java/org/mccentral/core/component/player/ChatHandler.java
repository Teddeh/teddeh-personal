package org.mccentral.core.component.player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.mccentral.core.Core;
import org.mccentral.core.component.Component;
import org.mccentral.core.database.MCCPlayer;
import org.mccentral.core.locale.LocaleMessage;
import org.mccentral.core.util.Colour;
import org.mccentral.core.util.Messenger;

/**
 * Created by Teddeh on 9/04/2016.
 */
public class ChatHandler extends Component
{
    public ChatHandler(Core plugin)
    {
        super(plugin);
    }

    @Override
    protected void onEnable()
    {

    }

    @Override
    protected void onDisable()
    {

    }

    @EventHandler
    public void onTalk(AsyncPlayerChatEvent event)
    {
        MCCPlayer player = plugin.getPlayer(event.getPlayer().getUniqueId());
        String format = player.getRank().getName(false) == null ? Colour.Gray : player.getRank().getRankColor(false) + player.getRank().getName(false) + Colour.Reset + " ";
        event.setFormat(Colour.translate(format + event.getPlayer().getName() + Colour.White + ": ") + Colour.Gray + event.getMessage());

        Messenger.message(player, LocaleMessage.WELCOME);
    }
}
