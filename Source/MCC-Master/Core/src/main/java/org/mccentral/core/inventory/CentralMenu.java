package org.mccentral.core.inventory;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.mccentral.core.Core;

import java.util.HashSet;

/**
 * Created by Teddeh on 11/04/2016.
 */
public abstract class CentralMenu implements InventoryHolder
{
	protected Core plugin;

	private final Inventory inventory;
	private CentralMenu parent;

	private final String title;
	private final int rows;
	private final boolean closeOnNullClick, resetCursor;

	private HashSet<MenuItem> items;

	public CentralMenu(Core core, String title, int rows)
	{
		this(core, title, rows, false, null, false);
	}

	public CentralMenu(Core core, String title, int rows, boolean closeOnNullClick, boolean resetCursor)
	{
		this(core, title, rows, closeOnNullClick, null, resetCursor);
	}

	public CentralMenu(Core core, String title, int rows, CentralMenu parent, boolean resetCursor)
	{
		this(core, title, rows, false, parent, resetCursor);
	}

	public CentralMenu(Core core, String title, int rows, boolean closeOnNullClick, CentralMenu parent, boolean resetCursor)
	{
		plugin = core;
		this.title = title;
		this.rows = rows;
		this.parent = parent;
		this.closeOnNullClick = closeOnNullClick;
		this.resetCursor = resetCursor;

		items = new HashSet<>();
		inventory = Bukkit.createInventory(this, (rows * 9), title);
	}

	public void setParent(CentralMenu parent)
	{
		this.parent = parent;
	}

	public void addItem(MenuItem item)
	{
		items.add(item);
	}

	public void openInventory(Player player)
	{
		if (inventory == null) return;

		inventory.clear();
		for (MenuItem item : items)
		{
			inventory.setItem(item.getIndex(), item.getItemStack());
		}

		player.openInventory(inventory);
	}

	@Override
	public Inventory getInventory()
	{
		return inventory;
	}

	public String getTitle()
	{
		return title;
	}

	public int getRows()
	{
		return rows;
	}

	public boolean isCloseOnNullClick()
	{
		return closeOnNullClick;
	}

	public CentralMenu getParent()
	{
		return parent;
	}

	public HashSet<MenuItem> getItems()
	{
		return items;
	}

	public boolean isResetCursor()
	{
		return resetCursor;
	}
}
