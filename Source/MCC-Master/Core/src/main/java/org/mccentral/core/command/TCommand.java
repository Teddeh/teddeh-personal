package org.mccentral.core.command;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.mccentral.core.Core;
import org.mccentral.core.message.MessageChat;
import org.mccentral.core.message.MessageRunCommand;
import org.mccentral.core.util.Rank;

/**
 * Created by Teddeh on 9/04/2016.
 */
public class TCommand extends CentralCommand
{
    //TODO: This is just for Jedis testing, don't delete YET

    public TCommand(Core core)
    {
        super(core,
                "t",
                new String[]{},
                Rank.OWNER,
                "",
                "",
                "",
                Sender.BOTH);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args)
    {
        if(args.length < 1)
            return;

        if (args[0].equals("c"))
        {
            MessageRunCommand message = new MessageRunCommand(StringUtils.join(args, " ", 1, args.length));
            core.getJedis().sendMessage(message, "all");
        } else if(args.length > 2)
        {
            String sender = args[0];
            String to = args[1];
            String msg = StringUtils.join(args, " ", 2, args.length);
            MessageChat message = new MessageChat(to, "&c[" + sender +" ->] &f" + msg);
            core.getJedis().sendMessage(message, "all");
        }
    }
}
