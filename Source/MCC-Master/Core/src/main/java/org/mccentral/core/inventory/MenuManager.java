package org.mccentral.core.inventory;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.mccentral.core.Core;
import org.mccentral.core.component.Component;

/**
 * Created by Teddeh on 11/04/2016.
 */
public class MenuManager extends Component
{
	public MenuManager(Core plugin)
	{
		super(plugin);
	}

	@Override
	protected void onEnable()
	{
	}

	@Override
	protected void onDisable()
	{

	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event)
	{
		if (event.getInventory() == null) return;

		InventoryHolder holder = event.getInventory().getHolder();
		if (holder == null) return;

		if (!(holder instanceof CentralMenu)) return;

		event.setCancelled(true);
		if (!(event.getWhoClicked() instanceof Player)) return;

		Player player = (Player) event.getWhoClicked();
		CentralMenu menu = (CentralMenu) holder;
		ItemStack clicked = event.getCurrentItem();
		if (clicked == null)
		{
			if (menu.isCloseOnNullClick())
			{
				if (menu.getParent() != null)
				{
					if(menu.isResetCursor()) player.closeInventory();
					menu.getParent().openInventory(player);
					return;
				}

				player.closeInventory();
				return;
			}

			return;
		}

		if (clicked.getType() == Material.AIR) return;

		ClickType clickType = event.getClick();

		if (clickType == null) return;

		for (MenuItem menuItem : menu.getItems())
		{
			if (!menuItem.getItemStack().equals(clicked)) continue;

			menuItem.click(player, clickType);
			break;
		}
	}
}
