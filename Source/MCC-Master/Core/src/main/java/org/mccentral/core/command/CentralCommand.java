package org.mccentral.core.command;

import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;
import org.mccentral.core.Core;
import org.mccentral.core.chat.ChatType;
import org.mccentral.core.util.Colour;
import org.mccentral.core.util.PlayerUtil;
import org.mccentral.core.util.Rank;
import org.mccentral.core.util.ServerUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Teddeh on 10/04/2016.
 */
public abstract class CentralCommand extends BukkitCommand
{
	private final String command, suggest, tooltip, description;
	private final String[] aliases;
	private final Rank requiredRank;
	private final Sender sender;
	protected Core core;
	private List<Rank> blacklistedRanks = Lists.newArrayList();

	public CentralCommand(Core core, String command, String[] aliases, Rank requiredRank, String suggest, String tooltip, String description, Sender sender, Rank... blacklist)
	{
		super(command);
		this.core = core;
		this.command = command;
		this.aliases = aliases;
		this.requiredRank = requiredRank;
		this.suggest = suggest;
		this.tooltip = tooltip;
		this.description = description;
		this.sender = sender;
		for (Rank rank : blacklist)
			blacklistedRanks.add(rank);

		register(command);
	}

	@Override
	public final boolean execute(CommandSender commandSender, String s, String[] strings)
	{
		if (sender.equals(Sender.PLAYER) && commandSender instanceof Player)
			/*if (hasPermission(commandSender)) */execute(commandSender, strings);

		else if (sender.equals(Sender.CONSOLE) && (!(commandSender instanceof Player)))
			execute(commandSender, strings);

		else if (sender.equals(Sender.BOTH)) /*if (hasPermission(commandSender))*/ execute(commandSender, strings);

		return true;
	}

	public abstract void execute(CommandSender commandSender, String[] args);

	private void register(String command)
	{
		try
		{
			List<String> stringList = new ArrayList<>();
			Field field = Bukkit.getServer().getClass().getDeclaredField("commandMap");
			field.setAccessible(true);
			CommandMap commandMap = (CommandMap) field.get(Bukkit.getServer());
			for(String str : aliases) stringList.add(str);
			setAliases(stringList);
			setDescription(description);
			commandMap.register(command, this);
			ServerUtil.log(Level.INFO, "Registered Command: " + command);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	public boolean hasPermission(CommandSender sender)
	{
		if (sender instanceof Player)
		{
			Rank rank = core.getPlayer(((Player) sender).getUniqueId()).getRank(); //GET PLAYER RANK
			if (blacklistedRanks.contains(rank)) return false;
			if (rank.hasRank(getRequiredRank())) return true;
		} else
		{
			return true; //console
		}

		PlayerUtil.message((Player) sender, ChatType.PERMISSION, "This command requires " + getRequiredRank().getRankColor(false) + getRequiredRank().getName(true) + Colour.Reset + ".");
		return false;
	}


	public String getCommand()
	{
		return command;
	}

	public String getSuggest()
	{
		return suggest;
	}

	public String getTooltip()
	{
		return tooltip;
	}

	public Rank getRequiredRank()
	{
		return requiredRank;
	}

	public String[] getArrayAliases()
	{
		return aliases;
	}

	@Override
	public String getDescription()
	{
		return description;
	}

	public enum Sender
	{
		PLAYER,
		CONSOLE,
		BOTH
	}
}
