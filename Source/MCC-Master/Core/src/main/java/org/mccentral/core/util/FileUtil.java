package org.mccentral.core.util;

import org.apache.commons.io.FileUtils;
import org.mccentral.core.Core;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

/**
 * Created by Teddeh on 13/04/2016.
 */
public class FileUtil
{
	public static void copyFiles(File from, File to)
	{
		if(from == null || !from.exists())
		{
			ServerUtil.log(Level.SEVERE, "From Directory is null");
			return;
		}

		if(to == null || !to.exists())
		{
			to.mkdir();
			ServerUtil.log(Level.SEVERE, "Creating directory, " + to.getAbsolutePath());
		}


		try
		{
			FileUtils.copyDirectory(from, to);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	public static void deleteFile(File file)
	{
		try
		{
			FileUtils.deleteDirectory(file);
		} catch (IOException e)
		{
			e.printStackTrace();

		}

	}
}
