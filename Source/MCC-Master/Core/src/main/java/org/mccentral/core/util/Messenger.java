package org.mccentral.core.util;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.json.simple.JSONObject;
import org.mccentral.core.database.MCCPlayer;
import org.mccentral.core.locale.LocaleMessage;
import org.mccentral.core.locale.LocaleType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Messenger
{
	private static HashMap<LocaleType, Properties> locales = new HashMap<>();

	public Messenger()
	{
		File container = new File(Bukkit.getWorldContainer(), "locales");
		if (!container.exists()) container.mkdir();

		for (File localeFile : container.listFiles())
		{
			try
			{
				if (localeFile == null) continue;
				if (!localeFile.getName().contains(".properties")) continue;

				LocaleType localeType = LocaleType.valueOf(localeFile.getName().split(".properties")[0]);
				if (localeType == null) continue;

				Properties properties = new Properties();
				properties.load(new FileInputStream(localeFile));
				locales.put(localeType, properties);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void message(MCCPlayer player, LocaleMessage message)
	{
		if(Bukkit.getPlayer(player.getUUID()) == null) return;
		if(!player.getLocale().isEnabled()) return;
		if(!locales.containsKey(player.getLocale())) return;

		Properties property = locales.get(player.getLocale());
		if(!property.containsKey(message.getKey())) return;

		String str = property.getProperty(message.getKey());
		if(str == "" || str == null) return;

		IChatBaseComponent baseComponent = new IChatBaseComponent.ChatSerializer().a(JSONObject.escape(str));
		PlayerUtil.sendPacket(Bukkit.getPlayer(player.getUUID()), new PacketPlayOutChat(baseComponent));
	}
}
