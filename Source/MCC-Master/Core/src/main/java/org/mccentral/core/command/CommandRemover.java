package org.mccentral.core.command;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.mccentral.core.util.ServerUtil;

import java.lang.reflect.Field;
import java.util.logging.Level;

/**
 * Created: 27/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CommandRemover
{
	public CommandRemover()
	{
		try
		{
			Field field = Bukkit.getServer().getClass().getDeclaredField("commandMap");
			field.setAccessible(true);
			CommandMap commandMap = (CommandMap) field.get(Bukkit.getServer());
			commandMap.clearCommands();

			ServerUtil.log(Level.INFO, "All commands have been removed.");
		}
		catch (NoSuchFieldException | IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
}
