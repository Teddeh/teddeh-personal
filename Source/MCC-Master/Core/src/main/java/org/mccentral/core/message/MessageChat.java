package org.mccentral.core.message;

import org.bukkit.Bukkit;
import org.mccentral.core.util.Colour;

/**
 * Created by Teddeh on 16/04/2016.
 *
 * Send message to user or all users
 */
public class MessageChat extends Message
{
    private String message;
    private String playerReceiver;

    public MessageChat(String playerReceiver, String message)
    {
        this.playerReceiver = playerReceiver;
        this.message = message;
    }

    @Override
    public void onReceive(String sender)
    {
        if (playerReceiver.equals("all")) Bukkit.broadcastMessage(Colour.translate(message));
        else
        {
            if (Bukkit.getPlayer(playerReceiver) == null) return;
            Bukkit.getPlayer(playerReceiver).sendMessage(Colour.translate(message));
        }
    }
}
