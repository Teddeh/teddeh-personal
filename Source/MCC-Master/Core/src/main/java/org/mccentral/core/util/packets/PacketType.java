package org.mccentral.core.util.packets;

import com.google.common.collect.Sets;
import org.mccentral.core.reflection.MCVersion;
import org.mccentral.core.reflection.SafeClass;

import java.util.List;
import java.util.Set;

/**
 * Created by Teddeh on 12/04/2016.
 */
public enum PacketType {
    IN_ABILITIES("InAbilities"),
    IN_ARM_ANIMATION("InArmAnimation"),
    IN_BLOCK_DIG("InBlockDig"),
    IN_BLOCK_PLACE("InBlockPlace"),
    IN_CHAT("InChat"),
    IN_CLIENT_COMMAND("InClientCommand"),
    IN_CLOSE_WINDOW("InCloseWindow"),
    IN_CUSTOM_PAYLOAD("InCustomPayload"),
    IN_ENCHANT_ITEM("InEnchantItem"),
    IN_ENTITY_ACTION("InEntityAction"),
    IN_FLYING("InFlying"),
    IN_HELD_ITEM_SLOT("InHeldItemSlot"),
    IN_KEEP_ALIVE("InKeepAlive"),
    IN_LOOK("InFlying$PacketPlayInLook"),
    IN_POSITION("InFlying$PacketPlayInPosition"),
    IN_POSITION_LOOK("InFlying$PacketPlayInPositionLook"),
    IN_RESOURCE_PACK_STATUS("InResourcePackStatus"),
    IN_SET_CREATIVE_SLOT("InSetCreativeSlot"),
    IN_SETTINGS("InSettings"),
    IN_SPECTATE("InSpectate"),
    IN_STEER_VEHICLE("InSteerVehicle"),
    IN_TAB_COMPLETE("InTabComplete"),
    IN_TRANSACTION("InTransaction"),
    IN_UPDATE_SIGN("InUpdateSign"),
    IN_WINDOW_CLICK("InWindowClick"),


    OUT_ABILITIES("OutAbilities"),
    OUT_ANIMATION("OutAnimation"),
    OUT_ATTACH_ENTITY("OutAttachEntity"),
    OUT_BED("OutBed"),
    OUT_BLOCK_ACTION("OutBlockAction"),
    OUT_BLOCK_BREAK_ANIMATION("OutBlockBreakAnimation"),
    OUT_BLOCK_CHANGE("OutBlockChange"),
    OUT_CAMERA("OutCamera"),
    OUT_CHAT("OutChat"),
    OUT_CLOSE_WINDOW("OutCloseWindow"),
    OUT_COLLECT("OutCollect"),
    OUT_COMBAT_EVENT("OutCombatEvent"),
    OUT_CUSTOM_PAYLOAD("OutCustomPayload"),
    OUT_ENTITY("OutEntity"),
    OUT_ENTITY_DESTROY("OutEntityDestroy"),
    OUT_ENTITY_EFFECT("OutEntityEffect"),
    OUT_ENTITY_EQUIPMENT("OutEntityEquipment"),
    OUT_ENTITY_HEAD_ROTATION("OutEntityHeadRotation"),
    OUT_ENTITY_LOOK("OutEntity$PacketPlayOutEntityLook"),
    OUT_ENTITY_METADATA("OutEntityMetadata"),
    OUT_ENTITY_STATUS("OutEntityStatus"),
    OUT_ENTITY_TELEPORT("OutEntityTeleport"),
    OUT_ENTITY_VELOCITY("OutEntityVelocity"),
    OUT_EXPERIENCE("OutExperience"),
    OUT_EXPLOSION("OutExplosion"),
    OUT_GAME_STATE_CHANGE("OutGameStateChange"),
    OUT_HELD_ITEM_SLOT("OutHeldItemSlot"),
    OUT_KEEP_ALIVE("OutKeepAlive"),
    OUT_KICK_DISCONNECT("OutKickDisconnect"),
    OUT_LOGIN("OutLogin"),
    OUT_MAP("OutMap"),
    OUT_MAP_CHUNK("OutMapChunk"),
    OUT_MAP_CHUNK_BULK("OutMapChunkBulk"),
    OUT_MULTI_BLOCK_CHANGE("OutMultiBlockChange"),
    OUT_NAMED_ENTITY_SPAWN("OutNamedEntitySpawn"),
    OUT_NAMED_SOUND_EFFECT("OutNamedSoundEffect"),
    OUT_OPEN_SIGN_EDITOR("OutOpenSignEditor"),
    OUT_OPEN_WINDOW("OutOpenWindow"),
    OUT_PLAYER_INFO("OutPlayerInfo"),
    OUT_PLAYER_LIST_HEADER_FOOTER("OutPlayerListHeaderFooter"),
    OUT_POSITION("OutPosition"),
    OUT_REL_ENTITY_MOVE("OutEntity$PacketPlayOutRelEntityMove"),
    OUT_REL_ENTITY_MOVE_LOOK("OutEntity$PacketPlayOutRelEntityMoveLook"),
    OUT_REMOVE_ENTITY_EFFECT("OutRemoveEntityEffect"),
    OUT_RESOURCE_PACK_SEND("OutResourcePackSend"),
    OUT_RESPAWN("OutRespawn"),
    OUT_SCOREBOARD_DISPLAY_OBJECTIVE("OutScoreboardDisplayObjective"),
    OUT_SCOREBOARD_OBJECTIVE("OutScoreboardObjective"),
    OUT_SCOREBOARD_SCORE("OutScoreboardScore"),
    OUT_SCOREBOARD_TEAM("OutScoreboardTeam"),
    OUT_SERVER_DIFFICULTY("OutServerDifficulty"),
    OUT_SET_COMPRESSION("OutSetCompression"),
    OUT_SET_SLOT("OutSetSlot"),
    OUT_SPAWN_ENTITY("OutSpawnEntity"),
    OUT_SPAWN_ENTITY_EXPERIENCE_ORB("OutSpawnEntityExperienceOrb"),
    OUT_SPAWN_ENTITY_LIVING("OutSpawnEntityLiving"),
    OUT_SPAWN_ENTITY_PAINTING("OutSpawnEntityPainting"),
    OUT_SPAWN_ENTITY_WEATHER("OutSpawnEntityWeather"),
    OUT_SPAWN_POSITION("OutSpawnPosition"),
    OUT_STATISTIC("OutStatistic"),
    OUT_TAB_COMPLETE("OutTabComplete"),
    OUT_TILE_ENTITY_DATA("OutTileEntityData"),
    OUT_TITLE("OutTitle"),
    OUT_TRANSACTION("OutTransaction"),
    OUT_UPDATE_ATTRIBUTES("OutUpdateAttributes"),
    OUT_UPDATE_ENTITY_NBT("OutUpdateEntityNBT"),
    OUT_UPDATE_HEALTH("OutUpdateHealth"),
    OUT_UPDATE_SIGN("OutUpdateSign"),
    OUT_UPDATE_TIME("OutUpdateTime"),
    OUT_WINDOW_DATA("OutWindowData"),
    OUT_WINDOW_ITEMS("OutWindowItems"),
    OUT_WORLD_BORDER("OutWorldBorder"),
    OUT_WORlD_EVENT("OutWorldEvent"),
    OUT_WORLD_PARTICLES("OutWorldParticles"),


    UNKNOWN("UNKNOWN");

    private final Set<PacketListener> listeners = Sets.newConcurrentHashSet();
    private final String name;
    private final SafeClass template;

    PacketType(String name) {
        this.name = name;
        if(name.equalsIgnoreCase("UNKNOWN")) template = null;
        else template = MCVersion.getNMSClass("PacketPlay" + name);
    }

    void register(PacketListener listener) {
        listeners.add(listener);
    }

    void unregister(PacketListener listener) {
        listeners.remove(listener);
    }

    void onReceive(PacketEvent event) {
        listeners.forEach(l -> l.onPacketReceive(event));
    }

    void onSend(PacketEvent event) {
        listeners.forEach(l -> l.onPacketSend(event));
    }

    public Object newInstance() {
        return template.newInstance();
    }

    public Object newInstance(List<Class> paramTypes, Object... objects) {
        Class<?>[] types = new Class<?>[objects.length];
        return template.getConstructor(paramTypes.toArray(new Class[paramTypes.size()]))
        .newInstance(objects);
    }

    public SafeClass getTemplate() {
        return template;
    }

    public static PacketType getByName(String name) {
        for (PacketType type : values()) {
            String fullName = "PacketPlay" + type.name;
            if (fullName.contains(name)) {
                return type;
            }
        }

        return UNKNOWN;
    }
}
