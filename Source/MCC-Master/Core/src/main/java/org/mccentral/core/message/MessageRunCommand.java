package org.mccentral.core.message;

import org.bukkit.Bukkit;

/**
 * Created by Teddeh on 16/04/2016.
 *
 * Run a command on the console of a server
 */
public class MessageRunCommand extends Message
{
    private String command;

    public MessageRunCommand(String command) {
        this.command = command;
    }

    @Override
    public void onReceive(String sender)
    {
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
    }
}
