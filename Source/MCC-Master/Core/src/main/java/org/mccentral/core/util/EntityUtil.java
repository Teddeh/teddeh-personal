package org.mccentral.core.util;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;

/**
 * Created: 07/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class EntityUtil
{
	public static void silence(Entity entity, boolean silence)
	{
		((CraftEntity) entity).getHandle().b(silence);
	}
}
