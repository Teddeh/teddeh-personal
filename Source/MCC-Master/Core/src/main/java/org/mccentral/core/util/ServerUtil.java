package org.mccentral.core.util;

import org.bukkit.Bukkit;
import org.mccentral.core.chat.ChatType;

import java.util.logging.Level;

/**
 * Created by Teddeh on 11/04/2016.
 */
public class ServerUtil
{
	public static final String WEBSITE = "http://www.mccentral.org";
	public static final String WEBSITE_STORE = "http://buy.mccentral.org/";
	public static final String WEBSITE_FORUM = "http://www.mccentral.org/forum";
	public static final String WEBSITE_HOME = WEBSITE + "/home";
	public static final String WEBSITE_VOTE = WEBSITE + "/vote";
	public static final String WEBSITE_APPLY_HELPER = WEBSITE + "/appsprev";
	public static final String WEBSITE_APPLY_BUILDER = WEBSITE + "/buildrequirements";
	public static final String WEBSITE_APPLY_DEVELOPER = WEBSITE + "/developerrequirements";
	public static final String WEBSITE_MEMBERS = WEBSITE + "/users";
	public static final String WEBSITE_MEMBERS_STAFF = WEBSITE + "/staff";

	public static final String SERVER_NAME = "Minecraft Central";
	public static final String SERVER_NAME_SHORT = "MCCentral";
	public static final String SERVER_NAME_TINY = "MCC";

	public static void log(Level level, String message)
	{
		Bukkit.getServer().getLogger().log(level, "[" + SERVER_NAME_SHORT + "] " + message);
	}

	public static void broadcastMessage(String message)
	{
		Bukkit.getOnlinePlayers().forEach(player -> PlayerUtil.message(player, message));
	}

	public static void broadcastMessage(String prefix, String message)
	{
		Bukkit.getOnlinePlayers().forEach(player -> PlayerUtil.message(player, prefix, message));
	}

	public static void broadcastMessage(ChatType chatType, String message)
	{
		Bukkit.getOnlinePlayers().forEach(player -> PlayerUtil.message(player, chatType, message));
	}
}
