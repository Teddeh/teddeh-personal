package org.mccentral.core.cosmetic.addon;

import org.mccentral.core.util.Colour;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum RarityType
{
	COMMON("Common", Colour.Green, 1),
	RARE("Rare", Colour.Blue, 2),
	EPIC("Epic", Colour.LightPurple, 3),
	LEGENDARY("Legendary", Colour.Gold, 4),
	CLASSIFIED("Classified", Colour.Red, 5);

	private String name, color;
	private int ordinal;

	RarityType(String name, String color, int ordinal)
	{
		this.name = name;
		this.color = color;
		this.ordinal = ordinal;
	}

	public String getName()
	{
		return name;
	}

	public String getName(boolean uppercase)
	{
		return uppercase ? name.toUpperCase() : name;
	}

	public String getColor()
	{
		return color;
	}

	public String getColor(boolean bold)
	{
		return bold ? color + Colour.Bold : color;
	}

	public int getOrdinal()
	{
		return ordinal;
	}
}
