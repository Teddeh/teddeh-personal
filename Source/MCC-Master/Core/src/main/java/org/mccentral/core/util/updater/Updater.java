package org.mccentral.core.util.updater;

import org.mccentral.core.Core;
import org.mccentral.core.util.TimeUtil;
import org.mccentral.core.util.UpdateType;

/**
 * Created: 27/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Updater implements Runnable
{
	private Core core;

	public Updater(Core core)
	{
		this.core = core;
		core.getServer().getScheduler().scheduleSyncRepeatingTask(core, this, 0L, 1L);
	}

	@Override
	public void run()
	{
		for (UpdateType update : UpdateType.values())
		{
			if (!TimeUtil.elapsed(update.getTimeLeft(), update.getMilliseconds())) continue;

			update.setTimeLeft(System.currentTimeMillis());
			core.getServer().getPluginManager().callEvent(new ScheduleEvent(update));
		}
	}
}
