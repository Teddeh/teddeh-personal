package org.mccentral.core.cosmetic.gadget.gui;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mccentral.core.Core;
import org.mccentral.core.builder.ItemStackBuilder;
import org.mccentral.core.cosmetic.gadget.Gadget;
import org.mccentral.core.cosmetic.gadget.GadgetManager;
import org.mccentral.core.cosmetic.gadget.GadgetType;
import org.mccentral.core.inventory.CentralMenu;
import org.mccentral.core.inventory.MenuItem;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class GadgetMenu extends CentralMenu
{
	public GadgetMenu(Core core, GadgetManager gadgetManager)
	{
		this(core, gadgetManager, 1);
	}

	private GadgetMenu(Core core, GadgetManager gadgetManager, int page)
	{
		super(core, "Gadgets (Page " + page + ")", 6);

		int perPage = 25, pages = 1, x = GadgetType.values().length, z = 0;
		if (x / perPage > 0) pages = x / perPage;
		if (x % perPage > 0) pages += 1;
		if (page > pages) return;
		System.out.println((page * perPage) - perPage + " - " + page * perPage);
		for (int i = ((page * perPage) - perPage); i < (page * perPage); i++)
		{
			if (i >= x) break;
			GadgetType gadget = GadgetType.values()[i];
			addItem(new MenuItem(z, new ItemStackBuilder(gadget.getMaterial()).build())
			{
				@Override
				public void click(Player player, ClickType clickType)
				{
					gadgetManager.giveGadget(player, gadget);
				}
			});
			z += 1;
		}

		if (page < pages)
		{
			System.out.print(page);
			System.out.print(pages);
			int slot = ((getRows() - 1) * 9) + 9 - 1;
			addItem(new MenuItem(slot, new ItemStackBuilder(Material.ARROW).setName("Next Page").build())
			{
				@Override
				public void click(Player player, ClickType clickType)
				{
					new GadgetMenu(core, gadgetManager, page + 1).openInventory(player);
				}
			});
		}

		if (page > 1)
		{
			int slot = ((getRows() - 1) * 9);
			addItem(new MenuItem(slot, new ItemStackBuilder(Material.ARROW).setName("Back Page").build())
			{
				@Override
				public void click(Player player, ClickType clickType)
				{
					new GadgetMenu(core, gadgetManager, page - 1).openInventory(player);
				}
			});
		}
	}
}
