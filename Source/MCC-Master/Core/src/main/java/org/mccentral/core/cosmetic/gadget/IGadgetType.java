package org.mccentral.core.cosmetic.gadget;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
@Target(ElementType.TYPE_USE)
@Retention(RetentionPolicy.RUNTIME)
public @interface IGadgetType
{
	GadgetType type();
}
