package org.mccentral.core.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * Created by Teddeh on 11/04/2016.
 */
public class LocationUtil
{
	public static String locationToString(Location location)
	{
		return location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getZ() + ";" + location.getYaw() + ";" + location.getPitch();
	}

	public static Location stringToLocation(String location)
	{
		String[] str = location.split(";");
		return new Location(Bukkit.getWorld(str[0]), Double.parseDouble(str[1]),
				Double.parseDouble(str[2]), Double.parseDouble(str[3]), Float.valueOf(str[4]), Float.valueOf(str[5]));
	}

	public static Location lookAt(Location loc, Location lookAt, boolean yaw, boolean pitch)
	{
		loc = loc.clone();
		double x = lookAt.getX()-loc.getX(), y = lookAt.getY()-loc.getY(), z = lookAt.getZ()-loc.getZ();
		if(yaw)
		{
			if (x != 0)
			{
				float f = 0;
				if (x < 0) f = (float) (1.5 * Math.PI);
				else f = (float) (0.5 * Math.PI);

				loc.setYaw(f - (float) Math.atan(z / x));
			} else if (z < 0) loc.setYaw((float) Math.PI);

			loc.setYaw(-loc.getYaw() * 180f / (float)Math.PI);
		}

		if(pitch)
		{
			double xz = Math.sqrt(Math.pow(x, 2) + Math.pow(z, 2));
			loc.setPitch((float) -Math.atan(y / xz));
			loc.setPitch(loc.getPitch() * 180f / (float)Math.PI);
		}

		return loc;
	}


	public static Location stringTobLocation(String s)
	{
		String[] str = s.split(";");
		return new Location(Bukkit.getWorld(str[0]), Integer.parseInt(str[1]),
				Integer.parseInt(str[2]), Integer.parseInt(str[3]));
	}
}
