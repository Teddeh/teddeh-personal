package org.mccentral.core.component.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.mccentral.core.Core;
import org.mccentral.core.component.Component;
import org.mccentral.core.database.MCCPlayer;

import java.net.InetAddress;
import java.util.UUID;

/**
 * Created by Teddeh on 9/04/2016.
 */
public class Loader extends Component
{
	public Loader(Core plugin)
	{
		super(plugin);
	}


	/**
	 * DATABASE STRUCTURE
	 * <p>
	 * ID - UUID - LASTNAME - LASTSERVER - RANK - MONEY - PREVIOUSNAMES
	 */

	@Override
	protected void onEnable()
	{
		plugin.getRepository().prepareStatement("CREATE TABLE IF NOT EXISTS users (ID INT NOT NULL AUTO_INCREMENT, UUID VARCHAR(45), LASTNAME VARCHAR(16)," + "RANK VARCHAR(20), MONEY BIGINT, LASTSERVER VARCHAR(45), PREVIOUSNAMES TEXT, PRIMARY KEY(ID));").executeUpdate();
		for (Player player : Bukkit.getOnlinePlayers())
			player.kickPlayer("Server restarted!");
	}

	@Override
	protected void onDisable()
	{

	}

	@EventHandler
	public void onPreJoin(AsyncPlayerPreLoginEvent event)
	{
		//Make sure its completely saved from last go
		UUID uuid = event.getUniqueId();
		InetAddress address = event.getAddress();

		MCCPlayer player = new MCCPlayer(uuid);

		try
		{
			player.load(plugin.getRepository());
		} catch (Exception e)
		{
			e.printStackTrace();
			event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
			event.setKickMessage("An error has occured loading your profile. Please alert an Admin!");
			return;
		}

		player.setAddress(address);
		plugin.addPlayer(player);
		player.setServerType(plugin.getServerType());
		player.setName(event.getName());

	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event)
	{
		if (plugin.getPlayer(event.getPlayer().getUniqueId()) == null)
		{
			System.out.println("Couldnt save " + event.getPlayer().getName());
		} else
		{
			plugin.getPlayer(event.getPlayer().getUniqueId()).save(plugin.getRepository());
			plugin.removePlayer(event.getPlayer().getUniqueId());
		}
	}
}
