package org.mccentral.core.cosmetic.gadget;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.mccentral.core.cosmetic.addon.RarityType;
import org.mccentral.core.cosmetic.gadget.addon.TriggerType;
import org.mccentral.core.cosmetic.gadget.event.GadgetTriggerEvent;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class Gadget implements Listener
{
	private final JavaPlugin plugin;
	private final RarityType rarityType;
	private final TriggerType triggerType;
	private final String name;
	private final int id;

	private boolean registered = false;

	public Gadget(JavaPlugin plugin, String name, int id, RarityType rarityType, TriggerType triggerType)
	{
		this.plugin = plugin;
		this.name = name;
		this.id = id;
		this.rarityType = rarityType;
		this.triggerType = triggerType;
	}

	public void trigger(GadgetTriggerEvent event){}

	public abstract GadgetType getGadgetType();

	public final void register()
	{
		if(plugin == null) return;
		if(registered) return;
		Bukkit.getPluginManager().registerEvents(this, plugin);
		registered = true;
	}

	public final void unregister()
	{
		if(!registered) return;
		HandlerList.unregisterAll(this);
		registered = false;
	}

	public final JavaPlugin getPlugin()
	{
		return plugin;
	}

	public String getName()
	{
		return name;
	}

	public int getId()
	{
		return id;
	}

	public final RarityType getRarityType()
	{
		return rarityType;
	}

	public final TriggerType getTriggerType()
	{
		return triggerType;
	}

	public boolean isRegistered()
	{
		return registered;
	}
}
