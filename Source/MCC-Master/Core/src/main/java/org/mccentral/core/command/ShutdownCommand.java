package org.mccentral.core.command;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;
import org.mccentral.core.Core;
import org.mccentral.core.util.Colour;
import org.mccentral.core.util.Rank;

/**
 * Created: 27/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class ShutdownCommand extends CentralCommand
{
	public ShutdownCommand(Core core)
	{
		super(
				core,
				"stop",
				new String[]{"shutdown"},
				Rank.ADMIN,
				"/shutdown <reason>",
				"tooltip",
				"description",
				Sender.BOTH,
				new Rank[]{}
		);
	}

	@Override
	public void execute(CommandSender commandSender, String[] args)
	{
		core.disable();

		new BukkitRunnable()
		{
			public void run()
			{
				Bukkit.getOnlinePlayers().forEach(p -> p.kickPlayer(Colour.YellowB + "Minecraft Central" + Colour.Reset + "\nThis server is restarting."));
				Bukkit.getServer().shutdown();
			}
		}.runTaskLater(core, 40);
	}
}
