package org.mccentral.core.component.player;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;
import org.mccentral.core.util.SequenceUtil;
import org.mccentral.core.util.ServerUtil;

import java.util.logging.Level;

/**
 * Created: 24/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class NameTag
{
	public NameTag(Player player, String prefix, String suffix, Object rank, Object[] allRanks)
	{
		Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
		Team team;
		String code = null;
		for (int i = 0; i < allRanks.length; i++)
		{
			if (!rank.equals(allRanks[i])) continue;
			code = SequenceUtil.indexToColumn(i+1);
			if (scoreboard.getTeam(code) == null) continue;
			if (!scoreboard.getTeam(code).hasEntry(player.getName())) continue;
			scoreboard.getTeam(code).removeEntry(player.getName());
		}

		if (scoreboard.getTeam(code) == null)
		{
			team = scoreboard.registerNewTeam(code);
			if(prefix != null) team.setPrefix(prefix);
			if(suffix != null) team.setSuffix(suffix);
			team.setDisplayName(code);
			team.setNameTagVisibility(NameTagVisibility.ALWAYS);
			ServerUtil.log(Level.INFO, "Team created.");
		} else
		{
			team = scoreboard.getTeam(code);
			if(prefix != null && !prefix.equals("")) team.setPrefix(prefix);
			if(suffix != null && !suffix.equals("")) team.setSuffix(suffix);
			team.setDisplayName(code);
			team.setNameTagVisibility(NameTagVisibility.ALWAYS);
			ServerUtil.log(Level.INFO, "Existing Team recovered.");
		}

		if (!team.hasEntry(player.getName()))
		{
			ServerUtil.log(Level.INFO, "Team Entry added, " + player.getName() + ".");
			team.addEntry(player.getName());
		}
	}

	public static void removePlayerTag(Player player)
	{
		if (player == null || !player.isOnline()) return;
		Scoreboard scoreboard = player.getScoreboard();
		if (scoreboard == null)
		{
			ScoreboardManager manager = Bukkit.getScoreboardManager();
			scoreboard = manager.getMainScoreboard();
		}

		Team team = null;
		for(Team t : scoreboard.getTeams())
		{
			if(!t.getEntries().contains(player.getName())) continue;
			team = t;
		}

		if (team == null) return;
		if (!team.hasEntry(player.getName())) return;
		team.removeEntry(player.getName());
	}
}
