package org.mccentral.core.cosmetic.addon;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public interface CosmeticBuyable
{
	int getPrice();
}
