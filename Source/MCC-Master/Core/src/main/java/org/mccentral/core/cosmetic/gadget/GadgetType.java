package org.mccentral.core.cosmetic.gadget;

import org.bukkit.Material;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum GadgetType
{
	ARROW_JOY_RIDE(1, Material.ARROW, (byte) 0);

	private int id;
	private Material material;
	private byte itemByte;

	GadgetType(int id, Material material, byte itemByte)
	{
		this.id = id;
		this.material = material;
		this.itemByte = itemByte;
	}

	public int getId()
	{
		return id;
	}

	public Material getMaterial()
	{
		return material;
	}

	public byte getItemByte()
	{
		return itemByte;
	}
}
