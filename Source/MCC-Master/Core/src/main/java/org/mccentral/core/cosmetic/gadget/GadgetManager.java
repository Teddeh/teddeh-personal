package org.mccentral.core.cosmetic.gadget;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.mccentral.core.cosmetic.CosmeticManager;
import org.mccentral.core.cosmetic.gadget.addon.TriggerType;
import org.mccentral.core.cosmetic.gadget.event.GadgetTriggerEvent;
import org.mccentral.core.cosmetic.gadget.gadgets.ArrowGadget;
import org.mccentral.core.util.ServerUtil;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

/**
 * Created: 04/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class GadgetManager extends CosmeticManager
{
	private HashMap<UUID, GadgetType> activeGadgets;
	private final List<Gadget> gadgets;

	public GadgetManager(JavaPlugin plugin)
	{
		super(plugin);

		activeGadgets = new HashMap<>();
		gadgets = Arrays.asList(
			new ArrowGadget(plugin)
		);
	}

	public void giveGadget(Player player, GadgetType gadgetType)
	{
		if(hasActiveGadget(player)) removeGadget(player);
		activeGadgets.put(player.getUniqueId(), gadgetType);
		if(!isRegistered(gadgetType)) registerGadget(gadgetType);

		player.getInventory().setItem(4, new ItemStack(gadgetType.getMaterial(), 1, gadgetType.getItemByte()));
		player.updateInventory();
	}

	public boolean hasActiveGadget(Player player)
	{
		return activeGadgets.containsKey(player.getUniqueId());
	}

	public void removeGadget(Player player)
	{
		GadgetType type = activeGadgets.get(player.getUniqueId());
		activeGadgets.remove(player.getUniqueId());
		if(!isMoreActive(type)) unregisterGadget(type);
	}

	public Gadget getGadget(Player player)
	{
		for(Gadget gadget : gadgets)
		{
			if(gadget.getGadgetType() != getGadgetType(player)) continue;
			return gadget;
		}

		return null;
	}

	public GadgetType getGadgetType(Player player)
	{
		return activeGadgets.get(player.getUniqueId());
	}

	private boolean isMoreActive(GadgetType gadgetType)
	{
		for(GadgetType type : activeGadgets.values())
		{
			if(type != gadgetType) continue;
			return true;
		}

		return false;
	}

	private void unregisterGadget(GadgetType gadgetType)
	{
		for(Gadget gadget : gadgets)
		{
			if(gadget.getGadgetType() != gadgetType) continue;
			gadget.unregister();
			break;
		}
	}

	public void registerGadget(GadgetType gadgetType)
	{
		for(Gadget gadget : gadgets)
		{
			if(gadget.getGadgetType() != gadgetType) continue;
			gadget.register();
			break;
		}
	}

	private boolean isRegistered(GadgetType gadgetType)
	{
		for(Gadget gadget : gadgets)
		{
			if(gadget.getGadgetType() != gadgetType) continue;
			return gadget.isRegistered();
		}

		return false;
	}

	@EventHandler
	public void trigger(PlayerInteractEvent event)
	{
		if(!hasActiveGadget(event.getPlayer())) return;
		Player player = event.getPlayer();
		Gadget gadget = getGadget(player);
		if(event.getItem() == null || event.getItem().getType() == Material.AIR) return;
		if(event.getItem().getType() != gadget.getGadgetType().getMaterial()) return;

		if(gadget.getTriggerType() == TriggerType.LEFT_CLICK)
		{
			if(event.getAction() != Action.LEFT_CLICK_AIR && event.getAction() != Action.LEFT_CLICK_BLOCK) return;
			GadgetTriggerEvent triggerEvent = new GadgetTriggerEvent(player, gadget);
			Bukkit.getPluginManager().callEvent(triggerEvent);
			return;
		}

		if(gadget.getTriggerType() == TriggerType.RIGHT_CLICK)
		{
			if(event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
			GadgetTriggerEvent triggerEvent = new GadgetTriggerEvent(player, gadget);
			Bukkit.getPluginManager().callEvent(triggerEvent);
			return;
		}
	}
}
