package org.mccentral.game.arcade;

import org.mccentral.core.server.ServerType;
import org.mccentral.game.Game;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class Arcade extends Game
{
	@Override
	public void enable()
	{
		super.enable();
	}

	@Override
	public void disable()
	{
		super.disable();
	}

	@Override
	public ServerType getServerType()
	{
		return ServerType.GAME_ARCADE;
	}
}
