package org.mccentral.game.survivalgames.loot.chest;

import org.bukkit.Location;
import org.bukkit.Material;
import org.mccentral.core.builder.ItemStackBuilder;
import org.mccentral.game.survivalgames.loot.item.LootItem;

import java.util.Arrays;

/**
 * Created: 27/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class TierCrate extends LootChest
{
	public TierCrate(Location location)
	{
		super(location);

		addItem(Arrays.asList(
				new LootItem(new ItemStackBuilder(Material.APPLE).build()),
				new LootItem(new ItemStackBuilder(Material.BONE).build()),
				new LootItem(new ItemStackBuilder(Material.SEEDS).setAmount(8).build()),
				new LootItem(new ItemStackBuilder(Material.GRASS).setAmount(4).build()),
				new LootItem(new ItemStackBuilder(Material.STONE).setAmount(2).build())
		));
	}
}
