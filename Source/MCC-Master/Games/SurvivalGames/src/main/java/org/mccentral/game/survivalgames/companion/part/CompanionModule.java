package org.mccentral.game.survivalgames.companion.part;

import org.mccentral.game.survivalgames.companion.Companion;
import org.mccentral.game.survivalgames.companion.part.armorstand.CompanionEquipment;
import org.mccentral.game.survivalgames.companion.part.armorstand.CompanionOptions;

/**
 * Created: 29/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CompanionModule extends CompanionPart
{
	public CompanionModule(Companion parent, String name, int id, double yMod, float offset, float radius, float directionOffset, CompanionEquipment equipment, CompanionOptions options)
	{
		super(parent, name, id, yMod, offset, radius, directionOffset, equipment, options);
	}
}
