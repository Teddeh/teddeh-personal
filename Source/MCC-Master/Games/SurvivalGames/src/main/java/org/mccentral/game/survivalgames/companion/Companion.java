package org.mccentral.game.survivalgames.companion;

import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.mccentral.core.util.PlayerUtil;
import org.mccentral.game.survivalgames.companion.part.CompanionPart;
import org.mccentral.game.survivalgames.companion.part.entity.CompanionEndermite;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created: 29/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class Companion implements Cloneable
{
	private Location spawnLocation;
	private HashSet<CompanionPart> companionParts;
	private boolean active = false, moving = false;
	private Entity holder;
	private final EntityType entityType;
	private Player player;

	public Companion(Player player, EntityType entityType)
	{
		this.entityType = entityType;
		this.player = player;
		this.spawnLocation = player.getLocation();
		this.companionParts = new HashSet<>();
	}

	public Location getSpawnLocation()
	{
		return spawnLocation;
	}

	public HashSet<CompanionPart> getCompanionParts()
	{
		return companionParts;
	}

	public void addCompanionParts(CompanionPart... parts)
	{
		this.companionParts.addAll(Arrays.asList(parts));
	}

	public CompanionPart getPartByName(String name)
	{
		for (CompanionPart part : this.companionParts)
		{
			if (!part.getName().equals(name)) continue;
			return part;
		}

		return null;
	}

	public CompanionPart getPartById(int id)
	{
		for (CompanionPart part : this.companionParts)
		{
			if (part.getId() != id) continue;
			return part;
		}

		return null;
	}

	public Entity getHolder()
	{
		return holder;
	}

	public boolean isActive()
	{
		return active;
	}

	public void spawn()
	{
		holder = this.spawnLocation.getWorld().spawnEntity(this.spawnLocation, entityType);
		if(holder instanceof LivingEntity) CompanionFollower.follow((LivingEntity) holder, player.getUniqueId());

		PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy(holder.getEntityId());
		Bukkit.getOnlinePlayers().forEach(all -> PlayerUtil.sendPacket(all, destroy));
		companionParts.forEach(CompanionPart::spawn);
		active = true;
	}

	public void remove()
	{
		active = false;
		companionParts.forEach(CompanionPart::remove);
		if(holder != null) holder.remove();
	}

	public void setMoving(boolean moving)
	{
		this.moving = moving;
	}

	public boolean isMoving()
	{
		return moving;
	}

	public Player getPlayer()
	{
		return player;
	}
}
