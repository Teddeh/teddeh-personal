package org.mccentral.game.survivalgames.loot.item;

import org.bukkit.inventory.ItemStack;
import org.mccentral.game.survivalgames.loot.LootTier;

/**
 * Created: 27/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class LootItem
{
	private ItemStack itemStack;
	private LootTier lootTier;

	public LootItem(ItemStack itemStack)
	{
		this(itemStack, LootTier.TIER_1);
	}

	public LootItem(ItemStack itemStack, LootTier lootTier)
	{
		this.itemStack = itemStack;
		this.lootTier = lootTier;
	}

	public ItemStack getItemStack()
	{
		return itemStack;
	}

	public LootTier getLootTier()
	{
		return lootTier;
	}
}
