package org.mccentral.game.survivalgames.companion.test;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.mccentral.game.survivalgames.companion.Companion;
import org.mccentral.game.survivalgames.companion.part.CompanionModule;
import org.mccentral.game.survivalgames.companion.part.CompanionPart;
import org.mccentral.game.survivalgames.companion.part.armorstand.CompanionEquipment;
import org.mccentral.game.survivalgames.companion.part.armorstand.CompanionOptions;

/**
 * Created: 29/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Frog extends Companion
{
	public Frog(Player player)
	{
		super(player, EntityType.RABBIT);

		CompanionPart[] parts = new CompanionPart[]
		{
			new CompanionModule(this, "head", 1, -1f/*-1.15*/, 0f, 0f, 0f, new CompanionEquipment().setHelmet(new ItemStack(Material.SKULL_ITEM)), new CompanionOptions(false, false, false)),
			new CompanionModule(this, "leg_right", 1, -0.3f, (float) 45, 0.1f, 0f, new CompanionEquipment().setHand(new ItemStack(Material.STAINED_CLAY)), new CompanionOptions(true, false, false)),
			new CompanionModule(this, "leg_left", 1, -0.3f, (float) -80, 0.5f, 0f, new CompanionEquipment().setHand(new ItemStack(Material.STAINED_CLAY)), new CompanionOptions(true, false, false))
		};

		addCompanionParts(parts);
	}
}
