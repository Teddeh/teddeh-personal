package org.mccentral.game.survivalgames.loot.chest;

import org.bukkit.Location;
import org.mccentral.game.survivalgames.loot.item.LootItem;

import java.util.HashSet;

/**
 * Created: 27/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class LootChest
{
	private HashSet<LootItem> items;
	private boolean fresh, empty;
	private Location location;

	public LootChest(Location location)
	{
		this.location = location;
		this.fresh = true;
		this.empty = false;

		this.items = new HashSet<>();
	}

	public Location getLocation()
	{
		return location;
	}

	public boolean isFresh()
	{
		return fresh;
	}

	public boolean isEmpty()
	{
		return empty;
	}

	public HashSet<LootItem> getItems()
	{
		return items;
	}

	public <T> void addItem(T... item)
	{
		for(T t : item) items.add((LootItem) t);
	}

	public void setEmpty(boolean empty)
	{
		this.empty = empty;
	}

	public void setFresh(boolean fresh)
	{
		this.fresh = fresh;
	}
}
