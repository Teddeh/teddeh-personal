package org.mccentral.game.survivalgames;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;
import org.mccentral.core.Core;
import org.mccentral.core.command.CentralCommand;
import org.mccentral.core.util.LocationUtil;
import org.mccentral.core.util.Rank;
import org.mccentral.game.survivalgames.companion.CompanionManager;
import org.mccentral.game.survivalgames.companion.test.BB8;
import org.mccentral.game.survivalgames.companion.test.Frog;

/**
 * Created: 28/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class aCommand extends CentralCommand
{
	private CompanionManager companionManager;

	public aCommand(Core core, CompanionManager companionManager)
	{
		super(core, "a", new String[]{}, Rank.MEMBER, "", "", "", Sender.PLAYER, new Rank[]{});
		this.companionManager = companionManager;
	}

	private int i = 0;

	@Override
	public void execute(CommandSender commandSender, String[] args)
	{
		/*Player player = (Player) commandSender;
		ArmorStand as = (ArmorStand) player.getWorld().spawnEntity(player.getLocation().add(0, .5, 0), EntityType.ARMOR_STAND);

		//TODO: [BUG] Second ArmorStand not spawning in the correct position.
		Location loc = as.getLocation().clone().add(0, 1, 0);
		ArmorStand as2 = (ArmorStand) player.getWorld().spawnEntity(LocationUtil.lookAt(loc.add(loc.getDirection().clone().multiply(0.3f)), player.getLocation(), true, false), EntityType.ARMOR_STAND);
		as2.setSmall(true);

		as.setMarker(true);
		as2.setMarker(true);

//		as.setGravity(false);
//		as.setMarker(true);
		as.getEquipment().setHelmet(new ItemStack(Material.SKULL_ITEM));
		as2.getEquipment().setHelmet(new ItemStack(Material.SKULL_ITEM));

		as.setVisible(false);
		as2.setVisible(false);


		new BukkitRunnable()
		{
			public void run()
			{
				if (player == null)
				{
					as.remove();
					this.cancel();
					return;
				}

				if (i > 36) i = 0;

				Location clone = LocationUtil.lookAt(as.getLocation(), player.getLocation(), true, false);
				clone.setYaw(clone.getYaw() + 90);
				as.teleport(clone);
				clone.setYaw(clone.getYaw() - 90);
				as.setHeadPose(new EulerAngle(Math.toRadians(90), 0, Math.toRadians(i * 10)));

				double radius = 2;
				double yaw = Math.toRadians(player.getLocation().getYaw()) + (Math.PI/2);
//				double x = Math.cos(yaw) * radius + clone.getX();
//				double z = Math.sin(yaw) * radius + clone.getZ();
//				as2.teleport(new Location(as.getWorld(), x, clone.getY() + 1, z));

				double x = player.getLocation().getX() + radius * Math.cos(yaw);
				double z = player.getLocation().getZ() + radius * Math.sin(yaw);
				double y = player.getLocation().getY();

//				Location loc = as.getLocation().clone().add(0, 1, 0);
//				Vector dir = loc.getDirection();
				//player.getEyeLocation().clone().add(player.getLocation().getDirection().clone().multiply(0.2f)); ?
				as2.teleport(new Location(player.getWorld(), x, y, z));

//				if (as.getLocation().distance(player.getLocation()) > 3.2)
//				{
//					as.setGravity(true);
//					as2.setGravity(true);
//					as.setVelocity(new Vector(clone.getDirection().getX() * 0.2, clone.getDirection().getY(), clone.getDirection().getZ() * 0.2));
//					as2.setVelocity(new Vector(as2.getLocation().getDirection().getX() * 0.2, as2.getLocation().getDirection().getY(), as2.getLocation().getDirection().getZ() * 0.2));
//				}
//				else
//				{
//					as.setGravity(false);
//					as2.setGravity(false);
//					if(i != 0 && i != 9 && i != 18 && i != 27 && i != 36)
//						i++;
//					return;
//				}
				i++;
			}
		}.runTaskTimer(core, 0L, 1L);*/

		Player sender = (Player) commandSender;
		companionManager.addCompanion(sender, new BB8(sender));
//		companionManager.addCompanion(sender, new Frog(sender));
	}
}
