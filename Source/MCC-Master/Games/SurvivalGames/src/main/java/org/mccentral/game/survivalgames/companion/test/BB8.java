package org.mccentral.game.survivalgames.companion.test;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;
import org.mccentral.core.builder.SkullBuilder;
import org.mccentral.game.survivalgames.companion.Companion;
import org.mccentral.game.survivalgames.companion.part.CompanionModule;
import org.mccentral.game.survivalgames.companion.part.CompanionModuleAnimated;
import org.mccentral.game.survivalgames.companion.part.CompanionPart;
import org.mccentral.game.survivalgames.companion.part.armorstand.CompanionEntityPart;
import org.mccentral.game.survivalgames.companion.part.armorstand.CompanionEquipment;
import org.mccentral.game.survivalgames.companion.part.armorstand.CompanionOptions;

/**
 * Created: 29/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class BB8 extends Companion
{
	public BB8(Player player)
	{
		super(player, EntityType.PIG);

		int amount = 12;

//		CompanionPart[] parts = new CompanionPart[amount];
//		for(int i = 1; i <= amount; i++)
//		{
//			if(i % 2 == 0)
//			{
//				parts[i - 1] = new CompanionModuleAnimated(this, "hi", i, -1.1, (float) ((2f / amount * Math.PI) * i), 8f, 0f, new CompanionEquipment().setHelmet(new ItemStack(Material.GOLD_BLOCK)), new CompanionOptions(false, false, false))
//				{
//					private int i = 0;
//
//					@Override
//					public Location update()
//					{
//						if (getHolder() == null) return super.update();
//
//						if (i > 36) i = 0;
//						getHolder().setHeadPose(new EulerAngle(0, Math.toRadians(i * 10), 0));
//						i += 1;
//						return super.update();
//					}
//				};
//			}
//			else
//			{
//				parts[i - 1] = new CompanionModuleAnimated(this, "hi", i, -1.1, (float) ((2f / amount * Math.PI) * i), 8f, 0f, new CompanionEquipment().setHelmet(new ItemStack(Material.IRON_BLOCK)), new CompanionOptions(false, false, false))
//				{
//					private int i = 36;
//
//					@Override
//					public Location update()
//					{
//						if (getHolder() == null) return super.update();
//
//						if (i < 0) i = 36;
//						getHolder().setHeadPose(new EulerAngle(0, Math.toRadians(i * 10), 0));
//						i -= 1;
//						return super.update();
//					}
//				};
//			}
//		}

		ItemStack head = new SkullBuilder().setCustomSkull("eyJ0aW1lc3RhbXAiOjE0NTI4MDM4MTI2OTgsInByb2ZpbGVJZCI6IjYyMTQxMTlhODVlOTQzZDZhMjNmZDNhOTNjNjJjODdmIiwicHJvZmlsZU5hbWUiOiJBbGNhdHJheklUQSIsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9jYTNkNGM1OTM4ZmU3ZjdkMmY3MjVkMDJkNjk0YzExMTFlNGQ5YzdlZjQ5Y2JkYmVjZWFhYzlmYmQ5ZiJ9fX0=").build();
//		ItemStack head = new SkullBuilder().build();
//		ItemStack body = new SkullBuilder().build();
		ItemStack body = new SkullBuilder().setCustomSkull("eyJ0aW1lc3RhbXAiOjE0NTI4MDM4NTE5NjksInByb2ZpbGVJZCI6IjYyMTQxMTlhODVlOTQzZDZhMjNmZDNhOTNjNjJjODdmIiwicHJvZmlsZU5hbWUiOiJBbGNhdHJheklUQSIsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9iNDI4Mjk4OWU1MjY4YThjM2MwYmU3OTQ5ZWMyZGUxMzc3N2EzZDc2YTAyZjZmMjNiODJlODRkNDgwNGJlYSJ9fX0=").build();
		CompanionPart[] parts = new CompanionPart[]
		{
			new CompanionModule(this, "head", 1, -0.12, 0f, 0f, 0f, new CompanionEquipment().setHelmet(head), new CompanionOptions(true, false, false)),
			new CompanionModuleAnimated(this, "body", 2, -1.1, (float) -90, 0.3f, 90f, new CompanionEquipment().setHelmet(body), new CompanionOptions(false, false, false)) {
				int i = 0;
				@Override
				public void animate(CompanionEntityPart armorStand)
				{
					if(i >= 36) i=0;
					if(!isMoving())
					{
						if(i==1 || i==10 || i==19 || i==28) return;
					}

					armorStand.setHeadPose(armorStand.fromEulerAngle(new EulerAngle(Math.toRadians(90), 0, Math.toRadians(i * 10))));
					i+=1;
					return;
				}
			}
		};

		addCompanionParts(parts);
	}
}
