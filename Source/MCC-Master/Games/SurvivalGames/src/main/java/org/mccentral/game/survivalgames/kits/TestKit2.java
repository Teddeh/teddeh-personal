package org.mccentral.game.survivalgames.kits;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.mccentral.game.component.game.kit.CentralKit;
import org.mccentral.game.component.game.kit.KitDisplay;
import org.mccentral.game.component.game.kit.KitItem;

/**
 * Created by Teddeh on 12/04/2016.
 */
public class TestKit2 extends CentralKit
{
	public TestKit2()
	{
		super(
				1,                                                             //Game ID
				"Test kit",                                                    //Game Name
				new String[]
				{
					"This is",                                                 //Game Description[0]
					"a test",                                                  //Game Description[1]
					"description",                                             //Game Description[2]
					"for test kit"                                             //Game Description[3]
				},

				new KitDisplay(                                                //Game Display Item
						new ItemStack(Material.STONE),                         //Game Display ItemStack
						EntityType.ZOMBIE                                      //Game Display Entity
				),

				new KitItem[]                                                  //Game Kit Items
				{
					new KitItem(new ItemStack(Material.STONE), 0),             //Game Kit Item [0]
					new KitItem(new ItemStack(Material.DIRT), 1),              //Game Kit Item [1]
					new KitItem(new ItemStack(Material.LOG), 5)                //Game Kit Item [2]
				}
		);
	}
}
