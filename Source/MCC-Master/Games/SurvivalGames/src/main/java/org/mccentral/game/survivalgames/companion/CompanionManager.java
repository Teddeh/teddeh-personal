package org.mccentral.game.survivalgames.companion;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.mccentral.core.util.PlayerUtil;
import org.mccentral.core.util.UpdateType;
import org.mccentral.core.util.updater.ScheduleEvent;
import org.mccentral.game.Game;
import org.mccentral.game.survivalgames.companion.part.CompanionPart;

import java.util.HashMap;

/**
 * Created: 29/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CompanionManager implements Listener
{
	private HashMap<Player, Companion> companions;

	public CompanionManager(Game game)
	{
		Bukkit.getPluginManager().registerEvents(this, game);
		companions = new HashMap<>();
	}

	public void addCompanion(Player player, Companion companion)
	{
		companions.put(player, companion);
		companion.spawn();
	}

	@EventHandler
	public void movement(ScheduleEvent event)
	{
		if(event.getType() != UpdateType.TICK_1)
			return;

		companions.keySet().forEach(player -> {
			Companion companion = companions.get(player);
			if(companion.isActive())
			{
				if((companion.getHolder().getLocation().distance(player.getLocation()) > 2.2))
				{
					if(!companion.isMoving())
						companion.setMoving(true);
				}
				else
				{
					if(companion.isMoving())
						companion.setMoving(false);
				}
			}
		});
	}

	@EventHandler
	public void onCompanionDamage(EntityDamageEvent event)
	{
		Entity entity = event.getEntity();

		for(Companion companion : companions.values())
		{
			if(!companion.getHolder().equals(entity))
			{
				for(CompanionPart part : companion.getCompanionParts())
				{
					if(!part.getHolder().equals(entity)) continue;
					event.setCancelled(true);
					break;
				}
				return;
			}

			event.setCancelled(true);
			break;
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event)
	{
		for(Companion companion : companions.values())
		{
			PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(companion.getHolder().getEntityId());
			PlayerUtil.sendPacket(event.getPlayer(), packet);

			for(CompanionPart part : companion.getCompanionParts())
			{
				CraftEntity entity = (CraftEntity) part.getHolder();
				PacketPlayOutSpawnEntity spawn = new PacketPlayOutSpawnEntity(entity.getHandle(), 30);
				PacketPlayOutEntityTeleport tp = new PacketPlayOutEntityTeleport(entity.getHandle());
				PlayerUtil.sendPacket(event.getPlayer(), spawn, tp);
			}
		}
	}

	@EventHandler
	public void update(ScheduleEvent event)
	{
		if(event.getType() != UpdateType.SEC_2)
		for(Player all : Bukkit.getOnlinePlayers())
		{
			for(Companion companion : companions.values())
			{
				PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(companion.getHolder().getEntityId());
				PlayerUtil.sendPacket(all, packet);
				for(CompanionPart part : companion.getCompanionParts())
				{
					CraftEntity entity = (CraftEntity) part.getHolder();
					PacketPlayOutSpawnEntity spawn = new PacketPlayOutSpawnEntity(entity.getHandle(), 30);
					PacketPlayOutEntityTeleport tp = new PacketPlayOutEntityTeleport(entity.getHandle());
					PlayerUtil.sendPacket(all, spawn, tp);
				}
			}
		}
	}
}
