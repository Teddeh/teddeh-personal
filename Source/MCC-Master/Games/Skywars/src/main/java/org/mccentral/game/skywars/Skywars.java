package org.mccentral.game.skywars;

import org.mccentral.game.Game;
import org.mccentral.game.component.game.CentralGame;
import org.mccentral.game.component.game.GameType;
import org.mccentral.game.component.game.kit.CentralKit;
import org.mccentral.game.component.game.team.TeamGame;

/**
 * Created: 28/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Skywars extends CentralGame implements TeamGame
{
	public Skywars(Game game)
	{
		super(
				game,
				2,
				"Skywars",
				GameType.SKYWARS,
				new String[] {"description"},
				new CentralKit[] {}
		);
	}
}
