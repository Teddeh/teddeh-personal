package org.mccentral.hub;

import org.mccentral.core.Core;
import org.mccentral.core.component.player.ChatHandler;
import org.mccentral.core.cosmetic.gadget.GadgetManager;
import org.mccentral.core.server.ServerType;

/**
 * Created by Teddeh on 09/04/2016.
 */
public class Hub extends Core
{
	private GadgetManager gadgetManager;

	@Override
	public void enable()
	{
		enableComponents(new ChatHandler(this));

		gadgetManager = new GadgetManager(this);
		new TestCommand(this);
	}

	@Override
	public void disable()
	{

	}

	@Override
	public final ServerType getServerType()
	{
		return ServerType.LOBBY;
	}

	public GadgetManager getGadgetManager()
	{
		return gadgetManager;
	}
}
