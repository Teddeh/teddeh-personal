package org.mccentral.punishment.inventory.punish.timeSelector;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.TeddehMenu;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.inventory.punish.punishMenu.UI.PM_Head;
import org.mccentral.punishment.inventory.punish.punishMenu.PunishMenu;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;

import java.sql.Timestamp;

/**
 * Created: 28/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class TimeSelectorMenu extends TeddehMenu
{
	private int weeks = 0, days = 0, hours = 1, mins = 0;

	public TimeSelectorMenu(Punishment plugin, PunishManager punishManager, PlayerCache playerCache, String key, String reason, int[] time)
	{
		super(plugin, "Punishment Time", 5, true, new PunishMenu(plugin, punishManager, playerCache, key, reason), true);

		if(time != null)
		{
			weeks = time[0];
			days = time[1];
			hours = time[2];
			mins = time[3];
		}

		//Items
		addItem(new PM_Head(key, reason));
		addItem(new MenuItem(36, new ItemStackBuilder(Material.ARROW).setName(Colour.Red + "Go Back").build()) {
			@Override public void click(Player player, ClickType clickType)
			{
				player.closeInventory();
				new PunishMenu(plugin, punishManager, playerCache, key, reason).openInventory(player);
			}
		});

		addItem(new MenuItem(19, new ItemStackBuilder(Material.STAINED_GLASS, (byte) 14).setName(Colour.White + "Weeks: " + weeks).build()));
		addItem(new MenuItem(21, new ItemStackBuilder(Material.STAINED_GLASS, (byte) 3).setName(Colour.White + "Days: " + days).build()));
		addItem(new MenuItem(23, new ItemStackBuilder(Material.STAINED_GLASS, (byte) 5).setName(Colour.White + "Hours: " + hours).build()));
		addItem(new MenuItem(25, new ItemStackBuilder(Material.STAINED_GLASS, (byte) 4).setName(Colour.White + "Minutes: " + mins).build()));

		//GO TEMPBAN
		addItem(new MenuItem(44, new ItemStackBuilder(Material.DIAMOND).setName(Colour.Red + "Temp Ban").setLore(Colour.White + "Weeks: " + weeks, Colour.White + "Days: " + days, Colour.White + "Hours: " + hours, Colour.White + "Minutes: " + mins).build()) {
			public void click(Player player, ClickType clickType)
			{
				long w = (weeks <= 0 ? 0 : weeks * 604800000);
				long d = (days <= 0 ? 0 : days * 86400000);
				long h = (hours <= 0 ? 0 : hours * 3600000);
				long m = (mins <= 0 ? 0 : mins * 60000);

				punishManager.punish(player, key, PunishCategory.TEMP_BAN, reason, 1, new Timestamp(w+d+h+m));
				player.closeInventory();
			}
		});

		//UP
		addItem(new MenuItem(10, new ItemStackBuilder(Material.INK_SACK, (byte) 10).setName(Colour.GreenB + "UP").setLore(Colour.Red + "+ 1").build()) {
			public void click(Player player, ClickType clickType)
			{
				if(weeks >= 52) return;

				weeks += 1;
				new TimeSelectorMenu(plugin, punishManager, playerCache, key, reason, new int[] {weeks, days, hours, mins}).openInventory(player);
			}
		});
		addItem(new MenuItem(12, new ItemStackBuilder(Material.INK_SACK, (byte) 10).setName(Colour.GreenB + "UP").setLore(Colour.Aqua + "+ 1").build()) {
			public void click(Player player, ClickType clickType)
			{
				if(days >= 30) return;

				days += 1;
				new TimeSelectorMenu(plugin, punishManager, playerCache, key, reason, new int[] {weeks, days, hours, mins}).openInventory(player);
			}
		});
		addItem(new MenuItem(14, new ItemStackBuilder(Material.INK_SACK, (byte) 10).setName(Colour.GreenB + "UP").setLore(Colour.Green + "+ 1").build()) {
			public void click(Player player, ClickType clickType)
			{
				if(hours >= 24) return;

				hours += 1;
				new TimeSelectorMenu(plugin, punishManager, playerCache, key, reason, new int[] {weeks, days, hours, mins}).openInventory(player);
			}
		});
		addItem(new MenuItem(16, new ItemStackBuilder(Material.INK_SACK, (byte) 10).setName(Colour.GreenB + "UP").setLore(Colour.Yellow + "+ 10").build()) {
			public void click(Player player, ClickType clickType)
			{
				if(mins >= 60) return;

				mins += 10;
				new TimeSelectorMenu(plugin, punishManager, playerCache, key, reason, new int[] {weeks, days, hours, mins}).openInventory(player);
			}
		});

		//DOWN
		addItem(new MenuItem(28, new ItemStackBuilder(Material.INK_SACK, (byte) 13).setName(Colour.GreenB + "DOWN").setLore(Colour.Red + "- 1").build()) {
			public void click(Player player, ClickType clickType)
			{
				if(weeks <= 0) return;

				weeks -= 1;
				new TimeSelectorMenu(plugin, punishManager, playerCache, key, reason, new int[] {weeks, days, hours, mins}).openInventory(player);
			}
		});
		addItem(new MenuItem(30, new ItemStackBuilder(Material.INK_SACK, (byte) 13).setName(Colour.GreenB + "DOWN").setLore(Colour.Aqua + "- 1").build()) {
			public void click(Player player, ClickType clickType)
			{
				if(days <= 0) return;

				days -= 1;
				new TimeSelectorMenu(plugin, punishManager, playerCache, key, reason, new int[] {weeks, days, hours, mins}).openInventory(player);
			}
		});
		addItem(new MenuItem(32, new ItemStackBuilder(Material.INK_SACK, (byte) 13).setName(Colour.GreenB + "DOWN").setLore(Colour.Green + "- 1").build()) {
			public void click(Player player, ClickType clickType)
			{
				if(hours <= 0) return;
				hours -= 1;
				new TimeSelectorMenu(plugin, punishManager, playerCache, key, reason, new int[] {weeks, days, hours, mins}).openInventory(player);
			}
		});
		addItem(new MenuItem(34, new ItemStackBuilder(Material.INK_SACK, (byte) 13).setName(Colour.GreenB + "DOWN").setLore(Colour.Yellow + "- 10").build()) {
			public void click(Player player, ClickType clickType)
			{
				if(mins <= 0) return;

				mins -= 10;
				new TimeSelectorMenu(plugin, punishManager, playerCache, key, reason, new int[] {weeks, days, hours, mins}).openInventory(player);
			}
		});
	}
}
