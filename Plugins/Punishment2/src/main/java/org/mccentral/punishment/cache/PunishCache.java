package org.mccentral.punishment.cache;

import org.mccentral.punishment.PunishCategory;

import java.sql.Timestamp;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class PunishCache
{
	private int id;
	private PunishCategory punishCategory;
	private String reason, staff;
	private Timestamp time;

	public PunishCache(int id, PunishCategory punishCategory, String reason, String staff, Timestamp time)
	{
		this.id = id;
		this.punishCategory = punishCategory;
		this.reason = reason;
		this.staff = staff;
		this.time = time;
	}

	public int getId()
	{
		return id;
	}

	public PunishCategory getPunishCategory()
	{
		return punishCategory;
	}

	public String getReason()
	{
		return reason;
	}

	public String getStaff()
	{
		return staff;
	}

	public Timestamp getTime()
	{
		return time;
	}
}
