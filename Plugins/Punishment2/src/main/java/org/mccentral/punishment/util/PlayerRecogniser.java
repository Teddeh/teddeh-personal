package org.mccentral.punishment.util;

import org.bukkit.Bukkit;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created: 28/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PlayerRecogniser
{
	public static String getAsPlayer(String key)
	{
		if(isIP(key)) return key;
		if(Bukkit.getOfflinePlayer(UUID.fromString(key)) == null) return key;
		return Bukkit.getOfflinePlayer(UUID.fromString(key)).getName();
	}

	private static boolean isIP(String text)
	{
		Pattern pattern = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])|(\\*))$");
		Matcher matcher = pattern.matcher(text);
		return matcher.find();
	}
}
