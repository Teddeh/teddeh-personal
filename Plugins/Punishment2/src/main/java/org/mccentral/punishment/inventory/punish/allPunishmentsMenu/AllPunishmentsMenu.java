package org.mccentral.punishment.inventory.punish.allPunishmentsMenu;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.cache.PunishCache;
import org.mccentral.punishment.cache.type.BanCache;
import org.mccentral.punishment.cache.type.MuteCache;
import org.mccentral.punishment.cache.type.WarnCache;
import org.mccentral.punishment.inventory.api.TeddehMenu;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.inventory.punish.punishMenu.PunishMenu;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;

import java.util.UUID;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class AllPunishmentsMenu extends TeddehMenu
{
	public AllPunishmentsMenu(Punishment plugin, PunishManager punishManager, PlayerCache playerCache, String key, String reason)
	{
		super(plugin, "All punishments", 6, true, new PunishMenu(plugin, punishManager, playerCache, key, reason), true);

		addItem(new MenuItem(45, new ItemStackBuilder(Material.ARROW).setName(Colour.Red + "Go Back").build()) {
			@Override public void click(Player player, ClickType clickType)
			{
				player.closeInventory();
				new PunishMenu(plugin, punishManager, playerCache, key, reason).openInventory(player);
			}
		});

		int[] blacklist = new int[] {45};
		int x = 0;
		for(int i = 0; i < 45; i++)
		{
			x += 1;
			boolean ignore = false;
			for(Integer z : blacklist) {
				if(i == z) {
					ignore = true;
					break;
				}
			}
			if(ignore) continue;

			if(playerCache.getPunishmentsInOrder().size() < x) continue;
			PunishCache punishCache = playerCache.getPunishmentsInOrder().get(x-1);
			if(punishCache.getId() <= 0) continue;

			if(punishCache instanceof BanCache)
			{
				BanCache cache = (BanCache) punishCache;
				addItem(new MenuItem(i, new ItemStackBuilder(Material.EMPTY_MAP).setName(Colour.WhiteB + punishCache.getPunishCategory().toString().replace('_', ' '))
						.setLore(
								Colour.Gray + cache.getReason(), "",
								Colour.White + "Severity: " + Colour.WhiteB + cache.getSeverity() + Colour.Reset + " (" + Colour.RedB + cache.getSubSeverity() + Colour.Reset + ")",
								Colour.White + "Date: " + Colour.Gray + cache.getTime().toString(),
								Colour.White + "Staff: " + Colour.Gray + Bukkit.getOfflinePlayer(UUID.fromString(cache.getStaff())).getName(), "",
								Colour.White + "ID: " + Colour.Red + cache.getId()
						)
						.setAmount(x)
						.build()));
			}
			if(punishCache instanceof MuteCache)
			{
				MuteCache cache = (MuteCache) punishCache;
				addItem(new MenuItem(i, new ItemStackBuilder(Material.EMPTY_MAP).setName(Colour.WhiteB + punishCache.getPunishCategory().toString().replace('_', ' '))
						.setLore(
								Colour.Gray + cache.getReason(), "",
								Colour.White + "Severity: " + Colour.WhiteB + cache.getSeverity() + Colour.Reset + " (" + Colour.RedB + cache.getSubSeverity() + Colour.Reset + ")",
								Colour.White + "Date: " + Colour.Gray + cache.getTime().toString(),
								Colour.White + "Staff: " + Colour.Gray + Bukkit.getOfflinePlayer(UUID.fromString(cache.getStaff())).getName(), "",
								Colour.White + "ID: " + Colour.Red + cache.getId()
						)
						.setAmount(x)
						.build()));
			}
			if(punishCache instanceof WarnCache)
			{
				WarnCache cache = (WarnCache) punishCache;
				addItem(new MenuItem(i, new ItemStackBuilder(Material.EMPTY_MAP).setName(Colour.WhiteB + punishCache.getPunishCategory().toString().replace('_', ' '))
						.setLore(
								Colour.Gray + cache.getReason(), "",
								Colour.White + "Date: " + Colour.Gray + cache.getTime().toString(),
								Colour.White + "Staff: " + Colour.Gray + Bukkit.getOfflinePlayer(UUID.fromString(cache.getStaff())).getName(), "",
								Colour.White + "ID: " + Colour.Red + cache.getId()
						)
						.setAmount(x)
						.build()));
			}
		}
	}
}
