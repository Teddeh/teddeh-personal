package org.mccentral.punishment.inventory.punish.punishMenu.UI.category;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PM_ChatOffenceCatagory extends MenuItem
{
	public PM_ChatOffenceCatagory(PlayerCache playerCache)
	{
		super(11, new ItemStackBuilder(Material.IRON_SWORD).setName(Colour.WhiteB + "Chat Offences").setLore(Colour.GrayI + "Punishment Type: Mute", "", Colour.Yellow + "Total Chat punishments: " + Colour.Gold + playerCache.getTimesPunished(-1, PunishCategory.CHAT_OFFENCE)).build());
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
	}
}
