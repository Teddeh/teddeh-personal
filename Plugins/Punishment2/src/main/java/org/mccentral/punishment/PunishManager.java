package org.mccentral.punishment;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerPreLoginEvent;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.cache.type.BanCache;
import org.mccentral.punishment.cache.type.MuteCache;
import org.mccentral.punishment.cache.type.UnknownCache;
import org.mccentral.punishment.cache.type.WarnCache;
import org.mccentral.punishment.database.SafePrepareStatement;
import org.mccentral.punishment.message.MessageChannel;
import org.mccentral.punishment.severity.SubSeverity;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.Callback;
import org.mccentral.punishment.util.JsonBuilder;
import org.mccentral.punishment.util.PunishPermissions;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PunishManager implements Listener
{
	public HashMap<UUID, PlayerCache> cache = new HashMap<>();

	private Punishment plugin;

	public PunishManager(Punishment plugin)
	{
		this.plugin = plugin;

		Bukkit.getPluginManager().registerEvents(this, plugin);
	}

	public void unPunish(Player staff, String key, PunishType... punishType)
	{
		try
		{
			Connection connection = plugin.getMySQL().getConnection();
			if(connection.isClosed())
			{
				plugin.getMySQL().openConnection(new Callback<Connection>()
				{
					@Override
					public void call(Connection value)
					{
						unPunish(staff, key, punishType);
					}
				});
				return;
			}

			if(punishType == null)
			{
				final String query = "delete from punishment where `KEY`=?;";
				SafePrepareStatement statement = new SafePrepareStatement(plugin, query).set(1, key);
				statement.execute(new Callback<Boolean>()
				{
					@Override
					public void call(Boolean value)
					{
						if (value) System.out.println("UnPunish complete");
						plugin.getLilyMessenger().request(
								new JsonBuilder("key", key),
								MessageChannel.UN_PUNISH
						);
					}
				}, true);
			}
			else
			{
				for(PunishType type : punishType)
				{
					final String query = "update punishment set `EXPIRE`=? where `KEY`=? and `EXPIRE`>" + System.currentTimeMillis() + " and `TYPE`=?;";
					SafePrepareStatement statement = new SafePrepareStatement(plugin, query).set(1, System.currentTimeMillis()).set(2, key);
					statement.set(3, type.toString());
					statement.execute(new Callback<Boolean>()
					{
						@Override
						public void call(Boolean value)
						{
							if (value) System.out.println("UnPunish complete");
							plugin.getLilyMessenger().request(
									new JsonBuilder("type", type.toString())
											.append("key", key),
									MessageChannel.UN_PUNISH
							);
						}
					}, true);
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public void punish(Player punisher, String key, PunishCategory punishCategory, String reason, int severity, Timestamp customTime)
	{
		try
		{
			Connection connection = plugin.getMySQL().getConnection();
			if(connection.isClosed())
			{
				plugin.getMySQL().openConnection(new Callback<Connection>()
				{
					@Override
					public void call(Connection value)
					{
						punish(punisher, key, punishCategory, reason, severity, customTime);
					}
				});
				return;
			}

			/** SELECT */
			final String select = "select `CATEGORY`,`SEVERITY`,`SUB_SEVERITY` from " + plugin.getMySQL().getTable() + " where `KEY`=?;";
			new SafePrepareStatement(plugin, select).set(1, key).executeQuery(new Callback<ResultSet>()
			{
				private int subSev = 1;
				@Override
				public void call(ResultSet result)
				{
					try
					{
						while (result.next())
						{
							if (PunishCategory.valueOf(result.getString("CATEGORY")) != punishCategory) continue;
							if (result.getInt("SEVERITY") != severity) continue;
							subSev += 1;
						}

						if (subSev > 5) subSev = 5;
						SubSeverity[] subSeverity = punishCategory.getSubSeverities(severity);
						if (subSeverity.length < subSev) subSev = subSeverity.length;
						long time = punishCategory.getSubSeverity(severity, subSev).getTime(), expire = (System.currentTimeMillis() + time);

						PunishType punishType = punishCategory.getSubSeverity(severity, subSev).getType();

						/** INSERT */
						final String query = "insert into " + plugin.getMySQL().getTable() + " (`KEY`, `TYPE`, `CATEGORY`, `SEVERITY`, `SUB_SEVERITY`, `TIME`, `EXPIRE`, `STAFF`, `REASON`) values (?,?,?,?,?,?,?,?,?);";
						SafePrepareStatement statement = new SafePrepareStatement(plugin, query);
						statement.set(1, key);
						statement.set(2, punishType.toString());
						statement.set(3, punishCategory.toString());
						statement.set(4, (byte) severity);
						statement.set(5, (byte) subSev);
						statement.set(6, new Timestamp(System.currentTimeMillis()));
						if(customTime == null) statement.set(7, expire); else statement.set(7, System.currentTimeMillis() + customTime.getTime());
						statement.set(8, punisher.getUniqueId().toString());
						statement.set(9, reason);

						statement.execute(new Callback<Boolean>()
						{
							@Override
							public void call(Boolean value)
							{
								if(value) System.out.println("Punishment complete");
								plugin.getLilyMessenger().request(
										new JsonBuilder("category", punishCategory.toString())
											.append("type", punishType.toString())
											.append("punisher", punisher.getName())
											.append("punisherUUID", punisher.getUniqueId().toString())
											.append("key", key)
											.append("severity", String.valueOf(severity))
											.append("subseverity", String.valueOf(subSev))
											.append("time", String.valueOf(System.currentTimeMillis()))
											.append("expire", (customTime == null ? String.valueOf(expire) : String.valueOf(Long.valueOf(System.currentTimeMillis() + customTime.getTime()))))
											.append("reason", reason),
										MessageChannel.PUNISH
								);
							}
						}, true);
					} catch (SQLException e)
					{
						e.printStackTrace();
					}
				}
			}, true);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public String getBanMessage(PunishCategory category, int severity, int subSeverity, String reason, long time, long expireTime, int id)
	{
		String title = "";
		if(category == PunishCategory.CHAT_OFFENCE) title = "You are Muted!";
		else title = "You are Banned!";
		return Colour.RedB + title + Colour.Reset + "\n"
				+ Colour.White + "Reason: " + Colour.Gray + reason + "\n"
				+ (id < 0 ? "\n\n" : Colour.White + "Punishment ID: " + Colour.RedB + id + Colour.Reset + "\n\n")
				+ Colour.Yellow + "Believe your punishment was a mistake?" + "\n"
				+ Colour.Yellow + "Post an appeal at: " + Colour.Gold + "www.mccentral.org/appealpreview" + "\n\n"
				+ Colour.White + "Expires: " + getCalculation(category, severity, subSeverity, expireTime) + Colour.Reset + "\n";
	}

	public String getCalculation(PunishCategory category, int severity, int subSeverity, long time)
	{
		if(category != null)
		{
			PunishType type = category.getSubSeverity(severity, subSeverity).getType();
			if (type == PunishType.BAN_PERM || type == PunishType.MUTE_PERM)
				return Colour.Red + "Permanent " + Colour.White + "(" + Colour.Gray + "Never" + Colour.White + ")";
		}

		long diff = time - System.currentTimeMillis();
		long d = diff / (1000 * 60 * 60 * 24);
		long h = diff / (60 * 60 * 1000) % 24;
		long m = diff / (60 * 1000) % 60;
		long s = diff / 1000 % 60;

		return(!(d <= 0) ? Colour.Gold + d + Colour.Yellow + " Day" + (d > 1 ? "s" : "") + Colour.White + ", " : "")
				+ (!(h <= 0) ? Colour.Gold + h + Colour.Yellow + " Hour" + (h > 1 ? "s" : "") + Colour.White + ", " : "")
				+ (!(m <= 0) ? Colour.Gold + m + Colour.Yellow + " Minute" + (m > 1 ? "s" : "") + Colour.White + ", " : "")
				+ (!(s <= 0) ? Colour.Gold + s + Colour.Yellow + " Second" + (s > 1 ? "s" : "") : "");
	}

	public boolean isIP(String text)
	{
		Pattern pattern = Pattern.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])|(\\*))$");
		Matcher matcher = pattern.matcher(text);
		return matcher.find();
	}

	public boolean hasPerm(Player player, PunishPermissions permission)
	{
		boolean bool = player.hasPermission(permission.getPermission());
		if(!bool) player.sendMessage(Colour.Red + "You don't have permission to perform this action.");
		return bool;
	}

	@EventHandler
	public void onPlayerJoin(AsyncPlayerPreLoginEvent event)
	{
		final List<BanCache> banCache = new ArrayList<BanCache>();
		final List<MuteCache> muteCache = new ArrayList<MuteCache>();
		final List<WarnCache> warnCache = new ArrayList<WarnCache>();
		final List<UnknownCache> unknownCache = new ArrayList<UnknownCache>();

		String[] split = event.getAddress().getHostAddress().split(Pattern.quote("."));
		String ranged = split[0] + "." + split[1] + "." + split[2] + ".*";
		final String select = "select * from `" + plugin.getMySQL().getTable() + "` where `KEY`=? or `KEY`=? or `KEY`='" + ranged + "';";
		new SafePrepareStatement(plugin, select).set(1, event.getUniqueId().toString()).set(2, event.getAddress().getHostAddress()).executeQuery(new Callback<ResultSet>()
		{
			@Override
			public void call(ResultSet result)
			{
				try
				{
					while (result.next())
					{
						PunishType punishType = PunishType.valueOf(result.getString("TYPE"));
						int id = result.getInt("ID");
						long expire = result.getLong("EXPIRE");
						byte severity = result.getByte("SEVERITY"), subSeverity = result.getByte("SUB_SEVERITY");
						Timestamp time = result.getTimestamp("TIME");
						String staff = result.getString("STAFF"), reason = result.getString("REASON");
						PunishCategory punishCategory = PunishCategory.valueOf(result.getString("CATEGORY"));

						if (punishType == PunishType.BAN || punishType == PunishType.BAN_PERM)
							banCache.add(new BanCache(id, punishCategory, reason, staff, time, new Timestamp(expire), severity, subSeverity));
						if (punishType == PunishType.MUTE || punishType == PunishType.MUTE_PERM)
							muteCache.add(new MuteCache(id, punishCategory, reason, staff, time, new Timestamp(expire), severity, subSeverity));
						if (punishType == PunishType.WARNING)
							warnCache.add(new WarnCache(id, punishCategory, reason, staff, time));
						if (punishType == PunishType.UNKNOWN)
							unknownCache.add(new UnknownCache(id, punishCategory, reason, staff, time, new Timestamp(expire)));
					}

					for(BanCache ban : banCache)
					{
						System.out.println(ban.getPunishCategory().getSubSeverity(ban.getSeverity(), ban.getSubSeverity()).getType().toString() + " - " + ban.getExpire().getTime());
						if(ban.getPunishCategory().getSubSeverity(ban.getSeverity(), ban.getSubSeverity()).getType() == PunishType.BAN_PERM)
						{
							if(ban.getExpire().getTime() <= 0)
							{
								event.disallow(PlayerPreLoginEvent.Result.KICK_OTHER, getBanMessage(ban.getPunishCategory(), ban.getSeverity(), ban.getSubSeverity(), ban.getReason(), ban.getTime().getTime(), ban.getExpire().getTime(), ban.getId()));
								break;
							}
						}

						if(ban.getExpire().getTime() > System.currentTimeMillis())
						{
							event.disallow(PlayerPreLoginEvent.Result.KICK_OTHER, getBanMessage(ban.getPunishCategory(), ban.getSeverity(), ban.getSubSeverity(), ban.getReason(), ban.getTime().getTime(), ban.getExpire().getTime(), ban.getId()));
							break;
						}
					}

					cache.put(event.getUniqueId(), new PlayerCache(banCache, muteCache, warnCache, unknownCache));
				}catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}, false);
	}

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event)
	{
		if(!cache.containsKey(event.getPlayer().getUniqueId())) return;
		for(MuteCache muteCache : cache.get(event.getPlayer().getUniqueId()).getMuteCacheList())
		{
			if(muteCache.getPunishCategory().getSubSeverity(muteCache.getSeverity(), muteCache.getSubSeverity()).getType() == PunishType.MUTE_PERM)
			{
				if(muteCache.getExpire().getTime() > System.currentTimeMillis())
				{
					event.setCancelled(true);
					event.getPlayer().sendMessage(getBanMessage(muteCache.getPunishCategory(), muteCache.getSeverity(), muteCache.getSubSeverity(), muteCache.getReason(), muteCache.getTime().getTime(), muteCache.getExpire().getTime(), muteCache.getId()));
					break;
				}
			}
			if(muteCache.getExpire().getTime() > System.currentTimeMillis())
			{
				event.setCancelled(true);
				event.getPlayer().sendMessage(getBanMessage(muteCache.getPunishCategory(), muteCache.getSeverity(), muteCache.getSubSeverity(), muteCache.getReason(), muteCache.getTime().getTime(), muteCache.getExpire().getTime(), muteCache.getId()));
				break;
			}
		}
	}

	private String[] blockedCommands = new String[]
			{
					"msg",
					"m",
					"whisper",
					"w",
					"tell",
					"t",
					"pm",
					"reply",
					"r",
					"mail",
					"emsg",
					"er",
					"ereply",
					"email",
					"action",
					"describe",
					"eme",
					"eaction",
					"edescribe",
					"etell",
					"ewhisper",
					"epm"
			};

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event)
	{
		if(event.isCancelled()) return;

		if(!cache.containsKey(event.getPlayer().getUniqueId())) return;
		if(!cache.get(event.getPlayer().getUniqueId()).getMuteCacheList().isEmpty())
		{
			boolean isMuted = false;
			MuteCache cached = null;
			for (MuteCache muteCache : cache.get(event.getPlayer().getUniqueId()).getMuteCacheList())
			{
				if(muteCache.getExpire().getTime() > System.currentTimeMillis())
				{
					isMuted = true;
					cached = muteCache;
					break;
				}
			}

			if(!isMuted) return;

			String command = event.getMessage();
			if (command.contains(" "))
			{
				command = command.split(Pattern.quote(" "))[0];
			}

			System.out.println(command);

			for (String cmd : blockedCommands)
			{
				if (command.split(Pattern.quote("/"))[1].equalsIgnoreCase(cmd))
				{
					event.setCancelled(true);
					event.getPlayer().sendMessage(getBanMessage(cached.getPunishCategory(), cached.getSeverity(), cached.getSubSeverity(), cached.getReason(), cached.getTime().getTime(), cached.getExpire().getTime(), cached.getId()));
				}
			}
		}
	}
}
