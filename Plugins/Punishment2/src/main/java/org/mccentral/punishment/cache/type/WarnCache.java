package org.mccentral.punishment.cache.type;

import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.cache.PunishCache;

import java.sql.Timestamp;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class WarnCache extends PunishCache
{
	public WarnCache(int id, PunishCategory punishCategory, String reason, String staff, Timestamp time)
	{
		super(id, punishCategory, reason, staff, time);
	}
}
