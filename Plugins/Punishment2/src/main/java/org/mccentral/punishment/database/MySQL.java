package org.mccentral.punishment.database;

import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.util.Callback;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class MySQL extends Database
{
	private final Punishment plugin;
	private final String table;
	private final String user;
	private final String database;
	private final String password;
	private final String port;
	private final String hostname;

	public MySQL(Punishment plugin, String hostname, String port, String database, String table, String username, String password)
	{
		super(plugin);
		this.plugin = plugin;
		this.hostname = hostname;
		this.port = port;
		this.database = database;
		this.table = table;
		this.user = username;
		this.password = password;
	}

	@Override
	public Connection openConnection(Callback<Connection> callback)
	{
		try
		{
			if (checkConnection()) return connection;

			connection = DriverManager.getConnection("jdbc:mysql://" + hostname + ":" + port + "/" + database, user, password);
			callback.call(connection);
			return connection;
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void close() throws SQLException
	{
		this.connection.close();
	}

	public void register()
	{
		if(connection == null) return;
		String table = "CREATE TABLE IF NOT EXISTS `" + getTable() + "`(" +
				"`ID` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT," +
				"`KEY` VARCHAR(50)," +
				"`TYPE` VARCHAR(50)," +
				"`CATEGORY` VARCHAR(50)," +
				"`SEVERITY` TINYINT(4)," +
				"`SUB_SEVERITY` TINYINT(4)," +
				"`TIME` TIMESTAMP NOT NULL," +
				"`EXPIRE` BIGINT(20)," +
				"`STAFF` VARCHAR(36)," +
				"`REASON` VARCHAR(255)," +
				"PRIMARY KEY ( `ID` ))";
		try
		{
			Statement statement = connection.createStatement();
			statement.executeUpdate(table);

			statement.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public String getTable()
	{
		return table;
	}
}

abstract class Database
{

	public Connection connection;
	protected Punishment plugin;

	protected Database(Punishment plugin)
	{
		this.plugin = plugin;
		this.connection = null;
	}

	public abstract Connection openConnection(Callback<Connection> callback) throws SQLException;

	public boolean checkConnection()
	{
		try
		{
			return (this.connection != null) && (!this.connection.isClosed());
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return false;
	}

	public Connection getConnection()
	{
		return this.connection;
	}

	public void closeConnection()
	{
		if (this.connection == null) return;

		try
		{
			this.connection.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}
