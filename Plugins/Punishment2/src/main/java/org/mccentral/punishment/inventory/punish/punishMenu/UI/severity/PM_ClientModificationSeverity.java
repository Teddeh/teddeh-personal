package org.mccentral.punishment.inventory.punish.punishMenu.UI.severity;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;
import org.mccentral.punishment.util.PunishPermissions;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PM_ClientModificationSeverity extends MenuItem
{
	private PunishManager punishManager;
	private int severity;
	private String key, reason;

	public PM_ClientModificationSeverity(PunishManager punishManager, PlayerCache playerCache, String key, String reason, int index, ItemStack itemStack, int severity)
	{
		super(index, itemStack);

		this.punishManager = punishManager;
		this.severity = severity;
		this.key = key;
		this.reason = reason;
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		PunishPermissions permission = PunishPermissions.valueOf("PUNISH_CLIENTMODS_SEV" + severity);
		if(!punishManager.hasPerm(player, permission)) return;

		if(punishManager.cache.containsKey(player.getUniqueId())) {
			if(!punishManager.cache.get(player.getUniqueId()).canIssueSevThree()) {
				player.sendMessage(Colour.Red + "You don't have permission to perform this action.");
				player.closeInventory();
				return;
			}
		}

		punishManager.punish(player, key, PunishCategory.CLIENT_MODIFICATION, reason, severity, null);
		player.closeInventory();
	}

	public static class PM_ClientModificationSeverity1 extends PM_ClientModificationSeverity
	{
		public PM_ClientModificationSeverity1(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
		{
			super(
					punishManager,
					playerCache,
					key,
					reason,
					24,
					new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 5)
							.setName(Colour.Green + "Severity " + Colour.GreenB + "1" + Colour.Reset)
							.setLore("", Colour.White + "1st Offence: 6 Hours", Colour.White + "2nd Offence: 12 Hours", Colour.White + "3rd Offence: 24 Hours", Colour.White + "4th Offence: 48 Hours", Colour.White + "5th+ Offence: 96 Hours", "", Colour.Gray + "Total Sev.1 punishments: " + Colour.Yellow + playerCache.getTimesPunished(1, PunishCategory.CLIENT_MODIFICATION))
							.build(),
					1
			);
		}
	}

	public static class PM_ClientModificationSeverity2 extends PM_ClientModificationSeverity
	{
		public PM_ClientModificationSeverity2(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
		{
			super(
					punishManager,
					playerCache,
					key,
					reason,
					33,
					new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 4)
							.setName(Colour.Yellow + "Severity " + Colour.YellowB + "2" + Colour.Reset)
							.setLore("", Colour.White + "1st Offence: 2 Days", Colour.White + "2nd Offence: 4 Days", Colour.White + "3rd Offence: 6 Days", Colour.White + "4th Offence: 8 Days", Colour.White + "5th+ Offence: 10 Days", "", Colour.Gray + "Total Sev.2 punishments: " + Colour.Yellow + playerCache.getTimesPunished(2, PunishCategory.CLIENT_MODIFICATION))
							.build(),
					2
			);
		}
	}

	public static class PM_ClientModificationSeverity3 extends PM_ClientModificationSeverity
	{
		public PM_ClientModificationSeverity3(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
		{
			super(
					punishManager,
					playerCache,
					key,
					reason,
					42,
					new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 14)
							.setName(Colour.Red + "Severity " + Colour.RedB + "3" + Colour.Reset)
							.setLore("", Colour.White + "1st Offence: 7 Days", Colour.White + "2nd Offence: 14 Days", Colour.White + "3rd Offence: 21 Days", Colour.White + "4th Offence: 28 Days", Colour.White + "5th+ Offence: Permanent", "", Colour.Gray + "Total Sev.3 punishments: " + Colour.Yellow + playerCache.getTimesPunished(3, PunishCategory.CLIENT_MODIFICATION))
							.build(),
					3
			);
		}
	}
}
