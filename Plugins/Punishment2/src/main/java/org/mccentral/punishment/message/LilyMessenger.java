package org.mccentral.punishment.message;

import lilypad.client.connect.api.Connect;
import lilypad.client.connect.api.event.EventListener;
import lilypad.client.connect.api.event.MessageEvent;
import lilypad.client.connect.api.request.impl.MessageRequest;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.scheduler.BukkitRunnable;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.PunishType;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.type.MuteCache;
import org.mccentral.punishment.cache.type.UnknownCache;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.JsonBuilder;
import org.mccentral.punishment.util.PunishPermissions;

import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.Collections;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created: 17/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class LilyMessenger
{
	private Punishment plugin;
	private PunishManager punishManager;

	public LilyMessenger(Punishment plugin, PunishManager punishManager)
	{
		this.plugin = plugin;
		this.punishManager = punishManager;
	}

	public void request(JsonBuilder json, MessageChannel channel)
	{
		try
		{
			Connect c = plugin.getLilyConnect();
			if (!c.isConnected())
			{
				c.connect();
				request(json, channel);
				return;
			}

			MessageRequest request = new MessageRequest(Collections.<String>emptyList(), channel.getChannel(), json.toString()); //servername, channelname (short), message
			c.request(request);
		} catch (Throwable e)
		{
			e.printStackTrace();
		}
	}

	@EventListener
	public void onMessage(MessageEvent event)
	{
		boolean isChannel = false;
		MessageChannel channel = null;
		for (MessageChannel messageChannel : MessageChannel.values())
		{
			if (!event.getChannel().equals(messageChannel.getChannel())) continue;
			channel = messageChannel;
			isChannel = true;
			break;
		}

		if (!isChannel) return;

		String sender = event.getSender();
		String message = "null";
		try
		{
			message = event.getMessageAsString();
		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		if (channel == null) return;
		switch (channel)
		{
			case PUNISH:
				try
				{
					JSONObject jsonObj = new JSONObject((JSONObject) new JSONParser().parse(message));

					PunishCategory punishCategory = PunishCategory.valueOf((String) jsonObj.get("category"));
					final PunishType punishType = (jsonObj.containsKey("type") ? PunishType.valueOf((String) jsonObj.get("type")) : null);

					String staffName = (String) jsonObj.get("punisher"); UUID staffUUID = UUID.fromString((String) jsonObj.get("punisherUUID"));
					String key = (String) jsonObj.get("key"), reason = (String) jsonObj.get("reason");
					int severity = Integer.valueOf((String) jsonObj.get("severity")), subSeverity = Integer.valueOf((String) jsonObj.get("subseverity"));
					long time = Long.valueOf((String) jsonObj.get("time")), expire = Long.valueOf((String) jsonObj.get("expire"));

					//The victim is an IP, lets search for everyone using this IP.
					if(punishManager.isIP(key))
					{
						Bukkit.getOnlinePlayers().forEach(player -> {
							String address = player.getAddress().getAddress().getHostAddress();
							String[] addressSplit = address.split(Pattern.quote(".")), keySplit = key.split(Pattern.quote("."));

							boolean isVictim = false;
							if(keySplit[3].charAt(0) == '*')
							{
								if(keySplit[0].equals(addressSplit[0]) && keySplit[1].equals(addressSplit[1]) && keySplit[2].equals(addressSplit[2]))
									isVictim = true;
							}
							else
							{
								if(keySplit[0].equals(addressSplit[0]) && keySplit[1].equals(addressSplit[1]) && keySplit[2].equals(addressSplit[2]) && keySplit[3].equals(addressSplit[3]))
									isVictim = true;
							}

							if(isVictim)
							{
								if(punishType == PunishType.BAN || punishType == PunishType.BAN_PERM) {
									new BukkitRunnable() {
										public void run() {
											player.kickPlayer(punishManager.getBanMessage(punishCategory, severity, subSeverity, reason, time, expire, -1));
										}
									}.runTask(plugin);
								}
								if(punishType == PunishType.MUTE || punishType == PunishType.MUTE_PERM) {
									player.sendMessage(punishManager.getBanMessage(punishCategory, severity, subSeverity, reason, time, expire, -1));
									punishManager.cache.get(player.getUniqueId()).addMuteCache(new MuteCache(-1, punishCategory, reason, staffUUID.toString(), new Timestamp(time), new Timestamp(expire), severity, subSeverity));
								}
								if(punishType == PunishType.WARNING) {
									player.sendMessage(Colour.RedB + "You have been Warned!");
									player.sendMessage(Colour.Yellow + reason);

									PacketPlayOutTitle[] packets = new PacketPlayOutTitle[] {
											new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + Colour.RedB + "You have been Warned" + "\"}"), 10, 60, 10),
											new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + Colour.Yellow + "By  " + staffName + "\"}"), 10, 60, 10)
									};

									PlayerConnection playerConnection = ((CraftPlayer) player).getHandle().playerConnection;
									for(PacketPlayOutTitle packet : packets) playerConnection.sendPacket(packet);
								}
								if(punishType == PunishType.KICK) {
									new BukkitRunnable() {
										public void run() {
											player.kickPlayer(Colour.RedB + "You have been Kicked!" + "\n" + Colour.Yellow + reason);
										}
									}.runTask(plugin);
								}
								if(punishType == PunishType.REVOKE_REPORT) {
									punishManager.cache.get(player.getUniqueId()).addUnknownCache(new UnknownCache(-1, punishCategory, reason, staffUUID.toString(), new Timestamp(time), new Timestamp(expire)));
								}
								if(punishType == PunishType.UNKNOWN) {
									if(punishCategory == PunishCategory.CLEAR_ALL_PUNISHMENTS) {
										punishManager.cache.get(player.getUniqueId()).removeAll();
										player.sendMessage(Colour.GreenB + "Your punishments have been cleared.");
									}
									else if(punishCategory == PunishCategory.CLEAR_CREATIVE_INVENTORY) {
										player.getInventory().clear();
										player.sendMessage(Colour.Red + "Your inventory has been cleared by, " + staffName);
									}
									else punishManager.cache.get(player.getUniqueId()).addUnknownCache(new UnknownCache(-1, punishCategory, reason, staffUUID.toString(), new Timestamp(time), new Timestamp(expire)));
								}
							}
						});
					}
					//The victim must be a player, lets find that player!
					else
					{
						Bukkit.getOnlinePlayers().forEach(player -> {
							if(player.getUniqueId().toString().equals(key) || player.getName().equals(key))
							{
								if(punishType == PunishType.BAN || punishType == PunishType.BAN_PERM) {
									new BukkitRunnable() {
										public void run() {
											player.kickPlayer(punishManager.getBanMessage(punishCategory, severity, subSeverity, reason, time, expire, -1));
										}
									}.runTask(plugin);
								}
								if(punishType == PunishType.MUTE || punishType == PunishType.MUTE_PERM) {
									player.sendMessage(punishManager.getBanMessage(punishCategory, severity, subSeverity, reason, time, expire, -1));
									punishManager.cache.get(player.getUniqueId()).addMuteCache(new MuteCache(-1, punishCategory, reason, staffUUID.toString(), new Timestamp(time), new Timestamp(expire), severity, subSeverity));
								}
								if(punishType == PunishType.WARNING) {
									player.sendMessage(Colour.RedB + "You have been Warned!");
									player.sendMessage(Colour.Yellow + reason);

									PacketPlayOutTitle[] packets = new PacketPlayOutTitle[] {
											new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + JSONObject.escape(Colour.RedB + "You have been Warned!") + "\"}"), 10, 60, 10),
											new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + Colour.Yellow + JSONObject.escape("By: " + staffName) + "\"}"), 10, 60, 10)
									};

									PlayerConnection playerConnection = ((CraftPlayer) player).getHandle().playerConnection;
									for(PacketPlayOutTitle packet : packets) playerConnection.sendPacket(packet);
								}
								if(punishType == PunishType.KICK) {
									new BukkitRunnable() {
										public void run() {
											player.kickPlayer(Colour.RedB + "You have been Kicked!" + "\n" + Colour.Yellow + reason);
										}
									}.runTask(plugin);
								}
								if(punishType == PunishType.UNKNOWN) {
									if(punishCategory == PunishCategory.CLEAR_ALL_PUNISHMENTS) {
										punishManager.cache.get(player.getUniqueId()).removeAll();
										player.sendMessage(Colour.GreenB + "Your punishments have been cleared.");
									}
									else if(punishCategory == PunishCategory.CLEAR_CREATIVE_INVENTORY) {
										player.getInventory().clear();
										player.sendMessage(Colour.Red + "Your inventory has been cleared by, " + staffName);
									}
									else punishManager.cache.get(player.getUniqueId()).addUnknownCache(new UnknownCache(-1, punishCategory, reason, staffUUID.toString(), new Timestamp(time), new Timestamp(expire)));
								}
							}
						});
					}

				} catch (ParseException e)
				{
					e.printStackTrace();
				}
				break;

			case CUSTOM:
				try
				{
					JSONObject jsonObj = new JSONObject((JSONObject) new JSONParser().parse(message));

					String reason = (String) jsonObj.get("reason"), victim = (String) jsonObj.get("victim");
					String reporterName = (String) jsonObj.get("reporter"); UUID reporterUUID = UUID.fromString((String) jsonObj.get("reporterUUID"));

					Bukkit.getOnlinePlayers().forEach(player -> {
						if(player.getUniqueId().toString().equals(victim) || player.getName().equals(victim))
						{
							request(
									new JsonBuilder("type", PunishType.REPORT.toString())
										.append("reporter", reporterName)
										.append("reporterUUID", reporterUUID.toString())
										.append("server", plugin.getServer().getName())
										.append("reason", reason)
										.append("victim", victim),
									MessageChannel.REPORT
							);
						}
					});

				} catch (ParseException e)
				{
					e.printStackTrace();
				}
				break;

			case REPORT:
				try
				{
					JSONObject jsonObj = new JSONObject((JSONObject) new JSONParser().parse(message));

					String reason = (String) jsonObj.get("reason"), server = (String) jsonObj.get("server"), victim = (String) jsonObj.get("victim");
					String reporterName = (String) jsonObj.get("reporter"); UUID reporterUUID = UUID.fromString((String) jsonObj.get("reporterUUID"));

					Bukkit.getOnlinePlayers().forEach(staff -> {
						if(staff.hasPermission(PunishPermissions.REPORT_VIEW.getPermission())) {
							staff.sendMessage("");
							staff.sendMessage(Colour.RedB + "Report Mail");
							staff.sendMessage(Colour.Gold + "From " + Colour.Yellow + reporterName);
							staff.sendMessage(Colour.Gold + "To " + Colour.Yellow + victim + Colour.White + " (" + Colour.Gray + server + Colour.White + ")");
							staff.sendMessage(Colour.Red + "Reason: " + Colour.White + reason);
						}
					});

				} catch (ParseException e)
				{
					e.printStackTrace();
				}
				break;

			case UN_PUNISH:
				try
				{
					JSONObject jsonObj = new JSONObject((JSONObject) new JSONParser().parse(message));

					final PunishType punishType = (jsonObj.containsKey("type") ? PunishType.valueOf((String) jsonObj.get("type")) : null);
					String key = (String) jsonObj.get("key");

					if(punishManager.isIP(key))
					{
						Bukkit.getOnlinePlayers().forEach(player -> {
							String address = player.getAddress().getAddress().getHostAddress();
							String[] addressSplit = address.split(Pattern.quote(".")), keySplit = key.split(Pattern.quote("."));

							boolean isVictim = false;
							if(keySplit[3].charAt(0) == '*')
							{
								if(keySplit[0].equals(addressSplit[0]) && keySplit[1].equals(addressSplit[1]) && keySplit[2].equals(addressSplit[2]))
									isVictim = true;
							}
							else
							{
								if(keySplit[0].equals(addressSplit[0]) && keySplit[1].equals(addressSplit[1]) && keySplit[2].equals(addressSplit[2]) && keySplit[3].equals(addressSplit[3]))
									isVictim = true;
							}

							if(isVictim)
							{
								if(punishType == null)
								{
									punishManager.cache.get(player.getUniqueId()).removeAll();
									player.sendMessage(Colour.GreenB + "Your punishments have been cleared.");
								}

								if(punishType == PunishType.MUTE || punishType == PunishType.MUTE_PERM) {
									for(MuteCache cache : punishManager.cache.get(player.getUniqueId()).getMuteCacheList()) {
										if(punishType == PunishType.MUTE_PERM) {
											punishManager.cache.get(player.getUniqueId()).removeMuteCacheByID(cache.getId());
											continue;
										}
										if(cache.getExpire().getTime() > System.currentTimeMillis()) {
											punishManager.cache.get(player.getUniqueId()).removeMuteCacheByID(cache.getId());
										}
									}
								}
								if(punishType == PunishType.REVOKE_REPORT) {
									for(UnknownCache cache : punishManager.cache.get(player.getUniqueId()).getUnknownCacheList()) {
										if(cache.getExpire().getTime() > System.currentTimeMillis()) {
											punishManager.cache.get(player.getUniqueId()).removeUnknownCacheByID(cache.getId());
										}
									}
								}
							}
						});
					}
					//The victim must be a player, lets find that player!
					else
					{
						Bukkit.getOnlinePlayers().forEach(player -> {
							if(player.getName().equals(key) || player.getUniqueId().toString().equals(key))
							{
								if(punishType == null)
								{
									punishManager.cache.get(player.getUniqueId()).removeAll();
									player.sendMessage(Colour.GreenB + "Your punishments have been cleared.");
									return;
								}

								if(punishType == PunishType.MUTE || punishType == PunishType.MUTE_PERM) {
									for(MuteCache cache : punishManager.cache.get(player.getUniqueId()).getMuteCacheList()) {
										if(punishType == PunishType.MUTE_PERM) {
											punishManager.cache.get(player.getUniqueId()).removeMuteCacheByID(cache.getId());
											continue;
										}
										if(cache.getExpire().getTime() > System.currentTimeMillis()) {
											punishManager.cache.get(player.getUniqueId()).removeMuteCacheByID(cache.getId());
										}
									}
								}
								if(punishType == PunishType.REVOKE_REPORT) {
									for(UnknownCache cache : punishManager.cache.get(player.getUniqueId()).getUnknownCacheList()) {
										if(cache.getExpire().getTime() > System.currentTimeMillis()) {
											punishManager.cache.get(player.getUniqueId()).removeUnknownCacheByID(cache.getId());
										}
									}
								}
							}
						});
					}

				} catch (ParseException e)
				{
					e.printStackTrace();
				}
				break;
		}
	}
}
