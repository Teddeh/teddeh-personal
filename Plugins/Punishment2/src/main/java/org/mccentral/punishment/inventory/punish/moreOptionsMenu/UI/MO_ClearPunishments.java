package org.mccentral.punishment.inventory.punish.moreOptionsMenu.UI;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;
import org.mccentral.punishment.util.PunishPermissions;

/**
 * Created: 24/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class MO_ClearPunishments extends MenuItem
{
	private PunishManager punishManager;
	private PlayerCache playerCache;
	private String key, reason;

	public MO_ClearPunishments(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
	{
		super(15, new ItemStackBuilder(Material.ANVIL).setName(Colour.White + "Clear All Punishments").build());

		this.punishManager = punishManager;
		this.playerCache = playerCache;
		this.key = key;
		this.reason = reason;
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		if(!punishManager.hasPerm(player, PunishPermissions.PUNISH_CLEARALL)) return;

		punishManager.unPunish(player, key, null);
		player.closeInventory();
	}
}
