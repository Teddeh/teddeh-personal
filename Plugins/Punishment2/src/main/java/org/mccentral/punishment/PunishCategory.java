package org.mccentral.punishment;

import org.mccentral.punishment.severity.Severity;
import org.mccentral.punishment.severity.SubSeverity;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum PunishCategory
{
	CHAT_OFFENCE(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, (15*60)*1000, PunishType.MUTE),//15 mins
					new SubSeverity(2, (30*60)*1000, PunishType.MUTE),//30 mins
					new SubSeverity(3, (45*60)*1000, PunishType.MUTE),//45 mins
					new SubSeverity(4, (60*60)*1000, PunishType.MUTE),//60 mins
					new SubSeverity(5, (75*60)*1000, PunishType.MUTE)// 75 mins
			}),
			new Severity(2, new SubSeverity[] {
					new SubSeverity(1, (((2*24)*60)*60)*1000, PunishType.MUTE),//2 days
					new SubSeverity(2, (((4*24)*60)*60)*1000, PunishType.MUTE),//4 days
					new SubSeverity(3, (((5*24)*60)*60)*1000, PunishType.MUTE),//5 days
					new SubSeverity(4, (((7*24)*60)*60)*1000, PunishType.MUTE),//7 days
					new SubSeverity(5, (((14*24)*60)*60)*1000, PunishType.MUTE)//14 days
			}),
			new Severity(3, new SubSeverity[] {
					new SubSeverity(1, 0, PunishType.MUTE_PERM) //Perm
			})
	}),
	GAMEPLAY_OFFENCE(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, ((1*60)*60)*1000, PunishType.BAN),
					new SubSeverity(2, ((2*60)*60)*1000, PunishType.BAN),
					new SubSeverity(3, ((3*60)*60)*1000, PunishType.BAN),
					new SubSeverity(4, ((4*60)*60)*1000, PunishType.BAN),
					new SubSeverity(5, ((5*60)*60)*1000, PunishType.BAN)
			}),
			new Severity(2, new SubSeverity[] {
					new SubSeverity(1, ((6*60)*60)*1000, PunishType.BAN),
					new SubSeverity(2, ((12*60)*60)*1000, PunishType.BAN),
					new SubSeverity(3, ((24*60)*60)*1000, PunishType.BAN),
					new SubSeverity(4, ((48*60)*60)*1000, PunishType.BAN),
					new SubSeverity(5, ((96*60)*60)*1000, PunishType.BAN)
			}),
			new Severity(3, new SubSeverity[] {
					new SubSeverity(1, (((7*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(2, (((14*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(3, (((21*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(4, (((28*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(5, 0, PunishType.BAN_PERM)
			}),
	}),
	CLIENT_MODIFICATION(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, ((6*60)*60)*1000, PunishType.BAN),
					new SubSeverity(2, ((12*60)*60)*1000, PunishType.BAN),
					new SubSeverity(3, ((24*60)*60)*1000, PunishType.BAN),
					new SubSeverity(4, ((48*60)*60)*1000, PunishType.BAN),
					new SubSeverity(5, ((96*60)*60)*1000, PunishType.BAN)
			}),
			new Severity(2, new SubSeverity[] {
					new SubSeverity(1, (((2*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(2, (((4*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(3, (((6*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(4, (((8*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(5, (((10*24)*60)*60)*1000, PunishType.BAN)
			}),
			new Severity(3, new SubSeverity[] {
					new SubSeverity(1, (((7*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(2, (((14*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(3, (((21*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(4, (((28*24)*60)*60)*1000, PunishType.BAN),
					new SubSeverity(5, 0, PunishType.BAN_PERM)
			}),
	}),
	INAPPROPRIATE_SKIN(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, (30*60)*1000, PunishType.BAN)
			})
	}),
	TPA_SPAM(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, (30*60)*1000, PunishType.BAN)
			})
	}),
	JOIN_ANNOUNCEMENT(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, (30*60)*1000, PunishType.BAN)
			})
	}),
	CLEAR_CREATIVE_INVENTORY(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, 0, PunishType.UNKNOWN)
			})
	}),
	MASS_OR_FALSE_REPORTS(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, (((2*24)*60)*60)*1000, PunishType.UNKNOWN)
			})
	}),
	FAILED_STAFF_REVIEW(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, (((7*24)*60)*60)*1000, PunishType.UNKNOWN)
			})
	}),
	CLEAR_ALL_PUNISHMENTS(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, 0, PunishType.UNKNOWN)
			})
	}),
	BAN_PERM(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, 0, PunishType.BAN_PERM)
			})
	}),
	TEMP_BAN(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, 0, PunishType.BAN)
			})
	}),
	IPBAN(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, (((7*24)*60)*60)*1000, PunishType.BAN)
			})
	}),
	WARN(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, 0, PunishType.WARNING)
			})
	}),
	KICK(new Severity[] {
			new Severity(1, new SubSeverity[] {
					new SubSeverity(1, 0, PunishType.KICK)
			})
	});

	private Severity[] severities;

	PunishCategory(Severity[] severities)
	{
		this.severities = severities;
	}

	public Severity[] getSeverities()
	{
		return severities;
	}

	public Severity getSeverity(int index)
	{
		return severities[index-1];
	}

	public SubSeverity[] getSubSeverities(int index)
	{
		return severities[index-1].getSubSeverities();
	}

	public SubSeverity getSubSeverity(int index1, int index2)
	{
		return severities[index1-1].getSubSeverities()[index2-1];
	}
}
