package org.mccentral.punishment.inventory.punish.punishMenu.UI;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;
import org.mccentral.punishment.util.PunishPermissions;

/**
 * Created: 24/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PM_Warning extends MenuItem
{
	private PunishManager punishManager;
	private String key, reason;

	public PM_Warning(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
	{
		super(17, new ItemStackBuilder(Material.PAPER).setName(Colour.Red + "Warning").setLore("", Colour.Gray + "Total Warnings: " + Colour.Yellow + playerCache.getTimesPunished(-1, PunishCategory.WARN)).build());

		this.punishManager = punishManager;
		this.key = key;
		this.reason = reason;
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		if(!punishManager.hasPerm(player, PunishPermissions.WARN)) return;

		punishManager.punish(player, key, PunishCategory.WARN, reason, 1, null);
		player.closeInventory();
	}
}
