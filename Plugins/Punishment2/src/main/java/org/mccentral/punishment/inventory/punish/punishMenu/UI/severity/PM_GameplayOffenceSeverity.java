package org.mccentral.punishment.inventory.punish.punishMenu.UI.severity;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;
import org.mccentral.punishment.util.PunishPermissions;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PM_GameplayOffenceSeverity extends MenuItem
{
	private PunishManager punishManager;
	private int severity;
	private String key, reason;

	public PM_GameplayOffenceSeverity(PunishManager punishManager, PlayerCache playerCache, String key, String reason, int index, ItemStack itemStack, int severity)
	{
		super(index, itemStack);

		this.punishManager = punishManager;
		this.severity = severity;
		this.key = key;
		this.reason = reason;
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		PunishPermissions permission = PunishPermissions.valueOf("PUNISH_GAMEPLAY_SEV" + severity);
		if(!punishManager.hasPerm(player, permission)) return;

		if(punishManager.cache.containsKey(player.getUniqueId())) {
			if(!punishManager.cache.get(player.getUniqueId()).canIssueSevThree()) {
				player.sendMessage(Colour.Red + "You don't have permission to perform this action.");
				player.closeInventory();
				return;
			}
		}

		punishManager.punish(player, key, PunishCategory.GAMEPLAY_OFFENCE, reason, severity, null);
		player.closeInventory();
	}

	public static class PM_GameplayOffenceSeverity1 extends PM_GameplayOffenceSeverity
	{
		public PM_GameplayOffenceSeverity1(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
		{
			super(
					punishManager,
					playerCache,
					key,
					reason,
					22,
					new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 5)
							.setName(Colour.Green + "Severity " + Colour.GreenB + "1" + Colour.Reset)
							.setLore("", Colour.White + "1st Offence: 1 Hour", Colour.White + "2nd Offence: 2 Hours", Colour.White + "3rd Offence: 3 Hours", Colour.White + "4th Offence: 4 Hours", Colour.White + "5th+ Offence: 5 Hours", "", Colour.Gray + "Total Sev.1 punishments: " + Colour.Yellow + playerCache.getTimesPunished(1, PunishCategory.GAMEPLAY_OFFENCE))
							.build(),
					1
			);
		}
	}

	public static class PM_GameplayOffenceSeverity2 extends PM_GameplayOffenceSeverity
	{
		public PM_GameplayOffenceSeverity2(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
		{
			super(
					punishManager,
					playerCache,
					key,
					reason,
					31,
					new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 4)
							.setName(Colour.Yellow + "Severity " + Colour.YellowB + "2" + Colour.Reset)
							.setLore("", Colour.White + "1st Offence: 6 Hours", Colour.White + "2nd Offence: 12 Hours", Colour.White + "3rd Offence: 24 Hours", Colour.White + "4th Offence: 48 Hours", Colour.White + "5th+ Offence: 96 Hours", "", Colour.Gray + "Total Sev.2 punishments: " + Colour.Yellow + playerCache.getTimesPunished(2, PunishCategory.GAMEPLAY_OFFENCE))
							.build(),
					2
			);
		}
	}

	public static class PM_GameplayOffenceSeverity3 extends PM_GameplayOffenceSeverity
	{
		public PM_GameplayOffenceSeverity3(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
		{
			super(
					punishManager,
					playerCache,
					key,
					reason,
					40,
					new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 14)
							.setName(Colour.Red + "Severity " + Colour.RedB + "3" + Colour.Reset)
							.setLore("", Colour.White + "1st Offence: 7 Days", Colour.White + "2nd Offence: 14 Days", Colour.White + "3rd Offence: 21 Days", Colour.White + "4th Offence: 28 Days", Colour.White + "5th+ Offence: Permanent", "", Colour.Gray + "Total Sev.3 punishments: " + Colour.Yellow + playerCache.getTimesPunished(3, PunishCategory.GAMEPLAY_OFFENCE))
							.build(),
					3
			);
		}
	}
}
