package org.mccentral.punishment.cache.type;

import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.cache.PunishCache;

import java.sql.Timestamp;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class UnknownCache extends PunishCache
{
	private Timestamp expire;

	public UnknownCache(int id, PunishCategory punishCategory, String reason, String staff, Timestamp time, Timestamp expire)
	{
		super(id, punishCategory, reason, staff, time);

		this.expire = expire;
	}

	public Timestamp getExpire()
	{
		return expire;
	}

	public void setExpire(Timestamp expire)
	{
		this.expire = expire;
	}
}
