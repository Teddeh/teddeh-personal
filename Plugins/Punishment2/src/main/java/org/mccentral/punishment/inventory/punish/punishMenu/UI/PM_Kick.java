package org.mccentral.punishment.inventory.punish.punishMenu.UI;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;
import org.mccentral.punishment.util.PunishPermissions;

/**
 * Created: 24/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PM_Kick extends MenuItem
{
	private PunishManager punishManager;
	private String key, reason;

	public PM_Kick(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
	{
		super(26, new ItemStackBuilder(Material.LEATHER_BOOTS).setName(Colour.Red + "Kick").build());

		this.punishManager = punishManager;
		this.key = key;
		this.reason = reason;
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		if(!punishManager.hasPerm(player, PunishPermissions.KICK)) return;

		punishManager.punish(player, key, PunishCategory.KICK, reason, 1, null);
		player.closeInventory();
	}
}
