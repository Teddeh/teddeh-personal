package org.mccentral.punishment.inventory.punish.moreOptionsMenu.UI;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;
import org.mccentral.punishment.util.PunishPermissions;

/**
 * Created: 24/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class MO_StaffReview extends MenuItem
{
	private PunishManager punishManager;
	private PlayerCache playerCache;
	private String key, reason;

	public MO_StaffReview(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
	{
		super(16, new ItemStackBuilder(Material.ANVIL).setName(Colour.Red + "Failed Staff Review").build());

		this.punishManager = punishManager;
		this.playerCache = playerCache;
		this.key = key;
		this.reason = reason;
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		if(!punishManager.hasPerm(player, PunishPermissions.FAILED_STAFF_REVIEW)) return;

		punishManager.punish(player, key, PunishCategory.FAILED_STAFF_REVIEW, reason, 1, null);
		player.closeInventory();
	}
}
