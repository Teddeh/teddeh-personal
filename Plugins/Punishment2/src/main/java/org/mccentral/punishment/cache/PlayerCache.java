package org.mccentral.punishment.cache;

import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.cache.misc.SeverityCache;
import org.mccentral.punishment.cache.type.BanCache;
import org.mccentral.punishment.cache.type.MuteCache;
import org.mccentral.punishment.cache.type.UnknownCache;
import org.mccentral.punishment.cache.type.WarnCache;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PlayerCache
{
	private List<PunishCache> allCacheList;

	private List<BanCache> banCacheList;
	private List<MuteCache> muteCacheList;
	private List<WarnCache> warnCacheList;
	private List<UnknownCache> unknownCacheList;

	public PlayerCache(List<BanCache> banCacheList, List<MuteCache> muteCacheList, List<WarnCache> warnCacheList, List<UnknownCache> unknownCacheList)
	{
		this.banCacheList = banCacheList;
		this.muteCacheList = muteCacheList;
		this.warnCacheList = warnCacheList;
		this.unknownCacheList = unknownCacheList;

		allCacheList = new ArrayList<>();
		allCacheList.addAll(banCacheList);
		allCacheList.addAll(muteCacheList);
		allCacheList.addAll(warnCacheList);
		allCacheList.addAll(unknownCacheList);
	}

	public List<BanCache> getBanCacheList()
	{
		return banCacheList;
	}

	public List<MuteCache> getMuteCacheList()
	{
		return muteCacheList;
	}

	public List<WarnCache> getWarnCacheList()
	{
		return warnCacheList;
	}

	public List<UnknownCache> getUnknownCacheList()
	{
		return unknownCacheList;
	}

	public void addBanCache(BanCache banCache)
	{
		banCacheList.add(banCache);
	}

	public void addMuteCache(MuteCache muteCache)
	{
		muteCacheList.add(muteCache);
	}

	public void addWarnCache(WarnCache warnCache)
	{
		warnCacheList.add(warnCache);
	}

	public void addUnknownCache(UnknownCache unknownCache)
	{
		unknownCacheList.add(unknownCache);
	}

	public void removeAll()
	{
		banCacheList.clear();
		muteCacheList.clear();
		warnCacheList.clear();
		unknownCacheList.clear();
		allCacheList.clear();
	}

	public void removeBanCacheByID(int id)
	{
		for(int i = 0; i < banCacheList.size(); i++)
		{
			if(banCacheList.get(i).getId() != id) continue;
			banCacheList.get(i).setExpire(new Timestamp(System.currentTimeMillis()));
			break;
		}
	}

	public void removeMuteCacheByID(int id)
	{
		for(int i = 0; i < muteCacheList.size(); i++)
		{
			if(muteCacheList.get(i).getId() != id) continue;
			muteCacheList.get(i).setExpire(new Timestamp(System.currentTimeMillis()));
			break;
		}
	}

	public void removeWarnCacheByID(int id)
	{
		for(UnknownCache cache : unknownCacheList)
		{
			if(cache.getId() != id) continue;
			unknownCacheList.remove(cache);
			break;
		}
	}

	public void removeUnknownCacheByID(int id)
	{
		for(int i = 0; i < unknownCacheList.size(); i++)
		{
			if(unknownCacheList.get(i).getId() != id) continue;
			unknownCacheList.get(i).setExpire(new Timestamp(System.currentTimeMillis()));
			break;
		}
	}

	public int getTimesPunished(int severity, PunishCategory punishCategory)
	{
		int i = 0;
		for(PunishCache cache : this.allCacheList)
		{
			if(cache.getPunishCategory() != punishCategory) continue;
			if(severity < 0) {i++; continue;}
			else
			{
				if(cache instanceof SeverityCache)
				{
					if(((SeverityCache) cache).getSeverity() == severity) i++;
				}
			}
		}

		return i;
	}

	public LinkedList<PunishCache> getPunishmentsInOrder()
	{
		LinkedList<PunishCache> punishCaches = new LinkedList<>();
		List<Integer> blacklist = new ArrayList<>();

		for(int i = 0; i < allCacheList.size(); i++)
		{
			PunishCache cache = getLowest(blacklist);
			if(cache == null) return punishCaches;
			blacklist.add(cache.getId());
			punishCaches.add(cache);
		}

		return punishCaches;
	}

	private PunishCache getLowest(List<Integer> blacklist)
	{
		PunishCache punishCache = null;
		int i = 0;
		for(PunishCache cache : allCacheList)
		{
			if(cache.getId() == 0) continue;

			boolean isBlacklist = false;
			for(Integer x : blacklist)
			{
				if(cache.getId() != x) continue;
				isBlacklist = true;
				break;
			}

			if(isBlacklist) continue;

			if(i == 0) {
				i = cache.getId();
				punishCache = cache;
				continue;
			}

			if(i < cache.getId()) continue;
			i = cache.getId();
			punishCache = cache;
		}

		return punishCache;
	}

	public boolean canIssueSevThree()
	{
		boolean allowed = true;
		for(UnknownCache cache : unknownCacheList)
		{
			if(cache.getPunishCategory() != PunishCategory.FAILED_STAFF_REVIEW) continue;
			if(cache.getExpire().getTime() > System.currentTimeMillis()) allowed = false;
		}

		return allowed;
	}
}
