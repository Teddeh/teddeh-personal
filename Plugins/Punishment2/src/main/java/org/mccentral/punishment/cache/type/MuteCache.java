package org.mccentral.punishment.cache.type;

import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.cache.PunishCache;
import org.mccentral.punishment.cache.misc.SeverityCache;

import java.sql.Timestamp;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class MuteCache extends PunishCache implements SeverityCache
{
	private Timestamp expire;
	private int severity, subSeverity;

	public MuteCache(int id, PunishCategory punishCategory, String reason, String staff, Timestamp time, Timestamp expire, int severity, int subSeverity)
	{
		super(id, punishCategory, reason, staff, time);

		this.expire = expire;
		this.severity = severity;
		this.subSeverity = subSeverity;
	}

	public Timestamp getExpire()
	{
		return expire;
	}

	@Override
	public int getSeverity()
	{
		return severity;
	}

	@Override
	public int getSubSeverity()
	{
		return subSeverity;
	}

	public void setExpire(Timestamp expire)
	{
		this.expire = expire;
	}
}
