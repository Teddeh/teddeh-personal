package org.mccentral.punishment;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum PunishType
{
	BAN,
	BAN_PERM,

	MUTE,
	MUTE_PERM,

	REVOKE_REPORT,

	KICK,
	REPORT,
	WARNING,

	UNKNOWN;
}
