package org.mccentral.punishment.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.PunishType;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.cache.type.BanCache;
import org.mccentral.punishment.cache.type.MuteCache;
import org.mccentral.punishment.cache.type.UnknownCache;
import org.mccentral.punishment.cache.type.WarnCache;
import org.mccentral.punishment.database.SafePrepareStatement;
import org.mccentral.punishment.inventory.punish.punishMenu.PunishMenu;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.Callback;
import org.mccentral.punishment.util.PunishPermissions;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PunishCommand implements CommandExecutor
{
	private Punishment plugin;
	private PunishManager punishManager;

	public PunishCommand(Punishment plugin, PunishManager punishManager)
	{
		this.plugin = plugin;
		this.punishManager = punishManager;

		//Self register.
		plugin.getCommand("punish").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args)
	{
		Player sender = (Player) commandSender;

		if (!sender.hasPermission(PunishPermissions.PUNISH_MENU.getPermission()) && !sender.isOp())
		{
			sender.sendMessage(Colour.Red + "You don't have permission to perform this action.");
			return true;
		}

		if (args.length < 2)
		{
			sender.sendMessage(Colour.Red + "Incorrect arguments, /punish <player> <reason>");
			return true;
		}

		StringBuilder reason = new StringBuilder();
		for (int i = 1; i < args.length; i++)
		{
			reason.append(args[i]);
			if (i < args.length - 1) reason.append(" ");
		}

		if(punishManager.isIP(args[0])) execute(sender, args[0], reason.toString());
		else {
			if(Bukkit.getOfflinePlayer(args[0]) == null)
			{
				sender.sendMessage(Colour.Red + "The user, " + args[0] + " cannot be recognised");
				return true;
			}

			execute(sender, Bukkit.getOfflinePlayer(args[0]).getUniqueId().toString(), reason.toString());
		}
		return true;
	}

	private void execute(Player sender, String key, String reason)
	{
		final Connection connection = plugin.getMySQL().getConnection();
		try
		{
			if (connection.isClosed())
			{
				sender.sendMessage(Colour.Yellow + "Punishment database is connecting..");
				plugin.getMySQL().openConnection(new Callback<Connection>()
				{
					@Override
					public void call(Connection value)
					{
						sender.sendMessage(Colour.Yellow + "Connected.");
						execute(sender, key, reason);
					}
				});
			}
		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		final List<BanCache> banCache = new ArrayList<BanCache>();
		final List<MuteCache> muteCache = new ArrayList<MuteCache>();
		final List<WarnCache> warnCache = new ArrayList<WarnCache>();
		final List<UnknownCache> unknownCache = new ArrayList<UnknownCache>();

		new SafePrepareStatement(plugin, "select * from " + plugin.getMySQL().getTable() + " where `KEY`=?").set(1, key).executeQuery(new Callback<ResultSet>() {
			@Override
			public void call(ResultSet result)
			{
				try
				{
					while (result.next())
					{
						PunishType punishType = PunishType.valueOf(result.getString("TYPE"));
						int id = result.getInt("ID");
						long expire = result.getLong("EXPIRE");
						byte severity = result.getByte("SEVERITY"), subSeverity = result.getByte("SUB_SEVERITY");
						Timestamp time = result.getTimestamp("TIME");
						String staff = result.getString("STAFF"), reason = result.getString("REASON");
						PunishCategory punishCategory = PunishCategory.valueOf(result.getString("CATEGORY"));

						if (punishType == PunishType.BAN || punishType == PunishType.BAN_PERM)
							banCache.add(new BanCache(id, punishCategory, reason, staff, time, new Timestamp(expire), severity, subSeverity));
						if (punishType == PunishType.MUTE || punishType == PunishType.MUTE_PERM)
							muteCache.add(new MuteCache(id, punishCategory, reason, staff, time, new Timestamp(expire), severity, subSeverity));
						if (punishType == PunishType.WARNING)
							warnCache.add(new WarnCache(id, punishCategory, reason, staff, time));
						if (punishType == PunishType.UNKNOWN)
							unknownCache.add(new UnknownCache(id, punishCategory, reason, staff, time, new Timestamp(expire)));
					}

					new PunishMenu(plugin, punishManager, new PlayerCache(banCache, muteCache, warnCache, unknownCache), key, reason).openInventory(sender);
				}catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}, true);
	}
}
