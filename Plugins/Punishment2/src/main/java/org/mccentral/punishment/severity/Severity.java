package org.mccentral.punishment.severity;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Severity
{
	private int severity;
	private SubSeverity[] subSeverities;

	public Severity(int severity, SubSeverity[] subSeverities)
	{
		this.severity = severity;
		this.subSeverities = subSeverities;
	}

	public int getSeverity()
	{
		return severity;
	}

	public SubSeverity[] getSubSeverities()
	{
		return subSeverities;
	}
}
