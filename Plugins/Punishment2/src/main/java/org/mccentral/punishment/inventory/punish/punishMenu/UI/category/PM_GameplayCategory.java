package org.mccentral.punishment.inventory.punish.punishMenu.UI.category;

import org.bukkit.Material;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PM_GameplayCategory extends MenuItem
{
	public PM_GameplayCategory(PlayerCache playerCache)
	{
		super(13, new ItemStackBuilder(Material.GOLD_SWORD).setName(Colour.WhiteB + "Gameplay Offences").setLore(Colour.GrayI + "Punishment Type: Ban", "", Colour.Yellow + "Total Gameplay punishments: " + Colour.Gold + playerCache.getTimesPunished(-1, PunishCategory.GAMEPLAY_OFFENCE)).build());
	}
}
