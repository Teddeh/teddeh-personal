package org.mccentral.punishment.util;

/**
 * Created: 18/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum PunishPermissions
{

	PUNISH_MENU("punishment.menu.open"),
	PUNISH_CLEARALL("punishment.clear"),
	PUNISH_CHATOFFENCE_SEV1("punishment.chatoffence.sev1"),
	PUNISH_CHATOFFENCE_SEV2("punishment.chatoffence.sev2"),
	PUNISH_CHATOFFENCE_SEV3("punishment.chatoffence.sev3"),
	PUNISH_GAMEPLAY_SEV1("punishment.gameplay.sev1"),
	PUNISH_GAMEPLAY_SEV2("punishment.gameplay.sev2"),
	PUNISH_GAMEPLAY_SEV3("punishment.gameplay.sev3"),
	PUNISH_CLIENTMODS_SEV1("punishment.clientmod.sev1"),
	PUNISH_CLIENTMODS_SEV2("punishment.clientmod.sev2"),
	PUNISH_CLIENTMODS_SEV3("punishment.clientmod.sev3"),

	WARN("punishment.warn.issue"),
	KICK("punishment.kick"),
	PERM_BAN("punishment.permban"),
	TEMP_BAN("punishment.tempban"),

	//REPORT
	REPORT_VIEW("punishment.report.view"),
	REPORT_ISSUE("punishment.report.issue"),

	//EXTRA
	OTHER_OPTIONS("punishment.extra.open"),
	INAPPROPRIATE_SKIN("punishment.extra.skin"),
	TPA_SPAM("punishment.extra.tpaspam"),
	JOIN_ANNOUNCEMENT("punishment.extra.joinann"),
	MASS_OR_FALSE_REPORTS("punishment.extra.reports"),
	CLEAR_INVENTORY("punishment.extra.clearinv"),
	FAILED_STAFF_REVIEW("punishment.extra.failedreview"),

	//UN PUNISH
	UN_BAN("punishment.unban"),
	UN_BANIP("punishment.unbanip"),
	UN_MUTE("punishment.unmute"),
	ALLOW_REPORT("punishment.reportaccess");

	private String permission;

	PunishPermissions(String permission)
	{
		this.permission = permission;
	}

	public String getPermission()
	{
		return permission;
	}
}
