package org.mccentral.punishment.inventory.punish.punishMenu.UI.severity;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.api.MenuItem;
import org.mccentral.punishment.util.Colour;
import org.mccentral.punishment.util.ItemStackBuilder;
import org.mccentral.punishment.util.PunishPermissions;

/**
 * Created: 23/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PM_ChatOffenceSeverity extends MenuItem
{
	private PunishManager punishManager;
	private int severity;
	private String key, reason;

	public PM_ChatOffenceSeverity(PunishManager punishManager, PlayerCache playerCache, String key, String reason, int index, ItemStack itemStack, int severity)
	{
		super(index, itemStack);

		this.punishManager = punishManager;
		this.severity = severity;
		this.key = key;
		this.reason = reason;
	}

	@Override
	public void click(Player player, ClickType clickType)
	{
		PunishPermissions permission = PunishPermissions.valueOf("PUNISH_CHATOFFENCE_SEV" + severity);
		if(!punishManager.hasPerm(player, permission)) return;

		punishManager.punish(player, key, PunishCategory.CHAT_OFFENCE, reason, severity, null);
		player.closeInventory();
	}

	public static class PM_ChatOffenceSeverity1 extends PM_ChatOffenceSeverity
	{
		public PM_ChatOffenceSeverity1(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
		{
			super(
					punishManager,
					playerCache,
					key,
					reason,
					20,
					new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 5)
							.setName(Colour.Green + "Severity " + Colour.GreenB + "1" + Colour.Reset)
							.setLore("", Colour.White + "1st Offence: 15 Minutes", Colour.White + "2nd Offence: 30 Minutes", Colour.White + "3rd Offence: 45 Minutes", Colour.White + "4th Offence: 60 Minutes", Colour.White + "5th+ Offence: 75 Minutes", "", Colour.Gray + "Total Sev.1 punishments: " + Colour.Yellow + playerCache.getTimesPunished(1, PunishCategory.CHAT_OFFENCE))
							.build(),
					1
			);
		}
	}

	public static class PM_ChatOffenceSeverity2 extends PM_ChatOffenceSeverity
	{
		public PM_ChatOffenceSeverity2(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
		{
			super(
					punishManager,
					playerCache,
					key,
					reason,
					29,
					new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 4)
							.setName(Colour.Yellow + "Severity " + Colour.YellowB + "2" + Colour.Reset)
							.setLore("", Colour.White + "1st Offence: 2 Days", Colour.White + "2nd Offence: 4 Days", Colour.White + "3rd Offence: 5 Days", Colour.White + "4th Offence: 7 Days", Colour.White + "5th+ Offence: 14 Days", "", Colour.Gray + "Total Sev.2 punishments: " + Colour.Yellow + playerCache.getTimesPunished(2, PunishCategory.CHAT_OFFENCE))
							.build(),
					2
			);
		}
	}

	public static class PM_ChatOffenceSeverity3 extends PM_ChatOffenceSeverity
	{
		public PM_ChatOffenceSeverity3(PunishManager punishManager, PlayerCache playerCache, String key, String reason)
		{
			super(
					punishManager,
					playerCache,
					key,
					reason,
					38,
					new ItemStackBuilder(Material.STAINED_GLASS_PANE, (byte) 14)
							.setName(Colour.Red + "Severity " + Colour.RedB + "3" + Colour.Reset)
							.setLore("", Colour.White + "1st Offence: Permanent")
							.build(),
					3
			);
		}
	}
}
