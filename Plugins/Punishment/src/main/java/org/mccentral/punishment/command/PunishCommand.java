package org.mccentral.punishment.command;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mccentral.punishment.PunishManager;
import org.mccentral.punishment.PunishPermissions;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.inventory.PunishMenu;
import org.mccentral.punishment.util.C;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PunishCommand implements CommandExecutor
{
	private Punishment plugin;
	private PunishManager punishManager;

	public PunishCommand(Punishment plugin, PunishManager punishManager)
	{
		this.plugin = plugin;
		this.punishManager = punishManager;

		plugin.getCommand("punish").setExecutor(this);
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args)
	{
		Player sender = (Player) commandSender;

		if(!sender.hasPermission(PunishPermissions.PUNISH_MENU.getPermission()) && !sender.isOp())
		{
			sender.sendMessage(C.Red + "You don't have permission to perform this action.");
			return true;
		}

		if (args.length < 2)
		{
			sender.sendMessage(C.Red + "Incorrect arguments, /punish <player> <reason>");
			return true;
		}

		StringBuilder reason = new StringBuilder();
		for (int i = 1; i < args.length; i++)
		{
			reason.append(args[i]).append(" ");
		}

		if(punishManager.isIP(args[0]))
		{
			new PunishMenu(plugin, null, punishManager, reason.toString(), sender, null, args[0], true).openInventory(sender);
			return true;
		}

		OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
		PlayerCache playerCache = null;
		if (offlinePlayer.isOnline()) {
			System.out.print("online");
			playerCache = punishManager.cache.get(offlinePlayer.getUniqueId());
		}

		new PunishMenu(plugin, Bukkit.getOfflinePlayer(args[0]), punishManager, reason.toString(), sender, playerCache, null, false).openInventory(sender);
		return true;
	}
}
