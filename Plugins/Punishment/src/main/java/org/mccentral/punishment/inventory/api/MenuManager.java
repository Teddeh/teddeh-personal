package org.mccentral.punishment.inventory.api;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.mccentral.punishment.Punishment;

/**
 * Created by Teddeh on 15/05/2016.
 */
public class MenuManager implements Listener
{
	public MenuManager(Punishment plugin)
	{
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event)
	{
		if (event.getInventory() == null) return;

		InventoryHolder holder = event.getInventory().getHolder();
		if (holder == null) return;

		if (!(holder instanceof CentralMenu)) return;

		event.setCancelled(true);
		if (!(event.getWhoClicked() instanceof Player)) return;

		Player player = (Player) event.getWhoClicked();
		CentralMenu menu = (CentralMenu) holder;
		ItemStack clicked = event.getCurrentItem();
		if (clicked == null)
		{
			if (menu.isCloseOnNullClick())
			{
				if (menu.getParent() != null)
				{
					if(menu.isResetCursor()) player.closeInventory();
					menu.getParent().openInventory(player);
					return;
				}

				player.closeInventory();
				return;
			}

			return;
		}

		if (clicked.getType() == Material.AIR) return;

		ClickType clickType = event.getClick();

		if (clickType == null) return;

		for (MenuItem menuItem : menu.getItems())
		{
			if (!menuItem.getItemStack().equals(clicked)) continue;

			menuItem.click(player, clickType);
			break;
		}
	}
}
