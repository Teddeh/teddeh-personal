package org.mccentral.punishment.lilypad;

/**
 * Created: 17/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum MessageChannel
{
	PUNISH("punish"),
	WARNING("punish_warning"),
	KICK("punish_kick"),
	REPORT_STAFF("punish_staff"),
	CLEAR("punish_clear"),
	OTHER("punish_other");

	private String channel;

	MessageChannel(String channel)
	{
		this.channel = channel;
	}

	public String getChannel()
	{
		return channel;
	}
}
