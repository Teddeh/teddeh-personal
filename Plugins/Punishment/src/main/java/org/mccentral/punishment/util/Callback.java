package org.mccentral.punishment.util;

/**
 * Created: 16/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class Callback<T>
{
	public abstract void call(T value);
}
