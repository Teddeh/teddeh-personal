package org.mccentral.punishment.cache.extra;

import org.mccentral.punishment.PunishCategory;

/**
 * Created: 17/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class PunishCache
{
	private int id;
	private PunishCategory category;

	public PunishCache(PunishCategory category, int id)
	{
		this.id = id;
		this.category = category;
	}

	public int getId()
	{
		return id;
	}

	public PunishCategory getCategory()
	{
		return category;
	}
}
