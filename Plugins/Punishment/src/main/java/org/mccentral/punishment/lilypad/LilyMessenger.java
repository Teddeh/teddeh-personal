package org.mccentral.punishment.lilypad;

import lilypad.client.connect.api.Connect;
import lilypad.client.connect.api.event.EventListener;
import lilypad.client.connect.api.event.MessageEvent;
import lilypad.client.connect.api.request.impl.MessageRequest;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.PunishPermissions;
import org.mccentral.punishment.Punishment;
import org.mccentral.punishment.cache.PlayerCache;
import org.mccentral.punishment.cache.extra.BanCache;
import org.mccentral.punishment.cache.extra.MuteCache;
import org.mccentral.punishment.cache.extra.OtherCache;
import org.mccentral.punishment.util.C;
import org.mccentral.punishment.util.json.JsonBuilder;

import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Created: 17/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class LilyMessenger
{
	private Punishment plugin;

	public LilyMessenger(Punishment plugin)
	{
		this.plugin = plugin;
	}

	public void request(JsonBuilder json, MessageChannel channel)
	{
		try
		{
			Connect c = plugin.getLilyConnect();
			if (!c.isConnected())
			{
				c.connect();
				request(json, channel);
				return;
			}

			MessageRequest request = new MessageRequest(Collections.<String>emptyList(), channel.getChannel(), json.toString()); //servername, channelname (short), message
			c.request(request);
		} catch (Throwable e)
		{
			e.printStackTrace();
		}
	}

	@EventListener
	public void onMessage(MessageEvent event)
	{
		boolean isChannel = false;
		MessageChannel channel = null;
		for (MessageChannel messageChannel : MessageChannel.values())
		{
			if (!event.getChannel().equals(messageChannel.getChannel())) continue;
			channel = messageChannel;
			isChannel = true;
			break;
		}

		if (!isChannel) return;

		String sender = event.getSender();
		String message = "null";
		try
		{
			message = event.getMessageAsString();
		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}

		if (channel == null) return;
		switch (channel)
		{
			case PUNISH:
				try
				{
					JSONObject jsonObj = new JSONObject((JSONObject) new JSONParser().parse(message));
					if (jsonObj == null) return;
					UUID victim = UUID.fromString(String.valueOf(jsonObj.get("victim")));
					boolean found = false;
					for (Player all : Bukkit.getOnlinePlayers())
					{
						if (!all.getUniqueId().equals(victim)) continue;
						found = true;
						break;
					}

					if (!found) return;
					Player player = Bukkit.getPlayer(victim);

					PunishCategory category = PunishCategory.valueOf((String) jsonObj.get("category"));
					if (category == PunishCategory.CHAT_OFFENCE)
					{

						int severity = Integer.valueOf((String) jsonObj.get("severity"));
						int subSeverity = Integer.valueOf((String) jsonObj.get("subSeverity"));
						int id = Integer.valueOf((String) jsonObj.get("id"));
						long time = Long.parseLong((String) jsonObj.get("time"));
						long expire = Long.parseLong((String) jsonObj.get("expire"));
						String staff = (String) jsonObj.get("staff");
						String reason = (String) jsonObj.get("reason");

						System.out.print(id);

						//Message
						player.sendMessage(" ");
						player.sendMessage(" ");
						player.sendMessage(" ");
						player.sendMessage(C.RedB + "You are Muted!");
						player.sendMessage(C.White + "Reason: " + C.Gray + reason);
						player.sendMessage(C.White + "Expires: " + plugin.getPunishManager().getCalculation(category, severity, subSeverity, expire));
						player.sendMessage(" ");
						player.sendMessage(C.Yellow + "Believe your punishment was a mistake?");
						player.sendMessage(C.Yellow + "Post an appeal at: " + C.Gold + "www.mccentral.org/appealpreview");

						if (!plugin.getPunishManager().cache.containsKey(victim))
						{
							try
							{
								Connection connection = plugin.getMySQL().getConnection();
								final String select = "select * from " + plugin.getMySQL().getTable() + " where UUID=?";
								PreparedStatement statement = connection.prepareStatement(select);
								statement.setString(1, victim.toString());
								ResultSet result = statement.executeQuery();

								List<BanCache> bans = new ArrayList<>();
								List<MuteCache> mutes = new ArrayList<>();
								List<OtherCache> other = new ArrayList<>();

								while (result.next())
								{
									PunishCategory c = PunishCategory.valueOf(result.getString("CATEGORY"));
									final int sev = result.getInt("SEVERITY"), subSev = result.getInt("SUB_SEVERITY"), id2 = result.getInt("ID");
									final Timestamp tme = result.getTimestamp("TIME"), exp = result.getTimestamp("EXPIRE_TIME");
									final String sta = result.getString("STAFF"), ip = result.getString("IP"), reas = result.getString("REASON");
									if (c == PunishCategory.GAMEPLAY_OFFENCE || c == PunishCategory.CLIENT_MODIFICATION)
									{
										bans.add(new BanCache(c, id2, sev, subSev, tme.getTime(), exp.getTime(), reas, sta));
									} else if (c == PunishCategory.CHAT_OFFENCE)
									{
										mutes.add(new MuteCache(c, id2, sev, subSev, tme.getTime(), exp.getTime(), reas, sta));
									} else
									{
										other.add(new OtherCache(c, id2, tme.getTime(), sta, reas));
									}
								}

								mutes.add(new MuteCache(category,
										id,
										severity,
										subSeverity,
										time,
										expire,
										staff,
										reason
								));
								plugin.getPunishManager().cache.put(victim, new PlayerCache(bans, mutes, other));
							} catch (SQLException e)
							{
								e.printStackTrace();
							}
							return;
						}

						PlayerCache playerCache = plugin.getPunishManager().cache.get(victim);
						List<MuteCache> muteCacheList = playerCache.getMuteCache();
						muteCacheList.add(new MuteCache(category,
								id,
								severity,
								subSeverity,
								time,
								expire,
								staff,
								reason
						));

						plugin.getPunishManager().cache.put(victim, new PlayerCache(playerCache.getBanCache(), muteCacheList, playerCache.getOtherCache()));
						return;
					}

					if(category == PunishCategory.FAILED_STAFF_REVIEW)
					{
						player.sendMessage(C.Red + "You have been restricted from using Sev.Three punishments.");
						return;
					}

					new BukkitRunnable()
					{
						public void run()
						{
							//Kicking the player Sync, Bukkit doesn't like Async
							Bukkit.getPlayer(victim).kickPlayer(C.YellowB + "Minecraft Central" + C.Reset + "\n" + C.RedB + "You have been Banned!" + C.Reset);
						}
					}.runTask(plugin);

				} catch (ParseException e)
				{
					e.printStackTrace();
				}
				break;

			case WARNING:
				try
				{
					JSONObject jsonObj = new JSONObject((JSONObject) new JSONParser().parse(message));
					if (jsonObj == null) return;

					UUID victim = UUID.fromString(String.valueOf(jsonObj.get("victim")));
					String reason = (String) jsonObj.get("reason");

					boolean found = false;
					for (Player all : Bukkit.getOnlinePlayers())
					{
						if (!all.getUniqueId().equals(victim)) continue;
						found = true;
						break;
					}

					if (!found) return;
					Player player = Bukkit.getPlayer(victim);

					PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + C.RedB + "! WARNING !" + "\"}"));
					PacketPlayOutTitle subtitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + C.Yellow + "You have been warned by: " + C.Gold + Bukkit.getOfflinePlayer(UUID.fromString(String.valueOf(jsonObj.get("reporter")))).getName() + "\"}"));

					player.sendMessage(" ");
					player.sendMessage(C.RedB + "! WARNING !");
					player.sendMessage(C.Yellow + reason);
					player.sendMessage(" ");

					((CraftPlayer) player).getHandle().playerConnection.sendPacket(title);
					((CraftPlayer) player).getHandle().playerConnection.sendPacket(subtitle);

				} catch (ParseException e)
				{
					e.printStackTrace();
				}

				break;

			case KICK:
				try
				{
					JSONObject jsonObj = new JSONObject((JSONObject) new JSONParser().parse(message));
					if (jsonObj == null) return;

					UUID victim = UUID.fromString(String.valueOf(jsonObj.get("victim")));
					String reason = (String) jsonObj.get("reason");

					boolean found = false;
					for (Player all : Bukkit.getOnlinePlayers())
					{
						if (!all.getUniqueId().equals(victim)) continue;
						found = true;
						break;
					}

					if (!found) return;
					Player player = Bukkit.getPlayer(victim);

					new BukkitRunnable() {
						public void run()
						{
							//Kicking the player Sync, Bukkit doesn't like Async
							player.kickPlayer(C.RedB + "Kicked" + C.Reset + "\n" + C.Yellow + reason);
						}
					}.runTask(plugin);

				} catch (ParseException e)
				{
					e.printStackTrace();
				}

				break;

			case REPORT_STAFF:

				try
				{
					JSONObject jsonObj = new JSONObject((JSONObject) new JSONParser().parse(message));
					if (jsonObj == null) return;

					UUID victim = UUID.fromString(String.valueOf(jsonObj.get("victim"))), reporter = UUID.fromString(String.valueOf(jsonObj.get("reporter")));
					String reason = (String) jsonObj.get("reason");

					for(Player staff : Bukkit.getOnlinePlayers())
					{
						if(!staff.hasPermission(PunishPermissions.REPORT_VIEW.getPermission()) || !staff.isOp()) continue;

						OfflinePlayer victim2 = Bukkit.getOfflinePlayer(victim), reporter2 = Bukkit.getOfflinePlayer(reporter);

						staff.sendMessage(C.Red + "Report: " + C.White + (reporter2 == null ? reporter.toString() : reporter2.getName()) + C.GoldB + " --> " + C.White + (victim2 == null ? victim.toString() : victim2.getName()));
						staff.sendMessage(C.Red + "Report: " + C.Gray + reason);
					}
				} catch (ParseException e)
				{
					e.printStackTrace();
				}

				break;

			case CLEAR:
				try
				{
					JSONObject jsonObj = new JSONObject((JSONObject) new JSONParser().parse(message));
					if (jsonObj == null) return;

					UUID victim = UUID.fromString(String.valueOf(jsonObj.get("victim"))), reporter = UUID.fromString(String.valueOf(jsonObj.get("staff")));

					boolean found = false;
					for (Player all : Bukkit.getOnlinePlayers())
					{
						if (!all.getUniqueId().equals(victim)) continue;
						found = true;
						break;
					}

					if (!found) return;
					Player player = Bukkit.getPlayer(victim);

					if(!plugin.getPunishManager().cache.containsKey(victim)) return;
					plugin.getPunishManager().cache.remove(victim);

				} catch (ParseException e)
				{
					e.printStackTrace();
				}
				break;

			case OTHER:
				try
				{
					JSONObject jsonObj = new JSONObject((JSONObject) new JSONParser().parse(message));
					if (jsonObj == null) return;

					UUID victim = UUID.fromString(String.valueOf(jsonObj.get("victim")));
					String staff = String.valueOf(jsonObj.get("staff"));
					PunishCategory category = PunishCategory.valueOf(String.valueOf(jsonObj.get("category")));

					boolean found = false;
					for (Player all : Bukkit.getOnlinePlayers())
					{
						if (!all.getUniqueId().equals(victim)) continue;
						found = true;
						break;
					}

					String type = "";
					if(category == PunishCategory.UN_BAN || category == PunishCategory.UN_BANIP)
						type = "ban";
					if(category == PunishCategory.UN_MUTE)
						type = "mute";


					if (!found) return;
					Player player = Bukkit.getPlayer(victim);
					player.kickPlayer(C.YellowB + "Minecraft Central" + C.Reset + "\n" +
							C.Green + "Your " + type + " punishment(s) have been removed!" + "\n\n" +
							C.Gray + "By: " + staff);
//					if(!plugin.getPunishManager().cache.containsKey(victim)) return;
//					List<BanCache> bans = new ArrayList<>();
//					List<MuteCache> mutes = new ArrayList<>();
//					List<OtherCache> other = new ArrayList<>();
//
//					if(category == PunishCategory.CHAT_OFFENCE)
//					{
//						m
//					}

				} catch (ParseException e)
				{
					e.printStackTrace();
				}
				break;
		}
	}
}
