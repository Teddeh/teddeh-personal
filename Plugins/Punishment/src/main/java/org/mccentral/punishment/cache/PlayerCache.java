package org.mccentral.punishment.cache;

import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.cache.extra.BanCache;
import org.mccentral.punishment.cache.extra.MuteCache;
import org.mccentral.punishment.cache.extra.OtherCache;
import org.mccentral.punishment.cache.extra.PunishCache;

import java.util.*;

/**
 * Created: 17/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class PlayerCache
{
	private List<BanCache> banCache;
	private List<MuteCache> muteCache;
	private List<OtherCache> otherCache;

	public PlayerCache(List<BanCache> banCache, List<MuteCache> muteCache, List<OtherCache> otherCache)
	{
		this.banCache = banCache;
		this.muteCache = muteCache;
		this.otherCache = otherCache;
	}

	public List<BanCache> getBanCache()
	{
		return banCache;
	}

	public List<MuteCache> getMuteCache()
	{
		return muteCache;
	}

	public List<OtherCache> getOtherCache()
	{
		return otherCache;
	}

	public int getTimesBanned(int severity, PunishCategory category)
	{
		if(banCache == null) return 0;
		if(severity == -1) return banCache.size();

		int i = 0;
		for(BanCache cache : banCache) {
			if(cache.getSeverity() != severity) continue;
			if(cache.getCategory() != category) continue;
			i++;
		}

		return i;
	}

	public int getTimesMuted(int severity)
	{
		if(muteCache == null) return 0;
		if(severity <= -1) return muteCache.size();

		int i = 0;
		for(MuteCache cache : muteCache) {
			if(cache.getSeverity() != severity) continue;
			i++;
		}

		return i;
	}

	public int getOtherOffences()
	{
		if(otherCache == null) return 0;
		return otherCache.size();
	}

	public LinkedList<PunishCache> getPunishmentsInOrder()
	{
		LinkedList<PunishCache> punishCaches = new LinkedList<>();
		List<Integer> blacklist = new ArrayList<>();

		for(int i = 0; i < (banCache.size() + muteCache.size() + otherCache.size()); i++)
		{
			PunishCache cache = getLowest(blacklist);
			if(cache == null) return punishCaches;
			blacklist.add(cache.getId());
			punishCaches.add(cache);
		}

		return punishCaches;
	}

	private PunishCache getLowest(List<Integer> blacklist)
	{
		List<PunishCache> punishCacheList = new ArrayList<>();
		for(BanCache cache : banCache) punishCacheList.add(cache);
		for(MuteCache cache : muteCache) punishCacheList.add(cache);
		for(OtherCache cache : otherCache) punishCacheList.add(cache);


		PunishCache punishCache = null;
		int i = 0;
		for(PunishCache cache : punishCacheList)
		{
			if(cache.getId() == 0) continue;

			boolean isBlacklist = false;
			for(Integer x : blacklist)
			{
				if(cache.getId() != x) continue;
				isBlacklist = true;
				break;
			}

			if(isBlacklist) continue;

			if(i == 0) {
				i = cache.getId();
				punishCache = cache;
				continue;
			}

			if(i < cache.getId()) continue;
			i = cache.getId();
			punishCache = cache;
		}

		punishCacheList.clear();
		return punishCache;
	}

	public boolean canIssueSevThree()
	{
		boolean allowed = true;
		for(BanCache cache : banCache)
		{
			if(cache.getCategory() != PunishCategory.FAILED_STAFF_REVIEW) continue;
			if(cache.getExpireTime() > System.currentTimeMillis()) allowed = false;
		}

		return allowed;
	}
}
