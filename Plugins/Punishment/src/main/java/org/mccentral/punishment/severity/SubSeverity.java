package org.mccentral.punishment.severity;

/**
 * Created: 15/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class SubSeverity
{
	private int offence;
	private long time;
	private Type type;

	public SubSeverity(int offence, long time, Type type)
	{
		this.offence = offence;
		this.time = time;
		this.type = type;
	}

	public int getOffence()
	{
		return offence;
	}

	public long getTime()
	{
		return time;
	}

	public Type getType()
	{
		return type;
	}

	public enum Type {
		PERM,
		NULL
	}
}
