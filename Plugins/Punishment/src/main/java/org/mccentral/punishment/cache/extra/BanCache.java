package org.mccentral.punishment.cache.extra;

import org.mccentral.punishment.PunishCategory;
import org.mccentral.punishment.util.C;

/**
 * Created: 17/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class BanCache extends PunishCache
{
	private int severity, subSeverity;
	private long timeBanned, expireTime;
	private String reason, staffUUID;

	public BanCache(PunishCategory category, int id, int severity, int subSeverity, long timeBanned, long expireTime, String reason, String staffUUID)
	{
		super(category, id);

		this.severity = severity;
		this.subSeverity = subSeverity;
		this.timeBanned = timeBanned;
		this.expireTime = expireTime;
		this.reason = reason;
		this.staffUUID = staffUUID;
	}

	public int getSeverity()
	{
		return severity;
	}

	public String getSeverityString()
	{
		if(severity == 1) return C.GreenB + severity;
		if(severity == 2) return C.YellowB + severity;
		if(severity == 3) return C.RedB + severity;
		return ""+severity;
	}

	public int getSubSeverity()
	{
		return subSeverity;
	}

	public long getTimeBanned()
	{
		return timeBanned;
	}

	public long getExpireTime()
	{
		return expireTime;
	}

	public String getReason()
	{
		return reason;
	}

	public String getStaffUUID()
	{
		return staffUUID;
	}
}
