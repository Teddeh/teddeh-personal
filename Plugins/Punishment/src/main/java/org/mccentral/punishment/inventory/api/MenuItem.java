package org.mccentral.punishment.inventory.api;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Teddeh on 15/05/2016.
 */
public class MenuItem
{
	private final ItemStack itemStack;
	private final int index;

	public MenuItem(int index, ItemStack itemStack)
	{
		this.itemStack = itemStack;
		this.index = index;
	}

	public ItemStack getItemStack()
	{
		return itemStack;
	}

	public int getIndex()
	{
		return index;
	}

	public void click(Player player, ClickType clickType) {}
}
