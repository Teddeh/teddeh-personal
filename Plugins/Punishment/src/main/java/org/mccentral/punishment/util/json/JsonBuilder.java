package org.mccentral.punishment.util.json;

/**
 * Created: 17/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class JsonBuilder
{
	private StringBuilder strBuilder;

	public JsonBuilder(String key, String value)
	{
		strBuilder = new StringBuilder();
		strBuilder.append("{\"" + key + "\":\"" + value + "\"");
	}

	public JsonBuilder append(String key, String value)
	{
		strBuilder.append(",\"" + key + "\":\"" + value + "\"");
		return this;
	}

	public String toString()
	{
		strBuilder.append("}");
		return strBuilder.toString();
	}
}
