package org.mccentral.companions.companion.part;

import org.mccentral.companions.companion.Companion;
import org.mccentral.companions.companion.part.armorstand.CompanionEntityPart;
import org.mccentral.companions.companion.part.armorstand.CompanionEquipment;
import org.mccentral.companions.companion.part.armorstand.CompanionOptions;

/**
 * Created: 29/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CompanionModuleAnimated extends CompanionPart
{
	public CompanionModuleAnimated(Companion parent, String name, int id, double yMod, float offset, float radius, float directionOffset, CompanionEquipment equipment, CompanionOptions options)
	{
		super(parent, name, id, yMod, offset, radius, directionOffset, equipment, options);
	}

	public void animate(CompanionEntityPart armorStand) {}
	public void moveAnimate(CompanionEntityPart armorStand) {}
	public void stillAnimate(CompanionEntityPart armorStand) {}
}
