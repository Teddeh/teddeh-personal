package org.mccentral.companions.util;

import java.time.Duration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Teddeh on 11/04/2016.
 */
public class TimeUtil
{

	public static String durationToString(Duration d)
	{
		String s = "";
		boolean started = false;
		if(d.toDays() >= 365)
		{
			started = true;
			s += d.toDays() / 365 + " year" + ((d.toDays() / 30 != 1) ? "s" : "") + ", ";
			d = d.minus(Duration.ofDays(d.toDays() / 365 * 365));
		}

		if (started || d.toDays() >= 31)
		{
			started = true;
			s += d.toDays() / 30 + " month" + ((d.toDays() / 30 != 1) ? "s" : "") + ", ";
			d = d.minus(Duration.ofDays(d.toDays() / 30 * 30));
		}

		if(started || d.toDays() >= 7)
		{
			started = true;
			s += d.toDays() / 7 + " week" + ((d.toDays() / 7 != 1) ? "s" : "") + ", ";
			d = d.minus(Duration.ofDays(d.toDays() / 7 * 7));
		}

		if(started || d.toDays() > 0)
		{
			started = true;
			s += d.toDays() + " day" + ((d.toDays() != 1) ? "s" : "") + ", ";
			d = d.minus(Duration.ofDays(d.toDays()));
		}

		if(started || d.toHours() > 0)
		{
			started = true;
			s += d.toHours() + " hour" + ((d.toHours() != 1) ? "s" : "") + ", ";
			d = d.minus(Duration.ofHours(d.toHours()));
		}

		if(started || d.toMinutes() > 0)
		{
			started = true;
			s += d.toMinutes() + " minute" + ((d.toMinutes() != 1) ? "s" : "") + ", ";
			d = d.minus(Duration.ofMinutes(d.toMinutes()));
		}

		if(started || d.getSeconds() > 0)
		{
			s += d.getSeconds() + " second" + ((d.getSeconds() != 1) ? "s" : "") + ", ";
		}
		return s;
	}

	public static Duration StringToDuration(String duration)
	{
		Pattern pattern = Pattern.compile("[1-9][0-9]*(y|mo|w|d|h|m|s)");
		Matcher matcher = pattern.matcher(duration);
		int years = 0, months = 0, weeks = 0, days = 0, hours = 0, minutes = 0, seconds = 0;
		while(matcher.find())
		{
			String r = matcher.group();
			switch (r.charAt(r.length()-1))
			{
				case 'y': years = Integer.parseInt(r.replace("y", "")); break;
				case 'o': months = Integer.parseInt(r.replace("mo", "")); break;
				case 'w': weeks = Integer.parseInt(r.replace("w", "")); break;
				case 'd': days = Integer.parseInt(r.replace("d", "")); break;
				case 'h': hours = Integer.parseInt(r.replace("h", "")); break;
				case 'm': minutes = Integer.parseInt(r.replace("m", "")); break;
				case 's': seconds = Integer.parseInt(r.replace("s", "")); break;
			}
		}

		return Duration.ofDays(years * 365 + months * 30 + weeks * 7 + days).plusHours(hours).plusMinutes(minutes).plusSeconds(seconds);
	}

	public static boolean elapsed(long from, long to)
	{
		return System.currentTimeMillis() - from > to;
	}
}
