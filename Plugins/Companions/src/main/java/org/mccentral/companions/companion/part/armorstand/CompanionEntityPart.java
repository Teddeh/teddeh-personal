package org.mccentral.companions.companion.part.armorstand;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.util.EulerAngle;
import org.mccentral.companions.CompanionPlugin;
import org.mccentral.companions.companion.CompanionManager;
import org.mccentral.companions.companion.part.CompanionModuleAnimated;
import org.mccentral.companions.companion.part.CompanionPart;
import org.mccentral.companions.util.PlayerUtil;

/**
 * Created: 01/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CompanionEntityPart extends EntityArmorStand
{
	public CompanionPart companionPart;

	public CompanionEntityPart(World world, CompanionPart companionPart)
	{
		super(world);

		this.companionPart = companionPart;
	}

	@Override
	public void m()
	{
        if(companionPart.getParent() != null && companionPart.getParent().getHolder() != null && companionPart.getParent().getPlayer() != null&& !companionPart.getParent().getPlayer().isOnline())
        {
            CompanionPlugin.getCompanionManager().removePlayerCompanion(companionPart.getParent().getPlayer());
            return;
        }
		if(companionPart.getParent() == null) return;

		Location loc = companionPart.update();
		setPosition(loc.getX(), loc.getY(), loc.getZ());
		yaw = loc.getYaw();
		pitch = loc.getPitch();

		if(companionPart instanceof CompanionModuleAnimated)
		{
			((CompanionModuleAnimated) companionPart).animate(this);
			if (companionPart.getParent().isMoving()) ((CompanionModuleAnimated) companionPart).moveAnimate(this);
			else ((CompanionModuleAnimated) companionPart).stillAnimate(this);
		}

		PacketPlayOutEntityTeleport teleport = new PacketPlayOutEntityTeleport(this);
		Bukkit.getOnlinePlayers().forEach(player -> PlayerUtil.sendPacket(player, teleport));
//		PlayerUtil.sendPacket(companionPart.getParent().getPlayer(), teleport); Had to remove for statue support
//		super.m();
	}

	public static CompanionEntityPart spawn(CompanionPart companionPart)
	{
		Location loc = companionPart.update();
		//System.out.println(loc.getX() + " , " + loc.getY() + " , " + loc.getZ());
		World w = ((CraftWorld) loc.getWorld()).getHandle();
		CompanionEntityPart armorStand = new CompanionEntityPart(w, companionPart);

		//add entity to the world
		armorStand.setLocation(loc.getX(), loc.getY()+companionPart.getParent().getYPlotOffset(), loc.getZ(), loc.getYaw(), loc.getPitch());
		w.addEntity(armorStand, CreatureSpawnEvent.SpawnReason.CUSTOM);

		//send packet
		PacketPlayOutSpawnEntity spawn = new PacketPlayOutSpawnEntity(armorStand, 30);
		PacketPlayOutEntityTeleport tp = new PacketPlayOutEntityTeleport(armorStand);
		Bukkit.getOnlinePlayers().forEach(all -> PlayerUtil.sendPacket(all, spawn, tp));
		return armorStand;
	}

//	public static void register()
//	{
//		try
//		{
//			Class<EntityTypes> entityTypeClass = EntityTypes.class;
//
//			Field c = entityTypeClass.getDeclaredField("c");
//			c.setAccessible(true);
//			HashMap<String, Class<?>> c_map = (HashMap) c.get(null);
//			c_map.put("CompanionEntityPart", CompanionEntityPart.class);
//
//			Field d = entityTypeClass.getDeclaredField("d");
//			d.setAccessible(true);
//			HashMap<Class<?>, String> d_map = (HashMap) d.get(null);
//			d_map.put(CompanionEntityPart.class, "CompanionEntityPart");
//
//			Field e = entityTypeClass.getDeclaredField("e");
//			e.setAccessible(true);
//			HashMap<Integer, Class<?>> e_map = (HashMap) e.get(null);
//			e_map.put(Integer.valueOf(300), CompanionEntityPart.class);
//
//			Field f = entityTypeClass.getDeclaredField("f");
//			f.setAccessible(true);
//			HashMap<Class<?>, Integer> f_map = (HashMap) f.get(null);
//			f_map.put(CompanionEntityPart.class, Integer.valueOf(300));
//
//			Field g = entityTypeClass.getDeclaredField("g");
//			g.setAccessible(true);
//			HashMap<String, Integer> g_map = (HashMap) g.get(null);
//			g_map.put("CompanionEntityPart", Integer.valueOf(300));
//
//		} catch (Exception exc)
//		{
//			Field d;
//			int d_map;
//			Method[] e;
//			Class[] paramTypes = {Class.class, String.class, Integer.TYPE};
//			try
//			{
//				Method method = EntityTypes.class.getDeclaredMethod("addMapping", paramTypes);
//				method.setAccessible(true);
//			} catch (Exception ex)
//			{
//				exc.addSuppressed(ex);
//				try
//				{
//					d_map = (e = EntityTypes.class.getDeclaredMethods()).length;
//					for (int d1 = 0; d1 < d_map; d1++)
//					{
//						Method method = e[d1];
//						if (Arrays.equals(paramTypes, method.getParameterTypes()))
//						{
//							method.invoke(null, new Object[]{CompanionEntityPart.class, "CompanionEntityPart", Integer.valueOf(300)});
//						}
//					}
//				} catch (Exception exe)
//				{
//					exc.addSuppressed(exe);
//				}
//				exc.printStackTrace();
//			}
//		}
//	}

	public EulerAngle fromVector3f(Vector3f old) {
		return new EulerAngle(Math.toRadians((double)old.getX()), Math.toRadians((double)old.getY()), Math.toRadians((double)old.getZ()));
	}

	public Vector3f fromEulerAngle(EulerAngle old) {
		return new Vector3f((float)Math.toDegrees(old.getX()), (float)Math.toDegrees(old.getY()), (float)Math.toDegrees(old.getZ()));
	}
}