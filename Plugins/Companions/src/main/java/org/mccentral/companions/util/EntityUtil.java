package org.mccentral.companions.util;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;

import java.lang.reflect.Field;

/**
 * Created: 07/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class EntityUtil
{
	public static void silence(Entity entity, boolean silence)
	{
		((CraftEntity)entity).getHandle().b(true);
	}

	public static void setNoAI(Entity entity)
	{
		net.minecraft.server.v1_8_R3.Entity nmsEntity = ((CraftEntity) entity).getHandle();
		NBTTagCompound tag = nmsEntity.getNBTTag();
		if (tag == null) {
			tag = new NBTTagCompound();
		}
		nmsEntity.c(tag);
		tag.setInt("NoAI", 1);
		nmsEntity.f(tag);
	}

	public static void lookAtPlayerAI(Entity entity, float range)
	{
		if (((CraftEntity) entity).getHandle() instanceof EntityInsentient)
		{
			EntityInsentient ent = (EntityInsentient) ((CraftEntity) entity).getHandle();
			PathfinderGoalLookAtPlayer goal1 = new PathfinderGoalLookAtPlayer(ent, EntityHuman.class, range);
			PathfinderGoalRandomLookaround goal2 = new PathfinderGoalRandomLookaround(ent);
			Field goalSelector = null;
			try
			{
				goalSelector = EntityInsentient.class.getDeclaredField("goalSelector");
			} catch (NoSuchFieldException e)
			{
				e.printStackTrace();
			}

			if(goalSelector == null) return;
			goalSelector.setAccessible(true);

			try
			{
				((PathfinderGoalSelector) goalSelector.get(ent)).a(7, goal1);
				((PathfinderGoalSelector) goalSelector.get(ent)).a(8, goal2);
			}
			catch (IllegalAccessException e)
			{
				e.printStackTrace();
			}
		}
	}
}
