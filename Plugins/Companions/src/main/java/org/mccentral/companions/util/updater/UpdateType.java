package org.mccentral.companions.util.updater;

/**
 * Created: 27/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum UpdateType
{
	MIN_10(600000),
	MIN_9(540000),
	MIN_8(480000),
	MIN_7(420000),
	MIN_6(360000),
	MIN_5(300000),
	MIN_4(240000),
	MIN_3(180000),
	MIN_2(120000),
	SEC_60(60000),
	SEC_50(50000),
	SEC_40(40000),
	SEC_30(30000),
	SEC_20(20000),
	SEC_10(10000),
	SEC_5(5000),
	SEC_4(4000),
	SEC_3(3000),
	SEC_2(2000),
	SEC_1(1000),
	TICK_15(750),
	TICK_10(500),
	TICK_5(250),
	TICK_4(200),
	TICK_3(150),
	TICK_2(100),
	TICK_1(50);

	private long time, timeLeft;

	UpdateType(long time)
	{
		this.time = time;
		this.timeLeft = System.currentTimeMillis();
	}

	public long getMilliseconds()
	{
		return time;
	}

	public long getTimeLeft()
	{
		return timeLeft;
	}

	public void setTimeLeft(long timeLeft)
	{
		this.timeLeft = timeLeft;
	}
}
