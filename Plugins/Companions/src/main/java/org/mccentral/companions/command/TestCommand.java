package org.mccentral.companions.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mccentral.companions.companion.Companion;
import org.mccentral.companions.companion.CompanionManager;
import org.mccentral.companions.type.BB8;
import org.mccentral.companions.type.CompanionType;
import org.mccentral.companions.type.Duck;
import org.mccentral.companions.type.Gorilla;

import java.util.HashSet;

/**
 * Created: 02/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class TestCommand implements CommandExecutor
{
	private CompanionManager companionManager;

	public TestCommand(CompanionManager companionManager)
	{
		this.companionManager = companionManager;
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args)
	{
		if(!(commandSender instanceof Player))
		{
			System.out.println("Player command only!");
			return true;
		}

		Player sender = (Player) commandSender;
		if(!sender.isOp())
			return true;
		if(args.length < 1)
		{
			sender.sendMessage("Incorrect args, /test <companion>");
			return true;
		}

		/** >! THIS IS JUST FOR TESTING !< */
		if(args[0].equalsIgnoreCase("BB8")) companionManager.spawnCompanion(sender, CompanionType.MC_8);
		else if(args[0].equalsIgnoreCase("Duck")) companionManager.spawnCompanion(sender, CompanionType.DUCK);
		else if(args[0].equalsIgnoreCase("Gorilla")) companionManager.spawnCompanion(sender, CompanionType.GORILLA);
		else if(args[0].equalsIgnoreCase("Dog")) companionManager.spawnCompanion(sender, CompanionType.PUG);
		else if(args[0].equalsIgnoreCase("Chimp")) companionManager.spawnCompanion(sender, CompanionType.CHIMP);
		else if(args[0].equalsIgnoreCase("Turtle")) companionManager.spawnCompanion(sender, CompanionType.TURTLE);
		else if(args[0].equalsIgnoreCase("Minion")) companionManager.spawnCompanion(sender, CompanionType.MINION);
		else if(args[0].equalsIgnoreCase("statue"))
		{

			Location lookAt = sender.getTargetBlock((HashSet<Byte>) null ,100).getLocation();
			companionManager.spawnCompanion(sender.getPlayer().getLocation(), lookAt, CompanionType.GORILLA);
			//companionManager.spawnCompanion(new Location(Bukkit.getWorld("world"), 0, 50, -3), lookAt, CompanionType.DUCK);
			//companionManager.spawnCompanion(new Location(Bukkit.getWorld("world"), 3, 50, -3), lookAt, CompanionType.MC_8);
		}

		sender.sendMessage(ChatColor.RED + "In BETA: " + ChatColor.WHITE + "Some things are a little buggyf.");

		return true;
	}
}
