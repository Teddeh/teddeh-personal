package org.mccentral.companions;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.mccentral.companions.command.TestCommand;
import org.mccentral.companions.companion.CompanionManager;
import org.mccentral.companions.type.CompanionType;
import org.mccentral.companions.util.updater.Updater;

import java.util.List;

/**
 * Created: 02/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class CompanionPlugin extends JavaPlugin {
    private static CompanionManager companionManager;
    private static CompanionPlugin plugin;
    private static List<String> flatWorlds;

    public  static boolean isFlatWorld(String world) {
        for (String flatWorld : flatWorlds)
            if (flatWorld.equalsIgnoreCase(world))
                return true;
        return false;
    }

    @Override
    public void onEnable() {
        plugin = this;
        saveDefaultConfig();
        new Updater(this);
        companionManager = new CompanionManager(this);
        getCommand("test").setExecutor(new TestCommand(companionManager));
        flatWorlds = getConfig().getStringList("Plotworlds");
        for (String world : flatWorlds) {
            Bukkit.getLogger().info("Flatworld:" + world);
        }

	    /** Should remove all glitched companions on server start up.
	     *  Mainly in case of crashes */
	    for(World world : Bukkit.getWorlds())
	    {
		    for(Entity entity : world.getEntities())
		    {
			    if(entity.getCustomName() == null) continue;
			    if(!entity.getCustomName().equals("companion")) continue;
			    for(Entity entity2 : entity.getNearbyEntities(2, 2, 2))
			    {
				    if(!(entity2 instanceof ArmorStand)) continue;
				    if(((ArmorStand) entity2).isVisible()) continue;
				    if(entity2.getCustomName() != null) continue;
				    entity2.remove();
			    }
			    entity.remove();
		    }
	    }


	    /** STATUE CODE */
	    Location lookAt = new Location(Bukkit.getWorld("world"), 0, 50, 0);
	    companionManager.spawnCompanion(new Location(Bukkit.getWorld("world"), -3, 50, -3), lookAt, CompanionType.GORILLA);
	    companionManager.spawnCompanion(new Location(Bukkit.getWorld("world"), 0, 50, -3), lookAt, CompanionType.DUCK);
	    companionManager.spawnCompanion(new Location(Bukkit.getWorld("world"), 3, 50, -3), lookAt, CompanionType.MC_8);
	    companionManager.spawnCompanion(new Location(Bukkit.getWorld("world"), -6, 50, -6), lookAt, CompanionType.CHIMP);
	    companionManager.spawnCompanion(new Location(Bukkit.getWorld("world"), 0, 50, -6), lookAt, CompanionType.PUG);
	    companionManager.spawnCompanion(new Location(Bukkit.getWorld("world"), 6, 50, -6), lookAt, CompanionType.MINION);
	    companionManager.spawnCompanion(new Location(Bukkit.getWorld("world"), 6, 50, 6), lookAt, CompanionType.TURTLE);


        /** COMPANION API */

//		Player player = null;
//
//		//SPAWN
//		getCompanionManager().spawnCompanion(player, CompanionType.MC_8); //Void
//
//		//HAS ACTIVE?
//		getCompanionManager().hasActiveCompanion(player); //Boolean
//
//		//Remove Player Companion
//		getCompanionManager().removePlayerCompanion(player); //Void
//
//		//Remove ALL
//		getCompanionManager().removeAllCompanions(); //Void
    }

    @Override
    public void onDisable() {
        super.onDisable();
        companionManager.removeAllCompanions();
	    companionManager.removeAllStatues();
    }

    public static CompanionPlugin getPlugin() {
        return plugin;
    }

    public static CompanionManager getCompanionManager() {
        return companionManager;
    }
}
