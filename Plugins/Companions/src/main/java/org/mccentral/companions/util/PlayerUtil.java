package org.mccentral.companions.util;

import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 * Created by Teddeh on 11/04/2016.
 */
public class PlayerUtil
{
	public static void sendPacket(Player player, net.minecraft.server.v1_8_R3.Packet... packets)
	{
		PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
		for(net.minecraft.server.v1_8_R3.Packet packet : packets) connection.sendPacket(packet);
	}
}
