package org.mccentral.companions.companion.part;

import net.minecraft.server.v1_8_R3.PacketPlayOutEntityTeleport;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.mccentral.companions.companion.Companion;
import org.mccentral.companions.companion.part.armorstand.CompanionEntityPart;
import org.mccentral.companions.companion.part.armorstand.CompanionEquipment;
import org.mccentral.companions.companion.part.armorstand.CompanionOptions;
import org.mccentral.companions.util.PlayerUtil;

/**
 * Created: 29/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public abstract class CompanionPart
{
	private final String name;
	private final int id;

	private float offset, radius, directionOffset;
	private double yMod;
	private final CompanionEquipment equipment;
	private final CompanionOptions options;
	private ArmorStand holder;
	private final Companion parent;


	public CompanionPart(Companion parent, String name, int id, double yMod, float offset, float radius, float directionOffset, CompanionEquipment equipment, CompanionOptions options)
	{
		this.name = name;
		this.id = id;
		this.yMod = yMod;
		this.parent = parent;
		this.offset = offset;
		this.radius = radius;
		this.directionOffset = directionOffset;
		this.equipment = equipment;
		this.options = options;
	}

	public void spawn()
	{
		CompanionEntityPart entityPart = new CompanionEntityPart(((CraftWorld) parent.getHolder().getWorld()).getHandle(), this).spawn(this);
		holder = (ArmorStand) entityPart.getBukkitEntity();
		holder.setMarker(false);
		options.apply(holder);
		equipment.apply(holder);

		if(parent.isStatue() && name.equals("head"))
		{
			holder.setCustomName(parent.getCompanionType().getName());
			holder.setCustomNameVisible(true);
		}

		teleport();
	}

	public Location update()
	{
		if (parent.getHolder() == null) return null;

		double yaw = Math.toRadians(offset + parent.getHolder().getLocation().getYaw()) + (Math.PI / 2);
		double x = parent.getHolder().getLocation().getX() + radius * Math.cos(yaw);
		double z = parent.getHolder().getLocation().getZ() + radius * Math.sin(yaw);
		double y = parent.getHolder().getLocation().getY() + yMod + parent.getYPlotOffset();
		Location loc = new Location(parent.getHolder().getLocation().getWorld(), x, y, z);
		loc.setYaw(parent.getHolder().getLocation().getYaw()+directionOffset);
		return loc;
	}

	public void teleport()
	{
		holder.teleport(update());

		PacketPlayOutEntityTeleport packet = new PacketPlayOutEntityTeleport(((CraftEntity) holder).getHandle());
		Bukkit.getOnlinePlayers().forEach(player -> PlayerUtil.sendPacket(player, packet));
	}

	public final void remove()
	{
		if(holder == null || holder.isDead()) return;
		holder.remove();
	}

	public CompanionOptions getOptions()
	{
		return options;
	}

	public CompanionEquipment getEquipment()
	{
		return equipment;
	}

	public float getRadius()
	{
		return radius;
	}

	public float getOffset()
	{
		return offset;
	}

	public ArmorStand getHolder()
	{
		return holder;
	}

	public Companion getParent()
	{
		return parent;
	}

	public String getName()
	{
		return name;
	}

	public int getId()
	{
		return id;
	}
}
