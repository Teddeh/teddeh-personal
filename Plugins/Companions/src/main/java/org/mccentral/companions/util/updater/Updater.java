package org.mccentral.companions.util.updater;

import org.bukkit.plugin.java.JavaPlugin;
import org.mccentral.companions.util.TimeUtil;

/**
 * Created: 27/04/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public class Updater implements Runnable
{
	private JavaPlugin plugin;

	public Updater(JavaPlugin plugin)
	{
		this.plugin = plugin;
		plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, this, 0L, 1L);
	}

	@Override
	public void run()
	{
		for (UpdateType update : UpdateType.values())
		{
			if (!TimeUtil.elapsed(update.getTimeLeft(), update.getMilliseconds())) continue;

			update.setTimeLeft(System.currentTimeMillis());
			plugin.getServer().getPluginManager().callEvent(new ScheduleEvent(update));
		}
	}
}
