package org.mccentral.companions.type;

import org.mccentral.companions.companion.Companion;
import org.mccentral.companions.util.C;

/**
 * Created: 02/05/2016
 *
 * @author Teddeh
 * @version 1.0
 */
public enum CompanionType
{
	MC_8(
			C.GoldB + "Star" + C.WhiteB + "Bot " + C.Reset + C.Red + "$5",
			new String[]{"Description line1", "line2", "ect.."},
			1,
			BB8.class
	),

	DUCK(
			C.YellowB + "Duck " + C.Reset + C.Red + "$5",
			new String[]{"Description line1", "line2", "ect.."},
			2,
			Duck.class
	),

	GORILLA(
			C.WhiteB + "Gorilla " + C.Reset + C.Red + "$15",
			new String[]{"Description line1", "line2", "ect.."},
			3,
			Gorilla.class
	),

	CHIMP(
			C.WhiteB + "Chimp " + C.Reset + C.Red + "$?",
			new String[]{"Description line1", "line2", "ect.."},
			4,
			Chimp.class
	),

	PUG(
			C.AquaB + "Pug " + C.Reset + C.Red + "$?",
			new String[]{"Description line1", "line2", "ect.."},
			3,
			Gorilla.class
	),

	MINION(
			C.YellowB + "Minion " + C.Reset + C.Red + "$?",
			new String[]{"Description line1", "line2", "ect.."},
			3,
			Gorilla.class
	),

	TURTLE(
			C.GreenB + "Turtle " + C.Reset + C.Red + "$?",
			new String[]{"Description line1", "line2", "ect.."},
			3,
			Gorilla.class
	);

	private String name;
	private String[] description;
	private int id;
	private Class<? extends Companion> companionClass;

	CompanionType(String name, String[] description, int id, Class<? extends Companion> companionClass)
	{
		this.name = name;
		this.description = description;
		this.id = id;
		this.companionClass = companionClass;
	}

	public String getName()
	{
		return name;
	}

	public String[] getDescription()
	{
		return description;
	}

	public int getId()
	{
		return id;
	}

	public Class<? extends Companion> getCompanionClass()
	{
		return companionClass;
	}
}
